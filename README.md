# PyDynPeak is a Python software for Pulse Detection and Frequency Analysis in Hormonal Time Series.

## Version 0.1.0  April 2021

This software is based on the paper:

  Vidal A, Zhang Q, Médigue C, Fabre S, Clément F (2012) DynPeak: An
  Algorithm for Pulse Detection and Frequency Analysis in Hormonal
  Time Series. PLoS ONE 7(7):e39001. doi:10.1371/journal.pone.0039001

And on the DynPeak Scilab ATOMS Toolbox version 2.1.0, Authors
(cf. https://atoms.scilab.org/toolboxes/Dynpeak): Claire Medigue, Serge Steer,
Qinghua Zhang, Alexandre Vidal, Frédérique Clément

PyDynPeak has been developed by Hande Gözükan and Christian Poli with the scientific supervision of Frédérique Clément.


## Requirements

Python 3.7 or newer.

## Live Demo

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.inria.fr%2Fmusca%2Fpydynpeak/stable?filepath=pydynpeak%2Fnotebook%2F)

A live demo is available online on [mybinder.org](https://mybinder.org/). By
clicking "launch binder" badge above you can start experimenting with the
notebooks under [pydynpeak/notebook](./pydynpeak/notebook/) directory without
installing **PyDynPeak** locally. 

It might take some time to launch the demo and the user session is terminated
after 10 minutes of inactivity. 

Live demo uses **stable** branch.

## Installation

### With miniconda/anaconda (recommended):

Currently, the easiest way to install *PyDynPeak* is as follows:

1. Get the source code:
    * If you are a **git** user, clone this repository with **git**
    * If you prefer not to use git, download the source code package and unpack it (see the &#x1F4E5; button above)

2. Install the latest version of miniconda (if not yet done)

   **NB:** The following steps work if conda is in your PATH environment variable, e.g. if you set it up that way during installation, or if you're running the "Anaconda Prompt" (on windows).
   If that's not the case, consider adding conda path in your PATH variable or prefixing the following conda commands with the appropriate prefix.

3. Create a conda environment with the following commands:

```
cd [/your/path/to/]pydynpeak-master
conda env create -f binder/environment.yml [-n another-env-name]
```
NB: by default it will create an environment called *pydynpeak*. If you want, you can change this name using the ```-n``` option

4. Activate this environment:

```
conda activate pydynpeak
```

or, if ```conda activate``` is not supported on your system, do:

```
source [/path/to/]activate pydynpeak # on Linux or MacOS (activate has the same path as conda)
```
or

```
activate pydynpeak # on Windows
```



5. Run the tests:

```
cd ci_tests
python tests.py
```

6. Run the notebooks:

```
cd pydynpeak
cd notebook
jupyter notebook
```

## Getting started with PyDynPeak

A [quickstart](https://gitlab.inria.fr/musca/pydynpeak/-/blob/master/pydynpeak/notebook/quickstart.ipynb) notebook is available to get familiar with PyDynPeak use and functionalities.

However, if you prefer to use custom [magic commands](https://ipython.readthedocs.io/en/stable/interactive/magics.html)  (instead of API function calls) to work with PyDynPeak you can find some tips [here](https://gitlab.inria.fr/musca/pydynpeak/-/blob/master/docs/source/notebook/magics_quickstart.ipynb).

## Documentation

PyDynPeak Documentation is available [here](https://musca.gitlabpages.inria.fr/pydynpeak/index.html).

