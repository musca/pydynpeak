set -e -x
rm -rf $HOME/miniconda3/||true
cp -rp $HOME/blank_miniconda3 $HOME/miniconda3
export "PATH=$HOME/miniconda3/bin:$PATH"
conda install -y -c plotly -c conda-forge -c $CI_PROJECT_DIR/local_channel dynpeak
cd ci_tests
python tests.py
