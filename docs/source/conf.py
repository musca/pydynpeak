# This file is part of PyDynPeak software
# Copyright (C) 2021, Inria
#
# PyDynpeak is a Python software derived from:
#          DynPeak (Scilab ATOMS Toolbox) version 2.1.0 (authors:
#	   	   	   	Claire Medigue, Serge Steer, Qinghua Zhang,
#              			Alexandre Vidal, Frédérique Clément
#                               cf. https://atoms.scilab.org/toolboxes/Dynpeak)
#
# PyDynPeak is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PyDynPeak is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PyDynPeak.  If not, see <https://www.gnu.org/licenses/>
#
###############################################################################
#
# Authors : Frédérique Clément(+), Hande Gözükan(*), Christian Poli(*)
#
# +=scientific advisor, *=developer
#
###############################################################################

# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import sphinx_bootstrap_theme
import os
import sys
sys.path.insert(0, os.path.abspath('../../pydynpeak'))


# -- Project information -----------------------------------------------------

project = 'PyDynPeak'
copyright = '2019, Musca'
author = 'Christian Poli, Hande Gözükan'

# The full version, including alpha/beta/rc tags
release = '0.0.1'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    # extension to pull docstrings from modules to document
    'sphinx.ext.autodoc',
    # support for NumPy and Google style docstrings
    'sphinx.ext.napoleon',
    # to generate automatic links to the documentation of objects in other projects
    'sphinx.ext.intersphinx',
    'nbsphinx',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

master_doc = 'index'

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = "sphinx"

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#

html_theme = 'bootstrap'
html_theme_path = sphinx_bootstrap_theme.get_html_theme_path()

html_theme_options = {
    'navbar_sidebarrel': False,
    'source_link_position': "footer",
    'bootstrap_version': "3",
    'navbar_class': "navbar navbar-inverse",
    'navbar_links': [
        ("Users Guide", "users_guide"),
        ("API", "api_reference"),
        ("GitLab", "https://gitlab.inria.fr/musca/pydynpeak", True),
        ("Scilab Version",
         "https://team.inria.fr/musca/dynpeak/", True),
    ],
    'bootswatch_theme': 'cosmo',
}

html_sidebars = {'**': ['localtoc.html', 'searchbox.html']}

# html_theme = 'alabaster'

# # https://alabaster.readthedocs.io/en/latest/customization.html
# html_theme_options = {
#     "description": "A Python implementation of DynPeak Scilab version to be used with Jupyter notebooks.",
#     "fixed_sidebar": True,
#     'content_width': '700px',
#     "page_width": "50%",
#     "show_powered_by": False,
#     "show_relbars": True
# }

#nbsphinx_prompt_width = "-10%"
#nbsphinx_responsive_width = "100%"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']


intersphinx_mapping = {
    'python': ('https://docs.python.org/3', None),
    'pandas': ('http://pandas-docs.github.io/pandas-docs-travis/', None)
}

exclude_patterns = ['**.ipynb_checkpoints', '**template**']

nbsphinx_execute = 'always'
