Installation
============

^^^^^^^^^^^^^^^^^^^^^^
PyDynPeak Installation
^^^^^^^^^^^^^^^^^^^^^^

- Install the latest version of **Miniconda** for **Python 3.x** from https://docs.conda.io/en/latest/miniconda.html.
  
  - If you already have a recent installation of **miniconda** or **anaconda** (with **Python> = 3.7**) you can skip this step.
  - Note the name of the directory where you installed *miniconda/anaconda*. Later we will refer to it as ``/your/conda_dir/``.

- Download the environment.yml_ file in a directory of your choice.

- In a terminal run:

.. code-block:: shell

	conda env create -f /path/to/environment.yml``

This command will create a **conda** environment called **pydynpeak**. For more details see here_.


    - If you are using windows10 use "Anaconda Powershell Prompt" to run this command. It is accessible from the menu after miniconda installation.
    - Currently this command only works if you are on a network in the domain ``inria.fr``; otherwise you must activate the **VPN** (inria) before executing it.
    - Under Linux and MacOS, if the conda executable directory (``/your/conda_dir/bin/``) is not in the user's environment (**PATH** variable), depending on the choice made when installing miniconda/anaconda, you will need to provide the full path in the previous command:

      .. code-block:: shell

	/your/conda_dir/bin/conda env create -f /path/to/environment.yml

^^^^^^^^^
Execution
^^^^^^^^^

- Always activate the **pydynpeak** environment if it is not already active.

  Linux and MacOS:

  .. code-block:: shell

	conda activate pydynpeak

	
  In case of problem with the previous command run:

  .. code-block:: shell

	source activate pydynpeak

  Windows:

  .. code-block:: shell

	conda activate pydynpeak

* In the **notebook** directory of the **dynpeak** package run:

  .. code-block:: shell

	jupyter notebook

Your default browser will display the index of available demo notebooks which you can then open and run.


.. _environment.yml: https://gitlab.inria.fr/musca/pydynpeak/-/raw/master/environment.yml?inline=false
.. _here: https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#creating-an-environment-from-an-environment-yml-file
