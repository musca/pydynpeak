Contact
-------

Please contact MUSCA_ (MUltiSCAle population dynamics for physiological systems) team for further info.

.. _MUSCA: https://team.inria.fr/musca/members/
