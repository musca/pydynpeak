.. DynPeak documentation master file, created by
   sphinx-quickstart on Wed Nov 13 10:18:41 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

PyDynPeak documentation
=======================

Welcome to PyDynPeak's documentation.

PyDynPeak is an algorithm for pulse detection and frequency analysis in hormonal time series based on `Vidal A, Zhang Q, Médigue C, Fabre S, Clément F (2012) DynPeak: An Algorithm for Pulse Detection and Frequency Analysis in Hormonal Time Series. PLoS ONE 7(7):e39001. doi:10.1371/journal.pone.0039001
<https://hal.inria.fr/hal-00654790/>`_.


.. toctree::
   :maxdepth: 2

   users_guide
   api_reference

.. toctree::
   :maxdepth: 1
   
   Scilab Version <https://m3disim.saclay.inria.fr/people/frederique-clement/dynpeak/>
   GitLab <https://gitlab.inria.fr/musca/pydynpeak>

.. toctree::
   :maxdepth: 1

   FAQ <faq>
   contact
