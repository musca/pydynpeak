Users Guide
-----------

.. toctree::
   :maxdepth: 2

   installation
   notebook/quickstart
   notebook/magics_quickstart
