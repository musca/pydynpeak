#!/bin/bash
set -e -x
MINICONDA_DIR=$HOME/miniconda3
OS_TAG=$1
if [[ -d $MINICONDA_DIR/etc ]]; then
    echo "Miniconda already installed";
else
    rm -rf "$MINICONDA_DIR";
    wget https://repo.continuum.io/miniconda/Miniconda3-latest-${OS_TAG}-x86_64.sh -O miniconda.sh;
    bash miniconda.sh -b -p $MINICONDA_DIR;
fi;
source "$MINICONDA_DIR/etc/profile.d/conda.sh"
hash -r
if (echo "$(bash ./needs_rebuild.sh)"  | fgrep "REBUILD"); then
    conda remove --name pydynpeak --all --yes;
fi
envs=$(conda info --envs)
if ! (echo "$envs"  | grep -q "pydynpeak"); then
    conda config --set always_yes yes --set changeps1 no;
    conda remove --name pydynpeak --all --yes;
    conda update -q conda;
    conda info -a;
    conda env create -q -f binder/environment.yml;
fi

