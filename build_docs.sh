#!/bin/bash

set -e -x

source $HOME/miniconda3/etc/profile.d/conda.sh
conda activate pydynpeak
conda install -c anaconda -c conda-forge sphinx=3.3.1 nbsphinx=0.8.3 sphinx_bootstrap_theme
conda install "jinja2<2.12" -c conda-forge # avoids a nbsphinx issue when jinja2=3.0, to be removed ASAP
cd docs
cp ../pydynpeak/notebook/quickstart.ipynb source/notebook/
cp ../pydynpeak/notebook/magics_quickstart.ipynb source/notebook/
make clean
make html

mv build/html/ ../public/
rm -f source/notebook/quickstart.ipynb source/notebook/magics_quickstart.ipynb
