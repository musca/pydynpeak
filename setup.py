# This file is part of PyDynPeak software
# Copyright (C) 2021, Inria
#
# PyDynpeak is a Python software derived from:
#          DynPeak (Scilab ATOMS Toolbox) version 2.1.0 (authors:
#	   	   	   	Claire Medigue, Serge Steer, Qinghua Zhang,
#              			Alexandre Vidal, Frédérique Clément
#                               cf. https://atoms.scilab.org/toolboxes/Dynpeak)
#
# PyDynPeak is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PyDynPeak is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PyDynPeak.  If not, see <https://www.gnu.org/licenses/>
#
###############################################################################
#
# Authors : Frédérique Clément(+), Hande Gözükan(*), Christian Poli(*)
#
# +=scientific advisor, *=developer
#
###############################################################################

"""
Setup file for pydynpeak.
"""
import os
import os.path
from setuptools import setup
import unittest


# Get the long description from the relevant file
here = os.path.abspath(os.path.dirname(__file__))

with open(os.path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

PACKAGES = ['pydynpeak',
            'pydynpeak.core',
            'pydynpeak.model',
            'pydynpeak.dataset',
            'pydynpeak.notebook',
            'pydynpeak.vis',
            'pydynpeak.tests',
            ]

install_requires = [
    "numpy>=1.16.2",
    "pandas>=0.23.4",
    "scipy>=0.19.0",
    "notebook>=5.7.8",
    "ipywidgets>=7.5.1",
    "plotly>=4.1.1",
    "kaleido>=0.2.1",
    "psutil>=5.6.3",
    "requests>=2.21.0",
    "pyyaml"
]

#kw = {} if os.getenv("CONDA_BUILD") else dict(install_requires=install_requires)
kw = {}

setup(
    name="pydynpeak",
    version="0.1.0",
    author="See authors.md file",
    author_email="sed-sac@inria.fr",
    description="Python toolbox for Pulse Detection and Frequency Analysis in Hormonal Time Series.",
    # For an analysis of "install_requires" vs pip's requirements files see:
    # https://packaging.python.org/en/latest/requirements.html
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Scientific/Engineering :: Information Analysis',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    platforms='any',
    packages=PACKAGES,
    package_data={'pydynpeak': ['data/*.csv',
                                'data/*.xls', 'notebook/*.ipynb']},
    python_requires='>=3.6, <4',
    test_suite='pydynpeak.tests',
    **kw
)
