set -e -x
#export "PATH=$HOME/miniconda3/bin:$PATH"
#source $HOME/miniconda3/bin/activate pydynpeak
source $HOME/miniconda3/etc/profile.d/conda.sh
conda activate pydynpeak
cd ci_tests
python tests.py
