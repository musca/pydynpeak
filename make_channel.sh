#!/bin/bash
set -e -x
if [ -d "channel" ]; then
    echo "a directory named 'channel' already exists, abandon ..."
    exit 1
fi
LINUX_DIR="channel/linux-64/"
OSX_DIR="channel/osx-64/"
WIN_DIR="channel/win-64"
NOARCH_DIR="channel/noarch/"
mkdir -p "$NOARCH_DIR"
scp ci@dynpeak-linux.ci:/builds/miniconda3/conda-bld/noarch/dynpeak*.tar.bz2 "$NOARCH_DIR"
for d in $LINUX_DIR $OSX_DIR $WIN_DIR; do
    mkdir -p $d
done
$HOME/miniconda3/bin/conda-index channel/
ssh scm.gforge.inria.fr  'rm -rf /home/groups/paloma/htdocs/conda/channel'
scp -r channel scm.gforge.inria.fr:/home/groups/paloma/htdocs/conda/
