# This file is part of PyDynPeak software
# Copyright (C) 2021, Inria
# 
# PyDynpeak is a Python software derived from:
#          DynPeak (Scilab ATOMS Toolbox) version 2.1.0 (authors:
#	   	   	   	Claire Medigue, Serge Steer, Qinghua Zhang,
#              			Alexandre Vidal, Frédérique Clément
#                               cf. https://atoms.scilab.org/toolboxes/Dynpeak)
#
# PyDynPeak is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# PyDynPeak is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with PyDynPeak.  If not, see <https://www.gnu.org/licenses/>
# 
# Parts of this file are based on work covered by the following copyright
#       and permission notice:
#      """
#      // Copyright (C) 2012 - INRIA - Serge Steer
#      // This file must be used under the terms of the CeCILL.
#      // This source file is licensed as described in the file COPYING, which
#      // you should have received as part of this distribution.  The terms
#      // are also available at
#      // http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#      """
###############################################################################
#
# Authors : Frédérique Clément(+), Hande Gözükan(*), Christian Poli(*)
#
# +=scientific advisor, *=developer
#
###############################################################################

from os.path import dirname, join

from plotly.graph_objects import FigureWidget

from . import DynPeakTest
from pydynpeak.model import DynpeakExecuter, DynpeakExecuterWrapper
from pydynpeak.core import lhpeaks_default_params

from contextlib import redirect_stdout
from io import StringIO

import numpy as np
import pandas as pd

_data_dir = join(dirname(__file__), '..', 'data')
csvfile = join(_data_dir, 'CaratyData.csv')
csv_col_count = 9
csv_row_count = 91
excelfile = join(_data_dir, 'LacauneData.xls')
excel_col_count = 10
sname = 'Caraty ewes'
sep = '\t'


class TestDynpeakExecuter(DynPeakTest):

    def setUp(self):
        self.dyn_sans_data = DynpeakExecuter()

        self.dyn = DynpeakExecuter()
        self.dyn.load_csv(csvfile, sep=sep, seriesname=sname)

    def tearDown(self):
        self.dyn = None

    def test_currentseries(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.currentseries` property.

        '''
        # test when no data loaded
        self.assertEqual(self.dyn_sans_data.currentseries, None)

        # test when data loaded
        self.dyn.load_csv(csvfile, sep=sep, seriesname=sname)
        self.assertEqual(self.dyn.currentseries, 1)

        self.dyn.currentseries = csv_col_count
        self.assertEqual(self.dyn.currentseries, csv_col_count)

        # test for invalid_index=0 and invalid_index=csv_col_count+1
        invalid_index_list = [0, csv_col_count + 1]

        for invalid_index in invalid_index_list:
            with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='WARN') as cm:
                self.dyn.currentseries = invalid_index
                self.assertEqual(cm.output, [
                    "ERROR:pydynpeak.model.dynpeakexecuter:Please specify an index value between 1 and 9 inclusive.",
                    f"WARNING:pydynpeak.model.dynpeakexecuter:Invalid index {invalid_index}. Current series index has not changed."
                ])
                self.assertEqual(self.dyn.currentseries, csv_col_count)

    def test_load_csv(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.load_csv` method.

        '''
        self.assertEqual(self.dyn_sans_data.currentseries, None)

        # test with correct parameters
        self.dyn.load_csv(csvfile, sep=sep, seriesname=sname)
        self.assertEqual(self.dyn.currentseries, 1)
        self.dyn.currentseries = csv_col_count
        self.assertEqual(self.dyn.currentseries, csv_col_count)

        # test filepath=None
        with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
            self.dyn.load_csv(None, sep=sep, seriesname=sname)
            self.assertEqual(cm.output, [
                             "ERROR:pydynpeak.model.dynpeakexecuter:Invalid file path or buffer object type: <class 'NoneType'>"])

        # test invalid filepath
        with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
            self.dyn.load_csv('', sep=sep, seriesname=sname)
            self.assertEqual(cm.output, [
                             "ERROR:pydynpeak.model.dynpeakexecuter:File '' does not exist. Please specify a valid CSV file."])

        # wrong field separator
        self.dyn.load_csv(csvfile, seriesname=sname)
        self.assertEqual(self.dyn.currentseries, 1)
        with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='WARN') as cm:
            self.dyn.currentseries = csv_col_count
            self.assertEqual(cm.output, [
                "ERROR:pydynpeak.model.dynpeakexecuter:Please specify an index value between 1 and 1 inclusive.",
                f"WARNING:pydynpeak.model.dynpeakexecuter:Invalid index {csv_col_count}. Current series index has not changed."
            ])

    def test_load_excel(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.load_excel` method.

        '''
        self.assertEqual(self.dyn_sans_data.currentseries, None)

        # test with correct parameters
        self.dyn.load_excel(excelfile, sheet_name=0)
        self.assertEqual(self.dyn.currentseries, 1)
        self.dyn.currentseries = 10
        self.assertEqual(self.dyn.currentseries, 10)

        from packaging import version
        # test filepath=None
        with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
            self.dyn.load_excel(None)
            if version.parse(pd.__version__) >= version.parse('1.2.3'):
                self.assertEqual(cm.output, [
                    "ERROR:pydynpeak.model.dynpeakexecuter:Invalid file path or buffer object type: <class 'NoneType'>"]) # assertion error "assert content_or_path is not None" in pandas/io/excel/_base.py:947
            elif version.parse(pd.__version__) >= version.parse('1.2.0'):
                self.assertEqual(cm.output, [
                    "ERROR:pydynpeak.model.dynpeakexecuter:File 'None' does not exist. Please specify a valid CSV file."])
            else:
                self.assertEqual(cm.output, [
                    "ERROR:pydynpeak.model.dynpeakexecuter:Invalid file path or buffer object type: <class 'NoneType'>"])

        # test invalid filefath
        with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
            self.dyn.load_excel('')
            if version.parse(pd.__version__) >= version.parse('1.2.3'):
                self.assertEqual(cm.output, [
                    "ERROR:pydynpeak.model.dynpeakexecuter:File '' does not exist. Please specify a valid CSV file."]) # assertion error again
            else:
                self.assertEqual(cm.output, [
                    "ERROR:pydynpeak.model.dynpeakexecuter:File '' does not exist. Please specify a valid CSV file."])

        # invalid sheet name
        invalid_sheet_names = [1, "invalid"]
        for invalid_sheet_name in invalid_sheet_names:
            with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
                self.dyn.load_excel(excelfile, sheet_name=invalid_sheet_name)
                if version.parse(pd.__version__) >= version.parse('1.2.3'):
                    self.assertEqual(cm.output, [
                        f"ERROR:pydynpeak.model.dynpeakexecuter:Worksheet named '{invalid_sheet_name}' not found" if isinstance(invalid_sheet_name, str) else
                        f"ERROR:pydynpeak.model.dynpeakexecuter:Worksheet index {invalid_sheet_name} is invalid, 1 worksheets found"
                    ]
                    )
                else:
                    self.assertEqual(cm.output, [
                        f"ERROR:pydynpeak.model.dynpeakexecuter:File {excelfile} does not contain sheet {invalid_sheet_name}. Please specify an existing sheet name."])

    def test_view_head(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.view_head` method.

        '''

        # test when no data loaded
        with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
            self.assertIsNone(self.dyn_sans_data.view_head())
            self.assertEqual(cm.output, [
                             "ERROR:pydynpeak.model.dynpeakexecuter:No data available! Please first load data!"])

        # test when data loaded
        graph = self.dyn.view_head()
        self.assertTrue(isinstance(graph, FigureWidget))
        self.assertEqual((csv_col_count+1, 10),
                         graph.data[0].cells.values.shape)

        graph = self.dyn.view_head(50)
        self.assertTrue(isinstance(graph, FigureWidget))
        self.assertEqual((csv_col_count+1, 50),
                         graph.data[0].cells.values.shape)

    def test_view_tail(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.view_tail` method.

        '''
        # test when no data loaded
        with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
            self.assertIsNone(self.dyn_sans_data.view_tail())
            self.assertEqual(cm.output, [
                             "ERROR:pydynpeak.model.dynpeakexecuter:No data available! Please first load data!"])

        # test when data loaded
        graph = self.dyn.view_tail()
        self.assertTrue(isinstance(graph, FigureWidget))
        self.assertEqual((csv_col_count+1, 10),
                         graph.data[0].cells.values.shape)

        graph = self.dyn.view_tail(20)
        self.assertTrue(isinstance(graph, FigureWidget))
        self.assertEqual((csv_col_count+1, 20),
                         graph.data[0].cells.values.shape)

    def test_view_all(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.view_all` method.

        '''
        # test when no data loaded
        with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
            self.assertIsNone(self.dyn_sans_data.view_all())
            self.assertEqual(cm.output, [
                             "ERROR:pydynpeak.model.dynpeakexecuter:No data available! Please first load data!"])

        # test when data loaded
        graph = self.dyn.view_all()
        self.assertEqual(csv_col_count+1, len(graph.data[0].cells.values))
        self.assertEqual(csv_row_count, len(graph.data[0].cells.values[0]))

    def test_view_sampling_times(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.view_sampling_times` method.

        '''
        # test when no data loaded
        with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
            self.assertIsNone(self.dyn_sans_data.view_sampling_times())
            self.assertEqual(cm.output, [
                             "ERROR:pydynpeak.model.dynpeakexecuter:No data available! Please first load data!"])

        # test when data loaded
        graph = self.dyn.view_sampling_times()
        self.assertTrue(isinstance(graph, FigureWidget))
        self.assertEqual(1, len(graph.data[0].cells.values))
        self.assertEqual(csv_row_count, len(graph.data[0].cells.values[0]))

    def test_plot_lh_all(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.plot_lh_all` method.

        '''
        # test when no data loaded
        with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
            self.assertIsNone(self.dyn_sans_data.plot_lh_all())
            self.assertEqual(cm.output, [
                             "ERROR:pydynpeak.model.dynpeakexecuter:No data available! Please first load data!"])

        # load data without series name
        self.dyn.load_csv(csvfile, sep=sep)

        graph = self.dyn.plot_lh_all()
        self.assertTrue(isinstance(graph, FigureWidget))
        self.assertEqual(csv_col_count, len(graph.data))
        self.assertEqual("", graph.layout.title.text)

        # load data with series name
        self.dyn.load_csv(csvfile, sep=sep, seriesname=sname)

        graph = self.dyn.plot_lh_all()
        self.assertTrue(isinstance(graph, FigureWidget))
        self.assertEqual(csv_col_count, len(graph.data))
        self.assertEqual(sname, graph.layout.title.text)

        # specify plot header
        header = 'Caraty Data'
        graph = self.dyn.plot_lh_all(header=header)
        self.assertTrue(isinstance(graph, FigureWidget))
        self.assertEqual(csv_col_count, len(graph.data))
        self.assertEqual(header, graph.layout.title.text)

    def test_plot_lh(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.plot_lh` method.

        '''
        # test when no data loaded
        with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
            self.assertIsNone(self.dyn_sans_data.plot_lh(1))
            self.assertEqual(cm.output, [
                             "ERROR:pydynpeak.model.dynpeakexecuter:No data available! Please first load data!"])

        # test no index specified
        self.dyn.load_csv(csvfile, sep=sep, seriesname=sname)
        graph = self.dyn.plot_lh()
        self.assertIsNotNone(graph)
        self.assertTrue(isinstance(graph, FigureWidget))
        self.assertEqual(1, len(graph.data))

        # test valid index=2
        graph = self.dyn.plot_lh(2)
        self.assertIsNotNone(graph)
        self.assertTrue(isinstance(graph, FigureWidget))
        self.assertEqual(1, len(graph.data))

        # test invalid index = 0 and index=csv_col_count+1
        index_list = [0, csv_col_count + 1]
        for invalid_index in index_list:
            with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
                self.assertIsNone(self.dyn.plot_lh(invalid_index))
                self.assertEqual(cm.output, [
                                 f"ERROR:pydynpeak.model.dynpeakexecuter:Please specify an index value between 1 and {csv_col_count} inclusive."])

    def test_update_detection_params(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.update_detection_params`
        method.

        '''
        # test when no data loaded
        with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
            self.assertFalse(
                self.dyn_sans_data.update_detection_params(detectionthreshold=1))
            self.assertEqual(cm.output, [
                             "ERROR:pydynpeak.model.dynpeakexecuter:No data available! Please first load data!"])

        # test when data loaded
        # test no index specified
        self.assertEqual(0, self.dyn.get_detection_params().detectionthreshold)
        self.dyn.update_detection_params(detectionthreshold=1)
        self.assertEqual(1, self.dyn.get_detection_params().detectionthreshold)

        # test index=2 specified
        self.assertEqual(0, self.dyn.get_detection_params(
            index=2).detectionthreshold)
        self.dyn.update_detection_params(index=2, detectionthreshold=1)
        self.assertEqual(1, self.dyn.get_detection_params(
            index=2).detectionthreshold)

        # test invalid index = 0 and index=csv_col_count+1
        index_list = [0, csv_col_count + 1]
        for invalid_index in index_list:
            with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
                self.assertFalse(self.dyn.update_detection_params(
                    index=invalid_index, detectionthreshold=1))
                self.assertEqual(cm.output, [
                                 f"ERROR:pydynpeak.model.dynpeakexecuter:Please specify an index value between 1 and {csv_col_count} inclusive."])

    def test_update_detection_params_detectionthreshold(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.update_detection_params`
        method to change value of `detectionthreshold`.

        '''
        # test valid detectionthreshold=5
        self.assertTrue(self.dyn.update_detection_params(detectionthreshold=5))
        self.assertEqual(5, self.dyn.get_detection_params().detectionthreshold)

        # test invalid detectionthreshold=-1
        with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
            self.assertFalse(
                self.dyn.update_detection_params(detectionthreshold=-1))
            self.assertEqual(cm.output, [
                             "ERROR:pydynpeak.model.dynpeakexecuter:update_detection_params: Non negative value expected for detectionthreshold"])
        # note: to check if the value has not changed.
        # If the previous test that sets detectionthreshold=5 changes, this assertion should change as well
        self.assertEqual(5, self.dyn.get_detection_params().detectionthreshold)

    def test_update_detection_params_globalreltol(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.update_detection_params`
        method to change value of `globalreltol`.

        '''
        # test valid globalreltol=0.5 and globalreltol=1
        g_valid_values = [0.5, 1]
        for value in g_valid_values:
            self.assertTrue(
                self.dyn.update_detection_params(globalreltol=value))
            self.assertEqual(
                value, self.dyn.get_detection_params().globalreltol)

        # test invalid globalreltol=-1, globalreltol=0 and globalrelval= 1.5
        g_invalid_values = [-1, 0, 1.5]
        for value in g_invalid_values:
            with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
                self.assertFalse(
                    self.dyn.update_detection_params(globalreltol=value))
                self.assertEqual(cm.output, [
                                 "ERROR:pydynpeak.model.dynpeakexecuter:update_detection_params: 0<value<=1 expected for globalreltol"])
        # note: to check if the value has not changed.
        # If the previous test that sets globalreltol=1 changes, this assertion should change as well
        self.assertEqual(1, self.dyn.get_detection_params().globalreltol)

    def test_update_detection_params_threepointtol(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.update_detection_params`
        method to change value of `threepointtol`.

        '''
        # test valid threepointtol=0.5 and threepointtol=1
        t_valid_values = [0.5, 1]
        for value in t_valid_values:
            self.assertTrue(
                self.dyn.update_detection_params(threepointtol=value))
            self.assertEqual(
                value, self.dyn.get_detection_params().threepointtol)

        # test invalid threepointtol=-1, threepointtol=0 and threepointtol= 1.5
        t_invalid_values = [-1, 0, 1.5]
        for value in t_invalid_values:
            with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
                self.assertFalse(
                    self.dyn.update_detection_params(threepointtol=value))
                self.assertEqual(cm.output, [
                                 "ERROR:pydynpeak.model.dynpeakexecuter:update_detection_params: 0<value<=1 expected for threepointtol"])
        # note: to check if the value has not changed.
        # If the previous test that sets threepointtol=1 changes, this assertion should change as well
        self.assertEqual(1, self.dyn.get_detection_params().threepointtol)

    def test_update_detection_params_localabstol(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.update_detection_params`
        method to change value of `localabstol`.

        '''
        # test valid localabstol=0.5 and localabstol=2
        l_valid_values = [0.5, 2]
        for value in l_valid_values:
            self.assertTrue(
                self.dyn.update_detection_params(localabstol=value))
            self.assertEqual(
                value, self.dyn.get_detection_params().localabstol)

        # test invalid localabstol=-1, localabstol=0
        l_invalid_values = [-1, 0]
        for value in l_invalid_values:
            with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
                self.assertFalse(
                    self.dyn.update_detection_params(localabstol=value))
                self.assertEqual(cm.output, [
                                 "ERROR:pydynpeak.model.dynpeakexecuter:update_detection_params: Positive value expected for localabstol"])
        # note: to check if the value has not changed.
        # If the previous test that sets localabstol=2 changes, this assertion should change as well
        self.assertEqual(2, self.dyn.get_detection_params().localabstol)

    def test_update_detection_params_period(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.update_detection_params`
        method to change value of `period`.

        '''
        # test valid period=0.5 and period=2
        p_valid_values = [0.5, 2]
        for value in p_valid_values:
            self.dyn.update_detection_params(period=value)
            self.assertEqual(value, self.dyn.get_detection_params().period)

        # test invalid period=-1, period=0
        p_invalid_values = [-1, 0]
        for value in p_invalid_values:
            with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
                self.assertFalse(
                    self.dyn.update_detection_params(period=value))
                self.assertEqual(cm.output, [
                                 "ERROR:pydynpeak.model.dynpeakexecuter:update_detection_params: Positive value expected for period"])
        # note: to check if the value has not changed.
        # If the previous test that sets localabstol=2 changes, this assertion should change as well
        self.assertEqual(2, self.dyn.get_detection_params().period)

    def test_update_detection_params_uncertainty(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.update_detection_params`
        method to change value of `uncetainity`.

        '''
        # test valid uncertainty=[[1,1000], [0,0]]
        uc_valid_values = [[[1, 1000], [0, 0]], [[1, 1000], [0, 1]], [
            [1, 1000], [1, 1]], [[1, 1000], [0.7, 0.5]], [[1, 1000], [0, 0.5]]]
        for value in uc_valid_values:
            self.assertTrue(
                self.dyn.update_detection_params(uncertainty=value))
            np.testing.assert_array_almost_equal(
                value, self.dyn.get_detection_params().uncertainty)

        # test invalid uncertainty=[0,1000], uncertainty=5, uncertainty="string"
        uc_invalid_values = [[0, 1000], 5, "string"]
        for value in uc_invalid_values:
            with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
                self.assertFalse(
                    self.dyn.update_detection_params(uncertainty=value))
                self.assertEqual(cm.output, [
                                 "ERROR:pydynpeak.model.dynpeakexecuter:update_detection_params: Real 2 rows array expected for uncertainty"])

        # test invalid uncertainty=[[-1, 1000], [0, 0]], uncertainty=[[-1, -1000], [0, 0]], uncertainty=[[1, -1], [0 ,0]]
        uc_invalid_values = [[[-1, 1000], [0, 0]], [[-1, -1000], [0, 0]]]
        for value in uc_invalid_values:
            with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
                self.assertFalse(
                    self.dyn.update_detection_params(uncertainty=value))
                self.assertEqual(cm.output, [
                                 "ERROR:pydynpeak.model.dynpeakexecuter:update_detection_params: first row elements must be non negative for uncertainty"])

        # test invalid uncertainty=[[0, 0], [0, 0]], uncertainty=[[1, 0], [0, 0]]
        uc_invalid_values = [[[0, 0], [0, 0]], [[1, 0], [0, 0]]]
        for value in uc_invalid_values:
            with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
                self.assertFalse(
                    self.dyn.update_detection_params(uncertainty=value))
                self.assertEqual(cm.output, [
                                 "ERROR:pydynpeak.model.dynpeakexecuter:update_detection_params: Increasing order expected on first row elements for uncertainty"])

        # note: to check if the value has not changed.
        # If the last valid test that sets uncertainty=[[1,1000], [0,0]] changes, this assertion should change as well
        np.testing.assert_array_almost_equal(
            [[1, 1000], [0, 0.5]], self.dyn.get_detection_params().uncertainty)

    def test_update_detection_params_interval(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.update_detection_params`
        method to change value of `interval`.

        '''
        # test valid selection = [0, 91], [5, 10]
        valid_values = [[0, 91], [5, 10]]
        for value in valid_values:
            self.assertTrue(self.dyn.update_detection_params(interval=value))
            self.assertEqual(value, self.dyn.get_detection_params().interval)

        # test for invalid interval values
        invalid_interval = [-1, 20]
        with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
            self.assertFalse(self.dyn.update_detection_params(
                interval=invalid_interval))
            self.assertEqual(cm.output, [
                             "ERROR:pydynpeak.model.dynpeakexecuter:Interval should be in [0, 91]."])

        invalid_interval = [30]
        with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
            self.assertFalse(self.dyn.update_detection_params(
                interval=invalid_interval))
            self.assertEqual(cm.output, [
                             "ERROR:pydynpeak.model.dynpeakexecuter:(0,) or (2,) dim. vector expected for interval."])

        invalid_interval = [30, 20]
        with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
            self.assertFalse(self.dyn.update_detection_params(
                interval=invalid_interval))
            self.assertEqual(cm.output, [
                             "ERROR:pydynpeak.model.dynpeakexecuter:Second element must be greater than the first one for interval."])

        invalid_interval = [10, 92]
        with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
            self.assertFalse(self.dyn.update_detection_params(
                interval=invalid_interval))
            self.assertEqual(cm.output, [
                             "ERROR:pydynpeak.model.dynpeakexecuter:Interval should be in [0, 91]."])

    def test_update_detection_params_selection(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.update_detection_params` method to change value of `selection`.
        '''
        # test if selection can be set
        selection = [10, 20]
        with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='WARN') as cm:
            self.assertTrue(
                self.dyn.update_detection_params(selection=selection))
            self.assertEqual(cm.output, [
                             "WARNING:pydynpeak.model.dynpeakexecuter:Selection key is not allowed to be set, ignoring."])
            self.assertEqual([], self.dyn.get_detection_params().selection)

    def test_update_detection_params_imposed(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.update_detection_params`
        method to change value of `imposedpeaks`.

        '''
        # test valid values
        valid_values = [[0, 10, 20], [5, 90], [50, 18, 90]]
        for value in valid_values:
            self.assertTrue(
                self.dyn.update_detection_params(imposedpeaks=value))
            self.assertEqual(
                value, self.dyn.get_detection_params().imposedpeaks)

        invalid_values = [[-1, 20], [30, 91]]
        for value in invalid_values:
            with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
                self.assertFalse(
                    self.dyn.update_detection_params(imposedpeaks=value))
                self.assertEqual(cm.output, [
                                 "ERROR:pydynpeak.model.dynpeakexecuter:update_detection_params: elements must be in the interval [0, 90] for imposedpeaks"])

        invalid_values = [5, [[28], [30]]]
        for value in invalid_values:
            with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
                self.assertFalse(
                    self.dyn.update_detection_params(imposedpeaks=value))
                self.assertEqual(cm.output, [
                                 "ERROR:pydynpeak.model.dynpeakexecuter:update_detection_params: A vector expected for imposedpeaks"])

    def test_update_detection_params_imposed_interval(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.update_detection_params`
        method to change value of `imposedpeaks` and `interval`.

        '''
        interval = [20, 80]
        # test valid values
        valid_values = [[0, 10, 20], [5, 60], [50, 18, 55]]
        for value in valid_values:
            self.assertTrue(self.dyn.update_detection_params(
                imposedpeaks=value, interval=interval))
            self.assertEqual(
                value, self.dyn.get_detection_params().imposedpeaks)

        invalid_values = [[-1, 20], [30, 61]]
        for value in invalid_values:
            with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
                self.assertFalse(self.dyn.update_detection_params(
                    imposedpeaks=value, interval=interval))
                self.assertEqual(cm.output, [
                                 "ERROR:pydynpeak.model.dynpeakexecuter:update_detection_params: elements must be in the interval [0, 60] for imposedpeaks"])

    def test_update_detection_params_ignored(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.update_detection_params`
        method to change value of `ignoredpoints`.

        '''
        # test valid values
        valid_values = [[0, 10, 20], [5, 90], [50, 18, 90]]
        for value in valid_values:
            self.assertTrue(
                self.dyn.update_detection_params(ignoredpoints=value))
            self.assertEqual(
                value, self.dyn.get_detection_params().ignoredpoints)

        invalid_values = [[-1, 20], [30, 91]]
        for value in invalid_values:
            with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
                self.assertFalse(
                    self.dyn.update_detection_params(ignoredpoints=value))
                self.assertEqual(cm.output, [
                                 "ERROR:pydynpeak.model.dynpeakexecuter:update_detection_params: elements must be in the interval [0, 90] for ignoredpoints"])

        invalid_values = [5, [[28], [30]]]
        for value in invalid_values:
            with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
                self.assertFalse(
                    self.dyn.update_detection_params(ignoredpoints=value))
                self.assertEqual(cm.output, [
                                 "ERROR:pydynpeak.model.dynpeakexecuter:update_detection_params: A vector expected for ignoredpoints"])

    def test_update_detection_params_ignored_interval(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.update_detection_params`
        method to change value of `ignoredpoints` and `interval`.

        '''
        interval = [20, 80]
        # test valid values
        valid_values = [[0, 10, 20], [5, 60], [50, 18, 55]]
        for value in valid_values:
            self.assertTrue(self.dyn.update_detection_params(
                ignoredpoints=value, interval=interval))
            self.assertEqual(
                value, self.dyn.get_detection_params().ignoredpoints)

        invalid_values = [[-1, 20], [30, 61]]
        for value in invalid_values:
            with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
                self.assertFalse(self.dyn.update_detection_params(
                    ignoredpoints=value, interval=interval))
                self.assertEqual(cm.output, [
                                 "ERROR:pydynpeak.model.dynpeakexecuter:update_detection_params: elements must be in the interval [0, 60] for ignoredpoints"])

    def test_print_detection_params(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.print_detection_params`
        method.

        '''
        # test when no data loaded
        with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
            self.assertIsNone(self.dyn_sans_data.print_detection_params())
            self.assertEqual(cm.output, [
                             "ERROR:pydynpeak.model.dynpeakexecuter:No data available! Please first load data!"])

        # test when data loaded
        stream = StringIO()
        write_to_stream = redirect_stdout(stream)

        with write_to_stream:
            self.dyn.print_detection_params()
            self.assertEqual(f"""
            Detection threshold\t = 0 ng/ml
            Coefficient of variation:
                Range\t = [0, 1000] ng/ml
                CV\t = [0, 0] %
            Global relative threshold\t = 0.2 %
            Local absolute threshold\t = 0.1 %
            Nominal peaks period\t = 40 minutes
            Three point threshold\t = 0.1 %

            Interval\t = []
            Ignored points\t = []
            Imposed peaks\t = []
            """.strip(), stream.getvalue().strip())

    def test_get_detection_params(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.get_detection_params`
        method.

        '''
        # test when no data loaded
        with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
            self.assertIsNone(self.dyn_sans_data.get_detection_params())
            self.assertEqual(cm.output, [
                             "ERROR:pydynpeak.model.dynpeakexecuter:No data available! Please first load data!"])

        # test when data loaded
        params = self.dyn.get_detection_params()
        self.assertEqual(lhpeaks_default_params(), params)

        # test when params is changed outside of dynpeak executer, the value is not affected
        params['detectionthreshold'] = 5
        self.assertEqual(0, self.dyn.get_detection_params().detectionthreshold)

    def test_detect_peaks(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.detect_peaks` method.

        '''
        # test when no data loaded
        with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
            self.assertIsNone(self.dyn_sans_data.detect_peaks())
            self.assertEqual(cm.output, [
                             "ERROR:pydynpeak.model.dynpeakexecuter:No data available! Please first load data!"])

        # test when data loaded, for CaratyData.csv file, 1st series, with default peak detection params
        result_dict = self.dyn.detect_peaks()
        ipi_frame = result_dict["ipi_frame"]

        index = [90, 130, 170, 220, 260, 310, 380, 440,
                 510, 570, 630, 680, 710, 750, 810, 870]
        ipi = pd.Series(data=[60, 40, 40, 50, 40, 50, 70, 60, 70,
                              60, 60, 50, 30, 40, 60, 60], index=index, name="ipi")
        tmed = pd.Series(data=[44.901961, 47.892157, 50.287115, 52.139625, 53.502478, 54.428464, 54.970373, 55.180995,
                               55.113122, 54.819543, 54.353049, 53.76643, 53.112476, 52.443978, 51.813725, 51.27451], index=index, name="median")
        thigh = pd.Series(data=[71.843137, 76.627451, 80.459384, 83.4234, 85.603965, 87.085542, 87.952596, 88.289593, 88.180995,
                                87.711269, 86.964878, 86.026287, 84.979961, 83.910364, 82.901961, 82.039216], index=index, name="t-high")
        tlow = pd.Series(data=[17.960784, 19.156863, 20.114846, 20.85585, 21.400991, 21.771385, 21.988149, 22.072398,
                               22.045249, 21.927817, 21.74122, 21.506572, 21.24499, 20.977591, 20.72549, 20.509804], index=index, name="t-low")

        pd.testing.assert_series_equal(ipi, ipi_frame["ipi"])
        pd.testing.assert_series_equal(tmed, ipi_frame["median"])
        pd.testing.assert_series_equal(thigh, ipi_frame["t-high"])
        pd.testing.assert_series_equal(tlow, ipi_frame["t-low"])

    def test_detect_peaks_interval(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.detect_peaks` method changing
        value of `interval`.

        '''
        # test when data loaded, for CaratyData.csv file, 1st series, with interval
        interval = [10, 71]
        result_dict = self.dyn.detect_peaks(interval=interval)
        ipi_frame = result_dict["ipi_frame"]

        index = [170, 220, 260, 310, 380, 440, 510, 570, 630, 680, 710]
        ipi = pd.Series(data=[40, 50, 40, 50, 70, 60, 70,
                              60, 60, 50, 30], index=index, name="ipi")
        tmed = pd.Series(data=[41.188811, 43.496503, 48.065268, 53.764569, 59.463869, 64.032634,
                               66.340326, 65.25641, 59.65035, 48.391608, 30.34965], index=index, name="median")
        thigh = pd.Series(data=[65.902098, 69.594406, 76.904429, 86.02331, 95.142191, 102.45221,
                                106.14452, 104.41026, 95.440559, 77.426573, 48.559441], index=index, name="t-high")
        tlow = pd.Series(data=[16.475524, 17.398601, 19.226107, 21.505828, 23.785548, 25.613054,
                               26.536131, 26.102564, 23.86014, 19.356643, 12.13986], index=index, name="t-low")

        pd.testing.assert_series_equal(ipi, ipi_frame["ipi"])
        pd.testing.assert_series_equal(tmed, ipi_frame["median"])
        pd.testing.assert_series_equal(thigh, ipi_frame["t-high"])
        pd.testing.assert_series_equal(tlow, ipi_frame["t-low"])

    def test_detect_peaks_imposedpeaks(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.detect_peaks` method changing
        value of `imposedpeaks`.

        '''
        # test when data loaded, for CaratyData.csv file, 1st series, with imposed peaks
        imposedpeaks = [15, 34]
        result_dict = self.dyn.detect_peaks(imposedpeaks=imposedpeaks)
        ipi_frame = result_dict["ipi_frame"]

        index = [90, 130, 150, 170, 220, 260, 310, 340, 380,
                 440, 510, 570, 630, 680, 710, 750, 810, 870]
        ipi = pd.Series(data=[60, 40, 20, 20, 50, 40, 50, 30, 40, 60,
                              70, 60, 60, 50, 30, 40, 60, 60], index=index, name="ipi")
        tmed = pd.Series(data=[44.736842, 40.815273, 38.464912, 37.4742, 37.631579, 38.72549, 40.544376, 42.876677, 45.510836, 48.235294,
                               50.838493, 53.108875, 54.834881, 55.804954, 55.807534, 54.631063, 52.063983, 47.894737], index=index, name="median")
        thigh = pd.Series(data=[71.578947, 65.304438, 61.54386, 59.95872, 60.210526, 61.960784, 64.871001, 68.602683, 72.817337,
                                77.176471, 81.341589, 84.9742, 87.73581, 89.287926, 89.292054, 87.409701, 83.302374, 76.631579], index=index, name="t-high")
        tlow = pd.Series(data=[17.894737, 16.326109, 15.385965, 14.98968, 15.052632, 15.490196, 16.21775, 17.150671, 18.204334, 19.294118,
                               20.335397, 21.24355, 21.933953, 22.321981, 22.323013, 21.852425, 20.825593, 19.157895], index=index, name="t-low")

        pd.testing.assert_series_equal(ipi, ipi_frame["ipi"])
        pd.testing.assert_series_equal(tmed, ipi_frame["median"])
        pd.testing.assert_series_equal(thigh, ipi_frame["t-high"])
        pd.testing.assert_series_equal(tlow, ipi_frame["t-low"])

    def test_detect_peaks_imposedpeaks_interval(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.detect_peaks` method changing
        value of `interval` and `imposedpeaks`.

        '''
        # test when data loaded, for CaratyData.csv file, 1st series, with imposed peaks and interval
        imposedpeaks = [9]
        interval = [10, 70]
        result_dict = self.dyn.detect_peaks(
            imposedpeaks=imposedpeaks, interval=interval)
        ipi_frame = result_dict["ipi_frame"]

        index = [170, 190, 220, 260, 310, 380, 440, 510, 570, 630, 680]
        ipi = pd.Series(data=[40, 20, 30, 40, 50, 70, 60,
                              70, 60, 60, 50], index=index, name="ipi")
        tmed = pd.Series(data=[32.517483, 30.27972, 33.286713, 39.871795, 48.368298, 57.109557,
                               64.428904, 68.659674, 68.135198, 61.188811, 46.153846], index=index, name="median")
        thigh = pd.Series(data=[52.027972, 48.447552, 53.258741, 63.794872, 77.389277, 91.375291,
                                103.08625, 109.85548, 109.01632, 97.902098, 73.846154], index=index, name="t-high")
        tlow = pd.Series(data=[13.006993, 12.111888, 13.314685, 15.948718, 19.347319, 22.843823,
                               25.771562, 27.463869, 27.254079, 24.475524, 18.461538], index=index, name="t-low")

        pd.testing.assert_series_equal(ipi, ipi_frame["ipi"])
        pd.testing.assert_series_equal(tmed, ipi_frame["median"])
        pd.testing.assert_series_equal(thigh, ipi_frame["t-high"])
        pd.testing.assert_series_equal(tlow, ipi_frame["t-low"])

    def test_detect_peaks_ignoredpoints(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.detect_peaks` method changing
        value of `ignoredpoints`.

        '''
        # test when data loaded, for CaratyData.csv file, 1st series, with ignored peaks
        ignoredpoints = [26]
        result_dict = self.dyn.detect_peaks(ignoredpoints=ignoredpoints)
        ipi_frame = result_dict["ipi_frame"]

        index = [90, 130, 170, 220, 270, 310, 380, 440,
                 510, 570, 630, 680, 710, 750, 810, 870]
        ipi = pd.Series(data=[60, 40, 40, 50, 50, 40, 70, 60, 70,
                              60, 60, 50, 30, 40, 60, 60], index=index, name="ipi")
        tmed = pd.Series(data=[45.327657, 48.271414, 50.594501, 52.356854, 53.618407, 54.439095, 54.878854, 54.997618, 54.855323,
                               54.511902, 54.027291, 53.461425, 52.874239, 52.325667, 51.875645, 51.584107], index=index, name="median")
        thigh = pd.Series(data=[72.524252, 77.234262, 80.951202, 83.770966, 85.789451, 87.102553, 87.806167, 87.99619,
                                87.768516, 87.219043, 86.443666, 85.53828, 84.598782, 83.721067, 83.001032, 82.534572], index=index, name="t-high")
        tlow = pd.Series(data=[18.131063, 19.308566, 20.2378, 20.942741, 21.447363, 21.775638, 21.951542, 21.999047, 21.942129,
                               21.804761, 21.610916, 21.38457, 21.149696, 20.930267, 20.750258, 20.633643], index=index, name="t-low")

        pd.testing.assert_series_equal(ipi, ipi_frame["ipi"])
        pd.testing.assert_series_equal(tmed, ipi_frame["median"])
        pd.testing.assert_series_equal(thigh, ipi_frame["t-high"])
        pd.testing.assert_series_equal(tlow, ipi_frame["t-low"])

    def test_detect_peaks_ignoredpoints_interval(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.detect_peaks` method changing
        value of `ignoredpoints` and `interval`.

        '''
        # test when data loaded, for CaratyData.csv file, 1st series, with ignored peaks, with interval
        ignoredpoints = [6]
        interval = [20, 60]
        result_dict = self.dyn.detect_peaks(
            ignoredpoints=ignoredpoints, interval=interval)
        ipi_frame = result_dict["ipi_frame"]

        index = [270, 310, 380, 440, 510, 570]
        ipi = pd.Series(data=[50, 40, 70, 60, 70, 60], index=index, name="ipi")
        tmed = pd.Series(data=[47.619048, 49.047619, 57.619048,
                               66.666667, 69.52381, 59.52381], index=index, name="median")
        thigh = pd.Series(data=[76.190476, 78.47619, 92.190476,
                                106.66667, 111.2381, 95.238095], index=index, name="t-high")
        tlow = pd.Series(data=[19.047619, 19.619048, 23.047619,
                               26.666667, 27.809524, 23.809524], index=index, name="t-low")

        pd.testing.assert_series_equal(ipi, ipi_frame["ipi"])
        pd.testing.assert_series_equal(tmed, ipi_frame["median"])
        pd.testing.assert_series_equal(thigh, ipi_frame["t-high"])
        pd.testing.assert_series_equal(tlow, ipi_frame["t-low"])

    def test_detect_peaks_and_plot(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.detect_peaks_and_plot` method.

        '''
        # test when no data loaded
        with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
            self.assertIsNone(self.dyn_sans_data.detect_peaks_and_plot())
            self.assertEqual(cm.output, [
                             "ERROR:pydynpeak.model.dynpeakexecuter:No data available! Please first load data!"])

        # test when data loaded
        graph = self.dyn.detect_peaks_and_plot()
        self.assertTrue(isinstance(graph, FigureWidget))

    def test_plot_detection_graph(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.plot_detection_graph` method.

        '''
        # test when no data loaded
        with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
            self.assertIsNone(self.dyn_sans_data.plot_detection_graph())
            self.assertEqual(cm.output, [
                             "ERROR:pydynpeak.model.dynpeakexecuter:No data available! Please first load data!"])

        # test when data loaded but no detection
        with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
            self.assertIsNone(self.dyn.plot_detection_graph())
            self.assertEqual(cm.output, [
                             f"ERROR:pydynpeak.model.dynpeakexecuter:No detection is executed for the series at {self.dyn.currentseries}."])

        # test after peak detection
        self.dyn.detect_peaks()

        graph = self.dyn.plot_detection_graph()
        self.assertTrue(isinstance(graph, FigureWidget))

    def test_print_peaks(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.print_peaks` method.

        '''
        # test when no data loaded
        with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
            self.assertIsNone(self.dyn_sans_data.print_peaks())
            self.assertEqual(cm.output, [
                             "ERROR:pydynpeak.model.dynpeakexecuter:No data available! Please first load data!"])

        # test when data loaded but no detection
        with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
            self.assertIsNone(self.dyn.print_peaks())
            self.assertEqual(cm.output, [
                             f"ERROR:pydynpeak.model.dynpeakexecuter:No detection is executed for the series at {self.dyn.currentseries}."])

        # test after peak detection
        self.dyn.detect_peaks()

        table = self.dyn.print_peaks()
        self.assertTrue(isinstance(table, FigureWidget))
        self.assertEqual(2, len(table.data[0].cells.values))
        self.assertEqual(17, len(table.data[0].cells.values[0]))

    def test_get_peak_list(self):
        '''Tests `pydynpeak.model.dynpeakexecuter.get_peak_list` method.

        '''
        # test when no data loaded
        with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
            self.assertIsNone(self.dyn_sans_data.get_peak_list())
            self.assertEqual(cm.output, [
                             "ERROR:pydynpeak.model.dynpeakexecuter:No data available! Please first load data!"])

        # test when data loaded but no detection
        with self.assertLogs('pydynpeak.model.dynpeakexecuter', level='ERROR') as cm:
            self.assertIsNone(self.dyn.get_peak_list())
            self.assertEqual(cm.output, [
                             f"ERROR:pydynpeak.model.dynpeakexecuter:No detection is executed for the series at {self.dyn.currentseries}."])

        # test when data loaded, for CaratyData.csv file, 1st series, with default peak detection params
        index = [30, 90, 130, 170, 220, 260, 310, 380,
                 440, 510, 570, 630, 680, 710, 750, 810, 870]
        peaks = pd.Series(data=[4.6, 3.17, 2.05, 2.04, 1.83, 3.4, 1.3, 1.36, 1.93, 3.25, 3.28, 3.17,
                                3.37, 3.55, 3.71, 3.25, 3.98], index=index, name=f"{sname}{self.dyn.currentseries}")

        self.dyn.detect_peaks()
        pd.testing.assert_series_equal(peaks, self.dyn.get_peak_list())
