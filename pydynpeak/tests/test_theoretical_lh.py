# This file is part of PyDynPeak software
# Copyright (C) 2021, Inria
# 
# PyDynpeak is a Python software derived from:
#          DynPeak (Scilab ATOMS Toolbox) version 2.1.0 (authors:
#	   	   	   	Claire Medigue, Serge Steer, Qinghua Zhang,
#              			Alexandre Vidal, Frédérique Clément
#                               cf. https://atoms.scilab.org/toolboxes/Dynpeak)
#
# PyDynPeak is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# PyDynPeak is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with PyDynPeak.  If not, see <https://www.gnu.org/licenses/>
# 
# Parts of this file are based on work covered by the following copyright
#       and permission notice:
#      """
#      // Copyright (C) 2012 - INRIA - Serge Steer
#      // This file must be used under the terms of the CeCILL.
#      // This source file is licensed as described in the file COPYING, which
#      // you should have received as part of this distribution.  The terms
#      // are also available at
#      // http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#      """
###############################################################################
#
# Authors : Frédérique Clément(+), Hande Gözükan(*), Christian Poli(*)
#
# +=scientific advisor, *=developer
#
###############################################################################

from . import DynPeakTest
import numpy as np

from ..core import theoretical_lh, noised_sampled_lh, fake_randn

def mspike(t):
    # case2 return 15
    #return 15 - 0.0087*t
    return 15


def pspike(t):
    # case 1 return 60
    return 60 - t/50
    #return 60.0


class TestTheoreticalLH(DynPeakTest):
    def test_theoretical_lh(self):
        """
        --> exec('/home/poli/DynPeak/Dynpeak/demos/SamplingEffect.sce',-1)

        --> Mspike="15";

        --> Pspike="60-t/50";

        --> deff("A=mspike(t)","A="+Mspike)

        --> deff("P=pspike(t)","P="+Pspike)

        --> k_hl=0.03465735903; //stiffness coefficient 

        --> Alpha=6;// instantaneous LH clearance rate (1/min)

        --> //Integrate theoritical (continuuous time) model

        --> Tmax=1500;

        --> dt=0.01;//observation step for theoritical model

        --> T=0:dt:Tmax;//observation dates for theoritical model

        --> LH=Theoritical_LH(T,mspike,pspike,k_hl,Alpha);

        --> LH(1:10)
        ans  =
        column 1 to 8
        0.   0.1455632   0.282599   0.411604   0.533046   0.6473654   0.7549769   0.8562713
        column 9 to 10
        0.9516164   1.0413587
        """
        k_hl = 0.03465735903  # stiffness coefficient
        alpha = 6.0
        #Integrate theoretical (continuous time) model
        t_max = 1500
        dt = 0.01 # observation step for theoretical model
        # T=0:dt:Tmax;//observation dates for theoretical model
        time_s = np.arange(0, t_max+dt, dt)
        scilab_lh_10 = np.array([0., 0.1455632, 0.282599, 0.411604,
                                   0.533046, 0.6473654, 0.7549769, 0.8562713, 0.9516164, 1.0413587])
        lh = theoretical_lh(time_s, mspike, pspike, k_hl, alpha)
        np.testing.assert_almost_equal(lh.T[0][:10], scilab_lh_10)
        """
        --> r = 4;
        --> f = 1.5;
        --> b = 0.05;
        --> LHmax=max(LH)
        LHmax  = 
        2.4270792
        --> tsampling=10;
        --> rand("seed", 0)
        --> [Ts,LHs]=NoisedSampled_LH(T,LH, tsampling,r,f,b);
        --> Ts(1:10)
        ans  =
        4.   14.   24.   34.   44.   54.   64.   74.   84.   94.
        --> LHs(1:10)
        ans  =
         column 1 to 6
        2.0332262   1.5663156   1.1149964   0.725462   0.5903304   0.4247071
         column 7 to 10
        2.102175   1.5047909   1.0004595   0.7130054
        """
        self.assertAlmostEqual(np.max(lh), 2.4270792)
        r = 4.0
        f = 1.5
        b = 0.05
        tsampling = 10.0
        scilab_ts_10 = np.array([4., 14., 24., 34., 44., 54., 64., 74., 84., 94.])
        scilab_lhs_10 = np.array([2.0332262, 1.5663156, 1.1149964, 0.725462,
                                      0.5903304, 0.4247071, 2.102175,
                                      1.5047909, 1.0004595, 0.7130054])
        np.random.seed(0)
        ts, lhs = noised_sampled_lh(time_s, lh, tsampling, r, f, b)
        np.testing.assert_equal(ts[:10], scilab_ts_10)
        np.testing.assert_almost_equal(lhs[:10], np.array([2.3330752,
                                            1.4451442, 1.2592613, 0.8171891,
                                            0.5330418, 0.3613926, 2.1601804,
                                            1.4769501, 1.122101 , 0.7709191]))
        ts, lhs = noised_sampled_lh(time_s, lh, tsampling, r, f, b, fake_randn=fake_randn)  
        np.testing.assert_equal(ts[:10], scilab_ts_10)
        np.testing.assert_almost_equal(lhs[:10], scilab_lhs_10)
        
