# This file is part of PyDynPeak software
# Copyright (C) 2021, Inria
# 
# PyDynpeak is a Python software derived from:
#          DynPeak (Scilab ATOMS Toolbox) version 2.1.0 (authors:
#	   	   	   	Claire Medigue, Serge Steer, Qinghua Zhang,
#              			Alexandre Vidal, Frédérique Clément
#                               cf. https://atoms.scilab.org/toolboxes/Dynpeak)
#
# PyDynPeak is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# PyDynPeak is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with PyDynPeak.  If not, see <https://www.gnu.org/licenses/>
# 
# Parts of this file are based on work covered by the following copyright
#       and permission notice:
#      """
#      // Copyright (C) 2012 - INRIA - Serge Steer
#      // This file must be used under the terms of the CeCILL.
#      // This source file is licensed as described in the file COPYING, which
#      // you should have received as part of this distribution.  The terms
#      // are also available at
#      // http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#      """
###############################################################################
#
# Authors : Frédérique Clément(+), Hande Gözükan(*), Christian Poli(*)
#
# +=scientific advisor, *=developer
#
###############################################################################

from . import DynPeakTest
import numpy as np
import os
import os.path
import pandas as pd
from ..core import lhpeaks, make_struct

_ddir = os.path.join(os.path.dirname(__file__), '..', 'data')
xls_file = os.path.join(_ddir, 'Profils_Endocriniens_Vaches_Greleraie.xls')
sheets = pd.read_excel(xls_file, sheet_name=None)

selections = [[[1, 36], [38, 73]],
                  [[1, 36], [38, 71]],
                  [[1, 36], [38, 74]],
                  [[1, 34], [37, 70]],
                  [[1, 37], [38, 73]],
                  [[1, 36], [38, 74]],
                  [[1, 36]],
                  [[1, 34], [38, 73]]]
#import pdb;pdb.set_trace()
p = make_struct("globalreltol", 0.20,
           "localabstol", 0.10,
           "threepointtol", 0.10,
           "uncertainty", [[0.1, 0.2, 0.4, 0.8, 1.6, 3.2, 6.4],
                               [0.078, 0.053, 0.044, 0.05, 0.042, 0.042, 0.034]],
           "period", 40,
           "imposedpeaks", [],
           "ignoredpoints", [],
           "selection", [])


refs = [[7,11,16,20,24,28,31],
            [47,51,55,60,64,68,72],
            #[5,8,11,19,26,33],
            [5,11,19,26,33],          
            #[39,46,50,53,60,63,66],
            [46,53,60,66],
            [6,10,15,19,24,29,34],
            [43,48,52,57,61,66,70],
            #[9,14,17,22,26,31],
            [6,9,13,17,22,26,31],
            #[41,50,56,60,64],
            [40,43,51,56,60,64],
            [5,11,17,24,30,36],
            #[46,50,54,60,68],
            [46,54,60,68],
            #[8,14,21,27,34],
            [2,8,14,21,27,34],
            #[45,50,53,56,61,68,73],
            [45,50,56,61,68,73],
            [5,11,17,23,29,34],
            [4,10,17,23,30],
            #[39,43,48,51,54,60,67,73]
            [43,48,54,60,67,73]]

class TestCowsOpt(DynPeakTest):
    def test_with_optimized_parameters(self):
        count = 0
        for k, df in zip(range(8), sheets.values()):
            df.fillna(value=0,inplace=True)
            t = df.iloc[1:87, 0].values * 60
            lh = df.iloc[1:87, 2].values
            sel = selections[k]
            for s0, s1 in sel:
                s = [s0-1, s1]
                p.selection = s
                #if count==14:
                #    import pdb;pdb.set_trace()
                peakind = lhpeaks(lh, t, p)
                #self.assertEqual(peakind, refs[count])
                print("Testing: ", refs[count], " i.e. ", np.array(refs[count])-1, count)
                np.testing.assert_equal(peakind, np.array(refs[count])-1)
                print(peakind, " OK")
                count += 1                
