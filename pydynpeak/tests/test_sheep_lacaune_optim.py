# This file is part of PyDynPeak software
# Copyright (C) 2021, Inria
# 
# PyDynpeak is a Python software derived from:
#          DynPeak (Scilab ATOMS Toolbox) version 2.1.0 (authors:
#	   	   	   	Claire Medigue, Serge Steer, Qinghua Zhang,
#              			Alexandre Vidal, Frédérique Clément
#                               cf. https://atoms.scilab.org/toolboxes/Dynpeak)
#
# PyDynPeak is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# PyDynPeak is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with PyDynPeak.  If not, see <https://www.gnu.org/licenses/>
# 
# Parts of this file are based on work covered by the following copyright
#       and permission notice:
#      """
#      // Copyright (C) 2012 - INRIA - Serge Steer
#      // This file must be used under the terms of the CeCILL.
#      // This source file is licensed as described in the file COPYING, which
#      // you should have received as part of this distribution.  The terms
#      // are also available at
#      // http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#      """
###############################################################################
#
# Authors : Frédérique Clément(+), Hande Gözükan(*), Christian Poli(*)
#
# +=scientific advisor, *=developer
#
###############################################################################

from . import DynPeakTest
import numpy as np
import os
import os.path
import pandas as pd
from ..core import lhpeaks
from ..core.utils import make_struct



_ddir = os.path.join(os.path.dirname(__file__), '..', 'data')
csv_file = os.path.join(_ddir, 'LacauneData.csv')
df = pd.read_csv(csv_file, sep='\t', header=None)

#import pdb;pdb.set_trace()
# test with optimization

p = make_struct(detectionthreshold=0,
         globalreltol=0.20,
         localabstol=0.10,
         threepointtol=0.10,
         uncertainty=[[0.1,0.2,0.4,0.8,1.6,3.2,6.4],[0.078,0.053,0.044,0.05,0.042,0.042,0.034]],
         period=40,
         imposedpeaks=[],
         ignoredpoints=[],
         selection=[]);

refs = [
    [6,16,25,32,37,43,52,58,61,72,80,84,91,97,101,108,115,124,132,135,140],
    [14,31,53,77,82,106,142],
    [9,18,27,36,46,56,65,74,83,91,100,108,117,124,129,135,139,143],
    [3,7,12,19,24,31,41,49,58,62,70,75,82,92,101,107,113,118,125,135,142],
    [9,18,25,35,40,45,54,58,69,80,92,95,105,118,129,139],   
    [3,11,21,27,31,35,42,47,54,58,63,68,74,78,82,86,90,95,99,103,108,114,118,122,125,129,133,137,140],
    [3,9,17,24,40,46,52,58,63,68,73,79,83,86,89,92,97,103,107,111,115,120,124,127,130,133,138,141],
    [2,7,13,20,30,35,42,47,53,61,67,75,84,93,102,112,121,131,139],
    [2,6,11,16,22,30,33,40,45,52,57,61,66,71,76,80,86,91,97,103,105,108,114,116,122,125,132,143],
    [8,12,25,36,48,58,69,80,89,102,119,140]
    ]

T = df.index.values*10

class TestSheepCaraty(DynPeakTest):
    def test_sheep_caraty_default(self):
        for col in df.columns:
            if col == 7: continue
            print("Testing: ", col)
            #import pdb;pdb.set_trace()
            peakind = lhpeaks(df.loc[:, col], T, p)
            np.testing.assert_equal(peakind, np.array(refs[col])-1)
            #self.assertTrue(np.all(peakind==np.array(refs[col])))

if __name__ == '__main__':
    unittest.main()
