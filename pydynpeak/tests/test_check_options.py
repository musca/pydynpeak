# This file is part of PyDynPeak software
# Copyright (C) 2021, Inria
# 
# PyDynpeak is a Python software derived from:
#          DynPeak (Scilab ATOMS Toolbox) version 2.1.0 (authors:
#	   	   	   	Claire Medigue, Serge Steer, Qinghua Zhang,
#              			Alexandre Vidal, Frédérique Clément
#                               cf. https://atoms.scilab.org/toolboxes/Dynpeak)
#
# PyDynPeak is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# PyDynPeak is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with PyDynPeak.  If not, see <https://www.gnu.org/licenses/>
# 
# Parts of this file are based on work covered by the following copyright
#       and permission notice:
#      """
#      // Copyright (C) 2012 - INRIA - Serge Steer
#      // This file must be used under the terms of the CeCILL.
#      // This source file is licensed as described in the file COPYING, which
#      // you should have received as part of this distribution.  The terms
#      // are also available at
#      // http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#      """
###############################################################################
#
# Authors : Frédérique Clément(+), Hande Gözükan(*), Christian Poli(*)
#
# +=scientific advisor, *=developer
#
###############################################################################

from . import DynPeakTest
from ..core import lhpeaks_check_options, make_struct

p = make_struct("globalreltol",0.20,
            "localabstol",0.10,
            "threepointtol",0.10,
            "uncertainty",[[0,1000], [0, 0]],
            "period",40,
            "imposedpeaks",[],
            "ignoredpoints",[],
            "selection",[])

class TestCheckOpts(DynPeakTest):
    def test_simple(self):
        lhpeaks_check_options(p, 0, "foo")
        p.globalreltol = -1
        with self.assertRaises(ValueError):
            lhpeaks_check_options(p, 0, "foo")
        p.globalreltol = 0.2
        p.localabstol = -1
        with self.assertRaises(ValueError):
            lhpeaks_check_options(p, 0, "foo")
            
