# This file is part of PyDynPeak software
# Copyright (C) 2021, Inria
# 
# PyDynpeak is a Python software derived from:
#          DynPeak (Scilab ATOMS Toolbox) version 2.1.0 (authors:
#	   	   	   	Claire Medigue, Serge Steer, Qinghua Zhang,
#              			Alexandre Vidal, Frédérique Clément
#                               cf. https://atoms.scilab.org/toolboxes/Dynpeak)
#
# PyDynPeak is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# PyDynPeak is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with PyDynPeak.  If not, see <https://www.gnu.org/licenses/>
# 
# Parts of this file are based on work covered by the following copyright
#       and permission notice:
#      """
#      // Copyright (C) 2012 - INRIA - Serge Steer
#      // This file must be used under the terms of the CeCILL.
#      // This source file is licensed as described in the file COPYING, which
#      // you should have received as part of this distribution.  The terms
#      // are also available at
#      // http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#      """
###############################################################################
#
# Authors : Frédérique Clément(+), Hande Gözükan(*), Christian Poli(*)
#
# +=scientific advisor, *=developer
#
###############################################################################

from . import DynPeakTest
import numpy as np
import os
import os.path
import pandas as pd
from ..core import lhpeaks, make_struct



_ddir = os.path.join(os.path.dirname(__file__), '..', 'data')
csv_file = os.path.join(_ddir, 'CaratyData.csv')
df = pd.read_csv(csv_file, sep='\t', header=None)

#import pdb;pdb.set_trace()
p = make_struct("detectionthreshold",0,
                "globalreltol",0.20,
                "localabstol",0.10,
                "threepointtol",0.10,
                "uncertainty",[[0,1000],[0, 0]],
                "period",40,
                "imposedpeaks",[],
                "ignoredpoints",[],
                "selection",[])

refs = [
    [4,10,14,18,23,27,32,39,45,52,58,64,69,72,76,82,88], 
    [3,66,76,85],
    [5,11,14,18,21,26,30,44,52,62,71,81],
    [3,6,14,19,24,30,47,60,64,68,71,77,82,91],   
    [3,11,14,18,23,28,37,42,46,52,58,63,69,73,79,85], 
    [4,10,23,29,45,57,64,70,78],
    [5,11,14,18,22,25,28,31,33,36,40,43,46,50,53,58,64,68,73,77,81,85,90],
    [7,15,19,77], 
    [6,10,16,23,30,45,52,58,66,71,77,83]
    ]

T = df.index.values*10

class TestSheepCaraty(DynPeakTest):
    def test_sheep_caraty_default(self):
        for col in df.columns:
            print("Testing: ", col)
            #import pdb;pdb.set_trace()
            peakind = lhpeaks(df.loc[:, col], T, p)
            np.testing.assert_equal(peakind, np.array(refs[col])-1)
            #self.assertTrue(np.all(peakind==np.array(refs[col])))
