# This file is part of PyDynPeak software
# Copyright (C) 2021, Inria
# 
# PyDynpeak is a Python software derived from:
#          DynPeak (Scilab ATOMS Toolbox) version 2.1.0 (authors:
#	   	   	   	Claire Medigue, Serge Steer, Qinghua Zhang,
#              			Alexandre Vidal, Frédérique Clément
#                               cf. https://atoms.scilab.org/toolboxes/Dynpeak)
#
# PyDynPeak is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# PyDynPeak is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with PyDynPeak.  If not, see <https://www.gnu.org/licenses/>
# 
# Parts of this file are based on work covered by the following copyright
#       and permission notice:
#      """
#      // Copyright (C) 2012 - INRIA - Serge Steer
#      // This file must be used under the terms of the CeCILL.
#      // This source file is licensed as described in the file COPYING, which
#      // you should have received as part of this distribution.  The terms
#      // are also available at
#      // http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#      """
###############################################################################
#
# Authors : Frédérique Clément(+), Hande Gözükan(*), Christian Poli(*)
#
# +=scientific advisor, *=developer
#
###############################################################################

from .lhpeaks_default_params import lhpeaks_default_params
from functools import singledispatch
import numpy as np
from .utils import fieldnames, make_struct

valid=["detectionthreshold","globalreltol", "localabstol", "threepointtol",
           "uncertainty", "period", "imposedpeaks",
           "ignoredpoints", "selection", "interval"]

dt = {k:type(k, (), {}) for k in valid}
dti = {k:t() for (k, t) in dt.items()}

def _n(arg):
    return arg.__class__.__name__

def chk_real_positive(arg, value, ns, fname):
    if isinstance(value, int):
        value = float(value)
    if not isinstance(value, float):
        raise ValueError(f"{fname}: A real scalar expected for {_n(arg)}")
    if value <= 0:
        raise ValueError(f"{fname}: Positive value expected for {_n(arg)}")
    return value

def chk_real_non_negative(arg, value, ns, fname):
    if isinstance(value, int):
        value = float(value)
    if not isinstance(value, float):
        raise ValueError(f"{fname}: A real scalar expected for {_n(arg)}")
    if value <0:
        raise ValueError(f"{fname}: Non negative value expected for {_n(arg)}")
    return value

def chk_real_interval_0_1(arg, value, ns, fname):
    if isinstance(value, int):
        value = float(value)    
    if not isinstance(value, float):
        raise ValueError(f"{fname}: A real scalar expected for {_n(arg)}")
    if value<=0 or value>1:
        raise ValueError(f"{fname}: 0<value<=1 expected for {_n(arg)}")
    return value

@singledispatch
def checkopt(arg, value, ns, fname):
    raise ValueError("Not implemented")

@checkopt.register(dt["detectionthreshold"])
def _(arg, value, ns, fname):
    return chk_real_non_negative(arg, value, ns, fname)

@checkopt.register(dt["globalreltol"])
@checkopt.register(dt["threepointtol"])
def _(arg, value, ns, fname):
    return chk_real_interval_0_1(arg, value, ns, fname)

@checkopt.register(dt["localabstol"])
@checkopt.register(dt["period"])
def _(arg, value, ns, fname):
    return chk_real_positive(arg, value, ns, fname)

@checkopt.register(dt["uncertainty"])
def _(arg, value, ns, fname):
    if isinstance(value, list):
        value = np.array(value, dtype=np.float64)
    if not (isinstance(value, np.ndarray) and
                np.issubdtype(value.dtype, np.floating) and
                value.ndim == 2 and
                value.shape[0] == 2):
        raise ValueError(f"{fname}: Real 2 rows array expected for {_n(arg)}")
    if np.any(value[0]<0):
        raise ValueError(f"{fname}: first row elements must be non negative for {_n(arg)}")
    if not np.all(np.diff(value[0])>0):
        raise ValueError(f"{fname}: Increasing order expected on first row elements for {_n(arg)}")
    if np.any(value[1]<0) or np.any(value[1]>1):
        raise ValueError(f"{fname}: second row elements must be in the interval [0, 1] for {_n(arg)}")
    return value

@checkopt.register(dt["imposedpeaks"])
@checkopt.register(dt["ignoredpoints"])
def _(arg, value, ns, fname):
    value = np.array(value, dtype=np.int64) # always cf. scilab: "params.imposedpeaks=imposedpeaks(:)"
    if value.ndim != 1:
        raise ValueError(f"{fname}: A vector expected for {_n(arg)}")
    if value.size and (np.min(value) <0 or np.max(value)>ns-1):
        raise ValueError(f"{fname}: elements must be in the interval [0, {ns-1}] for {_n(arg)}")
    return value

@checkopt.register(dt["selection"])
def _(arg, value, ns, fname):
    value = np.array(value, dtype=np.int64) # always cf. scilab: params.selection=selection(:)
    if not (value.shape in ((0,), (2,)) and np.issubdtype(value.dtype, np.integer)):
        raise ValueError(f"{fname}: (0,) or (2,) dim. vector expected for {_n(arg)}")
    if value.size and (value[0]<0 or value[1]>ns):
        raise ValueError(f"{fname}: selection elements must be in the interval [0, {ns}] for {_n(arg)}")
    if value.size and (value[0] >= value[1]):
        raise ValueError(f"{fname}: second element must be greater than the first one for {_n(arg)}")
    return value

@checkopt.register(dt["interval"])
def _(arg, value, ns, fname):
    value = np.array(value, dtype=np.int64) 
    if not (value.shape in ((0,), (2,)) and np.issubdtype(value.dtype, np.integer)):
        raise ValueError(f"{fname}: (0,) or (2,) dim. vector expected for {_n(arg)}")
    if value.size and (value[0]<0):
        raise ValueError(f"{fname}: interval lower bound must be in the interval greater than or equal to 0 for {_n(arg)}")
    if value.size and (value[0] >= value[1]):
        raise ValueError(f"{fname}: second element must be greater than the first one for {_n(arg)}")
    return value


def lhpeaks_check_options(p, ns, fname):
    if not hasattr(p, "_attributes"):
        p = make_struct(p)
    params = lhpeaks_default_params()
    optnames = fieldnames(p)
    for nm in optnames:
        val = checkopt(dti[nm], getattr(p, nm), ns, fname)
        setattr(params, nm, val)
    return params
