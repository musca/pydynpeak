# This file is part of PyDynPeak software
# Copyright (C) 2021, Inria
# 
# PyDynpeak is a Python software derived from:
#          DynPeak (Scilab ATOMS Toolbox) version 2.1.0 (authors:
#	   	   	   	Claire Medigue, Serge Steer, Qinghua Zhang,
#              			Alexandre Vidal, Frédérique Clément
#                               cf. https://atoms.scilab.org/toolboxes/Dynpeak)
#
# PyDynPeak is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# PyDynPeak is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with PyDynPeak.  If not, see <https://www.gnu.org/licenses/>
# 
# Parts of this file are based on work covered by the following copyright
#       and permission notice:
#      """
#      // Copyright (C) 2012 - INRIA - Qinghua ZHANG - Serge Steer
#      // This file must be used under the terms of the CeCILL.
#      // This source file is licensed as described in the file COPYING, which
#      // you should have received as part of this distribution.  The terms
#      // are also available at
#      // http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#      """
###############################################################################
#
# Authors : Frédérique Clément(+), Hande Gözükan(*), Christian Poli(*)
#
# +=scientific advisor, *=developer
#
###############################################################################

import numpy as np
from .lhpeaks_default_params import lhpeaks_default_params
from .lhpeaks_check_options import lhpeaks_check_options
from .cv_lh import cv_lh
from .lh_lower import lh_lower
from .lh_upper import lh_upper
from .lhpeaks_refine import lhpeaks_refine
from .utils import argmin_, argmax_, find_, find_1st, find_1st_as_array, min_, max_

def lhpeaks(sig, t, p=None):
    """
    function  peakind  = lhpeaks(sig,t, p)
    //LHPEAK: LH peak detection
    //peakind: indexes of detected peaks
    //
    //sig   : sampled LH signal 
    //t     : vector of sample dates or time step
    //p     : optionnal parameter struct
    //
    //Glossary---------------------------------------------------------------
    //   - The height of a peak is the difference of the signal value at the
    //     peak location and the minimal signal value
    //   - The  magnitude of a peak
    //   - The  relative magnitude of a peak  
    //Method-----------------------------------------------------------------
    // lhpeaks uses a multiple pass method to estimates the peak locations. 
    
    // The first pass tries to sequentially find peaks that have  great
    //     heights within a user specified time range consistent with the
    //     maximal frequency (see firstPeaks).
  
    // The second pass removes peaks whose height is small in comparison with
    //     the median of the detected pulse heights: For each pulse, if, for a
    //     given pulse, the ratio between its own height and the median height
    //     is less than a threshold parameter (globalreltol), the pulse is
    //     removed (see delGlobalRelSmallPeaks)

    // The third pass removes the peaks which are not sufficiently high with
    //     respect to the magnitude of the peaks immediately on the left and on the
    //     right. More precisely if M is the peak magnitude, Ml and Mr the magnitude
    //     of the left and right peaks, ml the minimum value of the signal in
    //     the left interpeak interval and mr the minimum value of the signal in
    //     the rigth inter peak interval, if
    //     (M-Ml)*(M-Mr)<(Ml-min(ml,mr))*(Mr-min(ml,mr))*localreltol the peak is
    //     rejected. See delLocalRelSmallPeaks.

    // The fourth pass looks for local maxima located between two consecutive
    //     peaks. These local maxima are considered to be peaks if they fullfill
    //     third pass criterion. See recoverMissedPeaks

    // The fifth pass removes peaks whose height is too small with respect to the
    //     minimum signal value in the left and right inter peak intervals.
    //     More precisely if h is the peak height, ml the minimum value of the
    //     signal in the left interpeak interval and mr the minimum value of the
    //     signal in the rigth inter peak interval, if
    //     (h-ml)*(h-mr)<localabstol^2 the peak is rejected. See
    //     delLocalAbsSmallPeaks

    // The sixth pass removes peaks which are formed by only 3 points : the
    //     peaks for which the immediately preceding and following samples are
    //     local minima of the time series. See delThreePointPeaks.
    // 

    // Finally the algorithm may optionnaly take advantage of the uncertainty on
    //     the signal values to make the inter peak interval length evolve more
    //     regularily. The previous steps have set for each found peak a
    //     property which indicates if a given peak is detected for all the
    //     uncertainty range or not. The alternatePeakPositions which searchs
    //     the peaks that are almost flat with respect to uncertainty.
    //     lhpeaks_refine computes the "optimal" peaks locations over all
    //     possible choices of peak presence or absence and possible peak
    //     displacements.

    //Revision history:------------------------------------------------------
    //   Jan 2011: Initial version (Qinghua ZHANG, INRIA)
    //   Feb 2011: (Qinghua ZHANG, INRIA)
    //      - three point peaks detection included
    //   July 2012:   (Serge Steer, INRIA)
    //      - argument checking added
    //      - a struct used to handle parameters
    //      - modified to managed non regularily discretized signals.
    //      - IPI regularity optimisation based on data uncertainty
    // Reference:------------------------------------------------------------
    //   "DynPeak : An algorithm for pulse detection and frequency analysis in
    //      hormonal time series", Alexandre VIDALI, Qinghua ZHANG, Claire
    //      MEDIGUE, Stéphane FABRE, Frédérique CLEMENT, Dec 2011  
    """
    sig = np.array(sig, dtype=np.float64)
    n_size = sig.size
    if sig.ndim != 1:
        raise ValueError("Real vector expected for 'sig'")
    t = np.array(t, dtype=np.float64)
    if t.ndim != 1:
        raise ValueError("Real vector expected for 't'")
    if t.size == 1:
        t = np.fromiter((e*t[0] for e in range(n_size)), np.float64)
    if t.size != sig.size:
        raise ValueError("Same size expected for 'sig' and 't'")
    if p is None:
        params = lhpeaks_default_params()
    else:
        params = lhpeaks_check_options(p, sig.size, "lhpeaks")
    period = params.period
    imposedpeaks = params.imposedpeaks
    if params.ignoredpoints.size:
        sig[params.ignoredpoints] = np.nan
    sel = params.selection
    if sel.size:
        sig = sig[sel[0]:sel[1]]
        t = t[sel[0]:sel[1]]
        if imposedpeaks.size:
            del_ix = find_(np.logical_or(imposedpeaks<sel[0], imposedpeaks>sel[1]))
            imposedpeaks = np.delete(imposedpeaks, del_ix)
            imposedpeaks -= sel[0]
    if params.ignoredpoints.size:
        k = find_(np.isnan(sig))
        sig = np.delete(sig, k)
        t = np.delete(t, k)
        for j in range(imposedpeaks.size):
            imposedpeaks[j] -= find_(k<imposedpeaks[j]).size
    localreltol = params.globalreltol**2
    sig = np.maximum(sig, params.detectionthreshold)
    _, indmax = argmax_(sig)
    #smax = sig[indmax]
    peakind = np.array([indmax]) # assume a peak at the max signal value
    if indmax < n_size: #sig.size-1: # TOASK current size or initial size (i.e. n_size or N)
        # search peaks between the max signal value and the end of the signal
        ind = first_peaks(sig[indmax:], t[indmax:], period, params.uncertainty)
        if ind.size:
            peakind = np.concatenate([peakind, indmax+ind])
    if indmax > 0:
        ind = first_peaks(sig[indmax::-1], -np.cumsum(np.insert(np.diff(t[indmax::-1]),0,0.)), period, params.uncertainty)
        if ind.size:
            peakind = np.concatenate([indmax-ind[::-1], peakind])
    q_ = np.ones(peakind.size)
    test = True
    if test:
        # Recover missed peaks thld2
        peakind, q_ = recover_missed_peaks(q_, sig, peakind, localreltol, params.uncertainty)
        # if debug_mode>0 then debug_mode=dynpeak_show_debug("Recovered",peakind,Q);end
    # Remove too small peaks - global median; thld1
    peakind, q_ = del_global_rel_small_peaks(q_, sig, peakind, params.globalreltol, params.uncertainty)
    #if  debug_mode>0 then debug_mode=dynpeak_show_debug("Global Median",peakind,Q),end
    # Remove too small peaks - local relative magnitude; thld2
    peakind, q_ = del_local_rel_small_peaks(q_, sig, peakind, localreltol, params.uncertainty)
    # if  debug_mode>0 then debug_mode=dynpeak_show_debug("Local Rel Mag",peakind,Q),end
    # (Added Dec 2010) Remove too small peaks - local absolute magnitude  AbsMagThreshold
    peakind, q_ = del_local_abs_small_peaks(q_, sig, peakind, params.localabstol, params.uncertainty)
    # if debug_mode>0 then debug_mode=dynpeak_show_debug("Local Abs Mag",peakind,Q),end
    peakind, q_ = del_three_point_peaks(q_, sig, t, peakind, params.threepointtol, params.uncertainty)
    # if debug_mode>0 then  debug_mode=dynpeak_show_debug("Three points",peakind,Q),end
    # [peakind,Q] = delOutliers(sig,t, peakind,params.threepointtol,params.uncertainty);
    # if debug_mode>0 then  debug_mode=dynpeak_show_debug("Outliers",peakind,Q),end
    km, alt = alternate_peak_positions(sig, peakind, params.uncertainty)
    # if debug_mode>0 then debug_mode=dynpeak_show_debug("Alternate position",peakind,Q),end
    for k in range(imposedpeaks.size-1, -1, -1): # reversed range
        ipk = imposedpeaks[k]
        j = find_(peakind == ipk)
        if j.size == 0:
            l = find_1st(peakind > ipk)
            peakind = np.concatenate([peakind[:l], np.array([ipk]), peakind[l:]])
            q_ = np.concatenate([q_[:l], np.array([1.]), q_[l:]])
            l = find_(peakind[km] > ipk)
            if l.size:
                km[l] += 1
        else:
            q_[j] = 1.
            i = find_(km==j) if km.shape==j.shape else None
            if i and i.size:
                # TOASK: why alt(i)=null() since dim(i)>1
                #alt = np.delete(alt, i)
                apt.pop(i)
                km  = np.delete(alt, i)
    p = peakind
    peakind, q_ = lhpeaks_refine(peakind, t, km, alt, q_)
    # if debug_mode>0 then debug_mode=dynpeak_show_debug("Refine",peakind,Q),end
    # update index to take care of ignored points
    ignoredpoints=params.ignoredpoints
    if params.selection.size:
        peakind += params.selection[0]
    if ignoredpoints.size:
        ignoredpoints.sort() # in place
        for k in range(ignoredpoints.size-1, -1, -1):
            l_ = find_(peakind>=ignoredpoints[k])
            if l_.size:
                peakind[l_] += 1
    # if debug_mode>0 then dynpeak_end_debug();end    
    return peakind

def del_global_rel_small_peaks(q_, sig, peakind, rthld1, uncertainty):
    """
    function [peakind,Q] = delGlobalRelSmallPeaks(sig, peakind,rthld1,uncertainty)
    // Remove too small peaks - global median;
    """
    q_ = np.array(q_) # copy ?
    qs = np.array(q_)
    smin = sig.min()
    hpeak = sig[peakind]
    pmed = np.median(hpeak-smin)
    # peaks to be kept if upper position is retained
    upper = lh_upper(hpeak, uncertainty) - smin>rthld1*pmed
    # peaks to be kept if lower position is retained
    lower = lh_lower(hpeak, uncertainty) - smin>rthld1*pmed
    # upper option peaks are considered probable
    q_[upper] = 0
    # lower option peaks are considered certain if they were not
    # previously tagged as uncertain
    # Q(lower)=ones(Q(lower)).*Qs(lower)
    q_[lower] = np.ones(q_[lower].size)*qs[lower]
    # upper option peaks  includes upper option peaks
    # so all upper option peaks are retained
    peakind = peakind[upper]
    q_ = q_[upper]
    return peakind, q_

def del_local_rel_small_peaks(q_, sig, peakind, rthld2, uncertainty):
    """
    function [peakind,Q] = delLocalRelSmallPeaks(sig, peakind,rthld2,uncertainty)
    // Remove too small peaks - local relative magnitude;
    """
    q_ = np.array(q_) # copy ?
    np_ = peakind.size
    pmask = np.ones(np_, dtype=bool)
    for k in range(1, np_-1):
        smin_left = min_(sig[peakind[k-1]+1:peakind[k]]) # min on the left
        smin_right = min_(sig[peakind[k]+1:peakind[k+1]]) # min on the right
        # handle case of two consecutive peaks
        if smin_left is None:
            smin = smin_right
        elif smin_right is None:
            smin = smin_left
        else:
            smin = min(smin_left, smin_right)
        cv=cv_lh(sig[peakind[k-1:k+2]],uncertainty)
        # hypothesis that maximize peak height
        sk = sig[peakind[k]] + cv[1]
        sk_left = sig[peakind[k-1]]-cv[0]
        sk_right = sig[peakind[k+1]]-cv[2]
        to_remove_upper=(sk-smin_left)*(sk-smin_right)< rthld2*(sk_left-smin)*(sk_right-smin)
        pmask[k] = (not to_remove_upper)
        # hypothesis that minimize peak height
        sk= sig[peakind[k]] - cv[1]
        sk_left = sig[peakind[k-1]] + cv[0]
        sk_right = sig[peakind[k+1]] + cv[2]
        to_remove_lower=(sk-smin_left)*(sk-smin_right)< rthld2*(sk_left-smin)*(sk_right-smin)
        if to_remove_lower:
            q_[k] = False
    return peakind[pmask], q_[pmask]
    
def recover_missed_peaks(q_, sig, peakind, rthld2, uncertainty):
    """
    function  [peakind,Q] = recoverMissedPeaks(sig, peakind,rthld2,uncertainty)
    """
    q_ = np.array(q_) # copy ?
    # Recover missed peaks
    for k in range(10):
        pind, q1 = missed_peaks(sig, peakind, rthld2, uncertainty)
        if not pind.size:
            break
        peakind = np.concatenate([peakind, pind])
        ind = np.argsort(peakind)
        peakind = peakind[ind]
        q_ = np.concatenate([q_, q1])
        q_ = q_[ind]
    return peakind, q_

def missed_peaks(sig, peakind, rthld2, uncertainty):
    """
    function [pind2,Q] = missedPeaks(sig, peakind,rthld2,uncertainty)
    //search for a missed peak between two peaks detected by the firstPeak pass
    //an intermediate  peak candidate is retained if it is relatively "high enough"
    //compared to the two surrounding peaks.
    """
    np_ = peakind.size
    pind2 = []
    q_ = []
    # loop on interpeak intervals
    for k in range(np_-1):
        sig_seg=sig[peakind[k]:peakind[k+1]+1] 
        if sig_seg.size < 5:
            continue
        cv = cv_lh(sig_seg, uncertainty)
        d = np.diff(sig_seg)
        # -------find local max (d(i)>0&d(i+1)<0) in sig_seg -----------------
        # maximize local max detection using uncertainty
        upper = np.logical_and(d[:-1]+cv[:-2]>0, d[1:]-cv[1:-1]<0)
        upper[[0, -1]] = False # avoid double detection of "flat peaks" on boundary peaks
        # minimize local max detection  using uncertainty
        lower = np.logical_and(d[:-1]-cv[:-2]>0, d[1:]+cv[1:-1]<0)
        p = upper # &lower
        if not np.any(p):
            continue
        kp = find_(upper|lower) + 1
        # -------compute high criterion for each local max-------------------
        crtu = np.zeros(kp.size)
        crtl = np.array(crtu)
        _ff = find_1st_as_array
        for i, kpi in enumerate(kp):
            # kminlr index of the 2 local minima on the left and the right of the
            # current local max
            si = sig_seg[kpi] # intermediate peak candidate
            cvi = cv[kpi]  # uncertainty for intermediate peak candidate
            # maximized peak criterion
            dp = d - cv[:-1];
            kminlr = kpi+np.concatenate([-_ff(dp[kpi-1::-1]<0), _ff(dp[kpi:]>0)]) # 2 elts
            slr = sig_seg[kminlr]
            cvlr = cv[kminlr] # uncertainty for  signal local min values
            crtu[i] = np.prod((si+cvi)-(slr-cvlr)) # maximized criterion
            # minimized peak criterion
            dp = d+cv[:-1]
            kminlr = kpi + np.concatenate([-_ff(dp[kpi-1::-1]<0), _ff(dp[kpi:]>0)]);
            slr = sig_seg[kminlr]
            cvlr = cv[kminlr] # uncertainty for  signal local min values
            crtl[i] = np.prod((si-cvi)-(slr+cvlr)) # minimized criterion
        # absolute minimum within the interval
        smin, kmin = argmin_(sig_seg)
        # -------select local max that has the maximum  criterion
        spmax, i = argmax_(crtl) # try minimized criterion
        # compare with peaks k and k+1 criterion
        if spmax > rthld2*(sig_seg[0]-smin)*(sig_seg[-1]-smin):
            pind2.append(peakind[k]+kp[i])
            q_.append(1) # mark this peak as certain
        else:
            _, i = argmax_(crtu) # try maximized criterion
            spmax = crtu[i]
            if spmax > rthld2*(sig_seg[0]+cv[0]-smin+cv[kmin])*(sig_seg[-1]+cv[-1]-smin+cv[kmin]):
                pind2.append(peakind[k]+kp[i])
                q_.append(0) # mark this peak as removable
    return np.array(pind2), np.array(q_)

def del_local_abs_small_peaks(q_, sig, peakind, athld, uncertainty):
    """
    function [peakind,Q] = delLocalAbsSmallPeaks(sig, peakind,athld,uncertainty)
    // (Added Dec 2010) Remove too small peaks - local absolute magnitude
    """
    q_ = np.array(q_) # copy ?
    np_ = peakind.size
    if np_<2:
        return peakind, q_
    pmasku = np.ones(np_, dtype=bool);
    pmaskl = np.ones(np_, dtype=bool);
    # First peak
    # hypothesis that maximize peak height
    smin_right = lh_lower(sig[peakind[0]+1:peakind[1]], uncertainty).min()
    s= lh_upper(sig[peakind[0]], uncertainty)
    pmasku[0] = s-smin_right > athld
    # hypothesis that minimize peak height
    smin_right = lh_upper(sig[peakind[0]+1:peakind[1]], uncertainty).min()
    s = lh_lower(sig[peakind[0]], uncertainty);
    pmaskl[0] = s-smin_right > athld;
    # Last peak
    # hypothesis that maximize peak height
    smin_left = lh_lower(sig[peakind[np_-2]+1:peakind[np_-1]], uncertainty).min()
    s = lh_upper(sig[peakind[-1]], uncertainty)
    pmasku[-1] = s-smin_left > athld
    # hypothesis that minimize peak height
    smin_left = lh_lower(sig[peakind[-2]+1:peakind[-1]], uncertainty).min()
    s = lh_upper(sig[peakind[-1]], uncertainty);
    pmasku[-1] = s - smin_left > athld;
    # other points
    # hypothesis that maximize peak height
    for k in range(1, np_-1):
        left_points = np.s_[peakind[k-1]+1:peakind[k]]
        # hypothesis that maximize peak height
        smin_left = lh_lower(sig[left_points], uncertainty).min()
        right_points= np.s_[peakind[k]+1:peakind[k+1]]
        smin_right = lh_lower(sig[right_points], uncertainty).min()
        sk = lh_upper(sig[peakind[k]], uncertainty)
        if (sk-smin_left)*(sk-smin_right) < athld**2:
            pmasku[k] = False
    # hypothesis that minimize peak height
    for k in range(1, np_-1):
        left_points = np.s_[peakind[k-1]+1:peakind[k]]
        # hypothesis that maximize peak height
        smin_left = lh_upper(sig[left_points], uncertainty).min()
        right_points = np.s_[peakind[k]+1:peakind[k+1]]
        smin_right = lh_upper(sig[right_points], uncertainty).min()
        sk = lh_lower(sig[peakind[k]], uncertainty)
        if (sk-smin_left)*(sk-smin_right) < athld**2:
            pmaskl[k] = False
    q_[(~pmasku)|(~pmaskl)] = 0
    pmask = find_(pmasku|pmaskl)
    return peakind[pmask], q_[pmask]

def del_three_point_peaks(q_, sig, t, peakind, p3pt_threshold, uncertainty):
    """
    function [peakind,Q] = delThreePointPeaks(sig,t, peakind,p3ptThreshold,uncertainty)
    """
    q_ = np.array(q_) # copy ?
    npind = peakind.size
    pmasku = np.ones(npind, dtype=bool)
    pmaskl = np.ones(npind, dtype=bool)
    # hypothesis that maximize 3pt peak  detection
    for kk in range(npind):
        if peakind[kk]<3 or peakind[kk]>sig.size-3:
            # skip boundary peaks
            continue
        pk = peakind[kk]
        # hypothesis that maximize 3pt peak  detection
        s_max = np.zeros(5)
        s_max[[0, 2, 4]] = lh_upper(sig[pk+np.array([-2, 0, 2])], uncertainty);
        s_max[[1, 3]] = lh_lower(sig[pk+np.array([-1, 1])], uncertainty);
        if np.all(s_max[[0, 2]]>s_max[1]) and np.all(s_max[[2, 4]]>s_max[3]): # then // \/\/
            # pk is the index of a 3pt peak
            d = np.diff(s_max) / np.diff(t[pk-2:pk+3]);
            # sharpness coefficient
            ratio=np.abs(0.5*(d[0]-d[3])/np.sqrt(-d[1]*d[2]))
            pmasku[kk] = not ratio>=p3pt_threshold
        # hypothesis that minimize 3pt peak  detection
        s_min = np.zeros(5)
        s_min[[0, 2, 4]] = lh_lower(sig[pk+np.array([-2, 0, 2])], uncertainty);
        s_min[[1, 3]] = lh_upper(sig[pk+np.array([-1, 1])], uncertainty);
        if np.all(s_min[[0, 2]]>s_min[1]) and np.all(s_min[[2, 4]]>s_min[3]): # then // \/\/
            # pk is the index of a 3pt peak
            d=np.diff(s_min)/np.diff(t[pk-2:pk+3])
            # sharpness coefficient
            ratio = np.abs(0.5*(d[0]-d[3])/np.sqrt(-d[1]*d[2]))
            pmaskl[kk] = not ratio>=p3pt_threshold
    q_[(~pmasku)|(~pmaskl)] = 0
    pmask = pmasku|pmaskl
    return  peakind[pmask], q_[pmask]

        

def first_peaks(sig, t, period, uncertainty):
    """
    function peakind = firstPeaks(sig,t,period,uncertainty)
    //sig         : part of a lh signal starting at a peak location
    //t           : associated time instants in min
    //period      : time horizon used for next peak search
    //uncertainty : not yet used here  
    //Method

    //  The algorithm looks for the sequence of pulse which have a larger height
    //  within a given time range:

    //  Starting with indmax=1 (the first point of  sig  is assumed
    //  to be a peak) the algorithm iterates up to the end of sig the
    //  following sequence: 

    //  1 - The algorithm first looks for the mimimun signal point in the
    //      interval [t(indmax), t(indmax+period)],while the min point is found
    //      at the right border of the interval the interval is increased by one
    //      time step. The index of the minimum point is stored into
    //      indmin. If indmin is the last time index the algorithm terminates
    //  2 - If the maximum signal value in the interval ]t(indmax) t(indmin)[ is
    //      greater than sig(indmax) then indmax is updated to this new maximum
    //      index and the algorithm restart at step 1.

    //  3 - The algorithm looks for the max point in the [t(indmin),
    //      t(indmin+period)], while the max point is found at the right border
    //      of the interval the interval is increased by one time step. indmax
    //      is set to this max point index. If indmax is the last time index the
    //      algorithm terminates. in the other case a new peak is added and the
    //      algorithms restart a new sequence at step 1.
    """
    n_ = sig.size
    indmax= 0 # sig(indmax) is a max point
    peakind = [] # vector of detected  peaks locations
    while True: # peak detection loop
        # looking for the min point in the [t(indmax) t(indmax)+period] interval
        inde = find_(t>t[indmax]+period)
        if inde.size==0:
            inde = n_
        else:
            inde = inde[0] #- 1
        _, indmin = argmin_(sig[indmax+1:inde])
        if indmin is None:
            break
        #smin = sig[indmin]
        indmin += indmax+1
        # check if sig continue to decrease after t(indmax)+period (serge june 14 2012)
        while indmin < n_-1 and sig[indmin+1]<=sig[indmin]:
            indmin +=1 
        if indmin==n_-1:
            # min reached on the boundary no maximum can follow, job finished
            break
        # Check better peak candidate found in the interval ]t(indmax),t(indmin)]
        exmax, exind = argmax_(sig[indmax+1:indmin+1]);
        if exmax>sig[indmax]:
            # a better peak candidate found. Update previously detetected peak location
            indmax += exind+1
            if len(peakind):
                peakind[-1] = indmax # Update the last index
            continue
        # /\/ detected
        # looking for the max point in the [t(indmin) t(indmin)+period] interval
        # that can be the next peak candidate
        indmaxlast = indmax;
        inde=find_(t>t[indmin]+period)
        if inde.size==0:
            inde=n_
        else:
            inde = inde[0] #- 1
        _, indmax = argmax_(sig[indmin+1:inde]);
        #smax = sig[indmax]
        indmax += indmin+1
        # check if sig continue to increase after t(indmin)+period (serge june 14 2012)
        while indmax < n_-1 and sig[indmax+1]>=sig[indmax]:
            indmax += 1
        if indmax < n_-1: 
            # /\/\ detected, indmax is the location of a peak
            peakind.append(indmax)
        else:
            # Left border, it is impossible to know if the t(N) is a peak
            # look for a smaller peak in [t(indmaxlast) t(N-1)].
            # search for a local min on the left of t(N)
            indmin1 = n_-1
            while indmin1>indmin and sig[indmin1-1]<sig[indmin1]:
                indmin1 -= 1
            if indmin1<=indmin:
                # there is no possible peak ( /\ ) in [t(indmin) t(N)], job finished
                break
            # search for the max point in [t(indmin) t(indmin1)]
            _, indmax = argmax_(sig[indmin+1:indmin1])
            #smax = sig[indmax] # useless?
            indmax=indmax+indmin
            # /\/\ detected, indmax is the location of a peak
            peakind.append(indmax)
    return np.array(peakind)

def alternate_peak_positions(sig, peakind, uncertainty):
    """
    function [km,alt]=alternatePeakPositions(sig,peakind,uncertainty)
    """
    u_ = uncertainty
    cv = cv_lh(sig, u_)
    if peakind[0] == 0:
        kleft = find_(lh_lower(sig[peakind[1:]], u_) <
                          lh_upper(sig[peakind[1:]-1], u_))
        if kleft.size:
            kleft += 1
    else: 
        kleft = find_(lh_lower(sig[peakind],u_) <
                          lh_upper(sig[peakind-1], u_))
    # point following peak can be considered as an alternative peak
    # position (2 point flat peak)
    if peakind[-1] == sig.size-1:
        kright=find_(lh_lower(sig[peakind[:-1]], u_) <
                         lh_upper(sig[peakind[:-1]+1], u_))
    else:
        kright=find_(lh_lower(sig[peakind], u_) <
                         lh_upper(sig[peakind+1], u_))

    # km is the indices of "two points peaks" in sig
    km = np.union1d(kleft, kright)
    alt = []  
    #set possible peak displacements
    for kmj in km:
        v = [0]
        if np.any(kmj==kleft):
            v = [0, -1]
        if np.any(kmj==kright):
            v += [1]
        alt.append(v);
    return km, alt
