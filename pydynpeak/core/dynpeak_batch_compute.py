# This file is part of PyDynPeak software
# Copyright (C) 2021, Inria
# 
# PyDynpeak is a Python software derived from:
#          DynPeak (Scilab ATOMS Toolbox) version 2.1.0 (authors:
#	   	   	   	Claire Medigue, Serge Steer, Qinghua Zhang,
#              			Alexandre Vidal, Frédérique Clément
#                               cf. https://atoms.scilab.org/toolboxes/Dynpeak)
#
# PyDynPeak is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# PyDynPeak is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with PyDynPeak.  If not, see <https://www.gnu.org/licenses/>
# 
# Parts of this file are based on work covered by the following copyright
#       and permission notice:
#      """
#      // Copyright (C) 2012 - INRIA - Serge Steer
#      // This file must be used under the terms of the CeCILL.
#      // This source file is licensed as described in the file COPYING, which
#      // you should have received as part of this distribution.  The terms
#      // are also available at
#      // http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#      """
###############################################################################
#
# Authors : Frédérique Clément(+), Hande Gözükan(*), Christian Poli(*)
#
# +=scientific advisor, *=developer
#
###############################################################################

from .interactive_lhpeaks import interactive_lhpeaks

import numpy as np

def dynpeak_batch_compute(lh, params):
    '''original function signature:
    function dynpeak_batch_compute(k,win)

    Parameters
    ----------

    lh: 
        Pandas series that contains signal values and time values as
        index. (contained in win) 

    params: 
        detection parameters. (contained in win) 

    Returns
    -------
    - subseries for selected interval
    - detected peaks for the selected interval
    - alpha value used for peak detection
    Note
    ----
    Parts related to user interface are removed from the original function.

    '''
    interval = params['interval']
    lh1 = lh.iloc[ interval[0] : interval[1]+1 ]
    params["selection"] = [0, len(lh1)]

    # indpeaks gives detected peak locations
    indpeaks, alpha = interactive_lhpeaks(lh1, params)

    return lh1, indpeaks, alpha
