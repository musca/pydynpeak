# This file is part of PyDynPeak software
# Copyright (C) 2021, Inria
# 
# PyDynpeak is a Python software derived from:
#          DynPeak (Scilab ATOMS Toolbox) version 2.1.0 (authors:
#	   	   	   	Claire Medigue, Serge Steer, Qinghua Zhang,
#              			Alexandre Vidal, Frédérique Clément
#                               cf. https://atoms.scilab.org/toolboxes/Dynpeak)
#
# PyDynPeak is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# PyDynPeak is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with PyDynPeak.  If not, see <https://www.gnu.org/licenses/>
# 
###############################################################################
#
# Authors : Frédérique Clément(+), Hande Gözükan(*), Christian Poli(*)
#
# +=scientific advisor, *=developer
#
###############################################################################

import numpy as np
import os
import os.path
from collections import OrderedDict
import sys
_ddir = os.path.join(os.path.dirname(__file__), '..', 'data')
_gen = None
_data = None

def gen_rand_normal():
    """
    Mocks scilab rand() behaviour.
    Data were generated in scilab that way :
    rand("normal")
    rand("seed", 0)
    m = rand(100000,1);
    csvWrite(m, 'data/rand_normal_seed_0.csv')
    """
    for r in _data:
        yield r

def init_fake_randn():
    global _gen, _data
    if _data is None:
        _data = np.genfromtxt(os.path.join(_ddir, 'rand_normal_seed_0.csv'))
    _gen = gen_rand_normal()

init_fake_randn()
    
def fake_randn():
    return next(_gen)

def argmin_(arr):
    if arr.size == 0:
        return None, None
    ix = np.nanargmin(arr)
    return arr[ix], ix

def argmax_(arr):
    if arr.size == 0:
        return None, None
    ix = np.nanargmax(arr)
    return arr[ix], ix

def min_(arr):
    if arr.size == 0:
        return None
    return arr.min()

def max_(arr):
    if arr.size == 0:
        return None
    return arr.max()

def find_(x):
    return np.nonzero(x)[0]

def find_1st(x, default=None):
    res = find_(x)
    return res[0] if res.size else default

def find_1st_as_array(x, dt=np.int64):
    res = find_1st(x,np.array([], dtype=dt))
    if isinstance(res, np.ndarray):
        return res
    return np.array([res], dtype=dt)

def tee_(x):
    if x.ndim>1:
        return x.T
    return x.reshape((x.shape[0], 1))
    
def make_dataclass(class_name, attr_list):
    return FakeDataClass(class_name, attr_list)

def fieldnames(inst):
    return list(inst._attributes)

def make_struct(*args, **kwargs):
    #import pdb;pdb.set_trace()
    if (args and kwargs):
        ValueError("Keyword args XOR non-keyword args required")
    if len(args):
        keys = args[::2]
        values = args[1::2]
        kwargs = dict(zip(keys, values))
    #Struct = make_dataclass('Struct', keys)
    class _Struct(dict):
        def __init__(self, **kw):
            self._attributes = kw.keys()
            super().__init__(**kw)
        def __getattr__(self, attr):
            if  attr !="_attributes":
                return self[attr]
            return super().__getattr__(attr)
        def __setattr__(self, attr, val):
            if attr !="_attributes":# or attr in self._attributes:            
                self[attr] = val
                return
            return super().__setattr__(attr, val)

    return _Struct(**kwargs)

