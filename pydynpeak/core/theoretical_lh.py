# This file is part of PyDynPeak software
# Copyright (C) 2021, Inria
# 
# PyDynpeak is a Python software derived from:
#          DynPeak (Scilab ATOMS Toolbox) version 2.1.0 (authors:
#	   	   	   	Claire Medigue, Serge Steer, Qinghua Zhang,
#              			Alexandre Vidal, Frédérique Clément
#                               cf. https://atoms.scilab.org/toolboxes/Dynpeak)
#
# PyDynPeak is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# PyDynPeak is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with PyDynPeak.  If not, see <https://www.gnu.org/licenses/>
# 
# Parts of this file are based on work covered by the following copyright
#       and permission notice:
#      """
#      // Copyright (C) 2012 - INRIA - Qinghua ZHANG - Serge Steer
#      // This file must be used under the terms of the CeCILL.
#      // This source file is licensed as described in the file COPYING, which
#      // you should have received as part of this distribution.  The terms
#      // are also available at
#      // http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#      """
###############################################################################
#
# Authors : Frédérique Clément(+), Hande Gözükan(*), Christian Poli(*)
#
# +=scientific advisor, *=developer
#
###############################################################################

import numpy as np
from scipy.integrate import odeint

def _theoretical_lh_dynamics(lhp, t, amplitude, period, k_hl, alpha):
    """
    // t         : current time
    // LHp       : current measured LH level in the jugular blood.
    // Amplitude : pituitary gland LH spike amplitude 
    // Period    : pituitary gland LH spike period 
    // k_hl      : stiffness coefficient (log(2)/tau_hl, where tau_hl is the spike half-life)
    // Alpha     : represents the instantaneous LH clearance rate from the blood.
    """
    p = period(t) * 1.0
    a = amplitude(t) * 1.0
    return a*np.exp(-k_hl*(t - np.floor(t/p)*p)) - alpha*lhp

def theoretical_lh(time_s,amplitude, period, k_hl, alpha):
    """
    // T         : observation time instants
    // LHp       : computed  measured LH level at the requested time instants.
    // Amplitude : pituitary gland LH spike amplitude 
    // Period    : pituitary gland LH spike period 
    // k_hl      : stiffness coefficient (log(2)/tau_hl, where tau_hl is the spike half-life)
    // Alpha     : represents the instantaneous LH clearance rate from the blood.
    """
    if not (isinstance(time_s, np.ndarray) and
                np.issubdtype(time_s.dtype, np.floating) and
                np.all(time_s>=0)):
        raise ValueError("Real positive vector expected for 'time_s'")
    if not np.all(np.diff(time_s)>0):
        raise ValueError("increasing order expected for 'time_s'")
    if not callable(amplitude):
        raise ValueError("function expected for 'amplitude'")
    if not (isinstance(k_hl, float) and k_hl >=0):
        raise ValueError("real non negative value expected for 'k_hl'")
    if not (isinstance(alpha, float) and alpha >=0):
        raise ValueError("real non negative value expected for 'alpha'")
    lh0  = 0
    return odeint(_theoretical_lh_dynamics, lh0, time_s, (amplitude, period, k_hl, alpha, ))

