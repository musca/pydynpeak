# This file is part of PyDynPeak software
# Copyright (C) 2021, Inria
# 
# PyDynpeak is a Python software derived from:
#          DynPeak (Scilab ATOMS Toolbox) version 2.1.0 (authors:
#	   	   	   	Claire Medigue, Serge Steer, Qinghua Zhang,
#              			Alexandre Vidal, Frédérique Clément
#                               cf. https://atoms.scilab.org/toolboxes/Dynpeak)
#
# PyDynPeak is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# PyDynPeak is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with PyDynPeak.  If not, see <https://www.gnu.org/licenses/>
# 
# Parts of this file are based on work covered by the following copyright
#       and permission notice:
#      """
#      // Copyright (C) 2012 - INRIA - Serge Steer
#      // This file must be used under the terms of the CeCILL.
#      // This source file is licensed as described in the file COPYING, which
#      // you should have received as part of this distribution.  The terms
#      // are also available at
#      // http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#      """
###############################################################################
#
# Authors : Frédérique Clément(+), Hande Gözükan(*), Christian Poli(*)
#
# +=scientific advisor, *=developer
#
###############################################################################

from .lhpeaks_default_params import lhpeaks_default_params
from .interactive_lhpeaks import interactive_lhpeaks
from .dynpeak_smooth_ipi import dynpeak_smooth_ipi
from .dynpeak_batch_compute import dynpeak_batch_compute

import numpy as np
import pandas as pd

def detect_peaks(series, params):
    '''
    Returns
    -------
    - subseries for selected interval
    - ipi, t-high, t-low, median values
    - peak detection parameters used
    - detected peaks for the selected interval

    '''
    # drop NaN values
    series = series.dropna()

    if params is None:
        params = lhpeaks_default_params()

    interval = params.get('interval', [])

    if interval == []:
        params['interval'] = [0, len(series) - 1]

    lh_serie, indpeaks, alpha = dynpeak_batch_compute(series, params)

    beta = alpha

    t = lh_serie.index

    # plot lh with these values
    loc = t[indpeaks]

    # TODO ask if this is true, I added this
    detected_peaks = lh_serie[loc]

    # lower
    y = np.diff(loc)

    f = dynpeak_smooth_ipi(y, nord = None)

    ipi_serie = pd.Series(y, loc[1:], name = "ipi")

    tmed_serie = pd.Series(f, loc[1:], name = "median")
    thigh_serie = pd.Series(f * (1 + beta), loc[1:], name = "t-high")
    tlow_serie = pd.Series(f * (1 - alpha), loc[1:], name = "t-low")

    lh_frame = pd.DataFrame(lh_serie, lh_serie.index)

    ipi_frame = pd.concat([ipi_serie, tmed_serie, thigh_serie, tlow_serie], axis=1)

    return lh_frame, ipi_frame, params, detected_peaks
