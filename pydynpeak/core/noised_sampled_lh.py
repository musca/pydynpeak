# This file is part of PyDynPeak software
# Copyright (C) 2021, Inria
# 
# PyDynpeak is a Python software derived from:
#          DynPeak (Scilab ATOMS Toolbox) version 2.1.0 (authors:
#	   	   	   	Claire Medigue, Serge Steer, Qinghua Zhang,
#              			Alexandre Vidal, Frédérique Clément
#                               cf. https://atoms.scilab.org/toolboxes/Dynpeak)
#
# PyDynPeak is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# PyDynPeak is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with PyDynPeak.  If not, see <https://www.gnu.org/licenses/>
# 
# Parts of this file are based on work covered by the following copyright
#       and permission notice:
#      """
#      // Copyright (C) 2012 - INRIA - Serge Steer
#      // This file must be used under the terms of the CeCILL.
#      // This source file is licensed as described in the file COPYING, which
#      // you should have received as part of this distribution.  The terms
#      // are also available at
#      // http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#      """
###############################################################################
#
# Authors : Frédérique Clément(+), Hande Gözükan(*), Christian Poli(*)
#
# +=scientific advisor, *=developer
#
###############################################################################

import numpy as np

from numpy.random import normal as randn

def noised_sampled_lh(t, lh, tstep, r, f, cv, fake_randn=None):
    """
    //T           : theoritical LHp signal observation dates (sampled with high frequency)
    //LH          : theoritical LHp signal
    //tstep       : sampling time in number of minutes
    //r           : date of the first sample in min
    //f           : standard deviation of the error on the measure date
    //CV          : function which computes the measurement uncertainty for a given
    //              LH level
    """
    global randn
    if fake_randn is not None: # TODO: remove this hack when scilab reference become useless ...
        randn = fake_randn
    if not (isinstance(t, np.ndarray) and
                np.issubdtype(t.dtype, np.floating) and
                np.all(t>=0)):
        raise ValueError("Real positive vector expected for 't'")
    if not np.all(np.diff(t)>0):
        raise ValueError("increasing order expected for 't'")
    if not (isinstance(lh, np.ndarray) and
                np.issubdtype(lh.dtype, np.floating)):
        raise ValueError("Real vector expected for 'lh'")
    if t.shape[0] != lh.shape[0]:
        raise ValueError("Incompatible input args: same sizes expected for 't' and 'lh'")
    dt = t[1] - t[0]
    if not isinstance(tstep, float):
        raise ValueError("Real expected for 'tstep'")
    if tstep < dt:
        raise ValueError("'tstep' too small")
    if not (isinstance(r, float) and r >=0):
        raise ValueError("real non negative value expected for 'r'")    
    if not (isinstance(f, float) and f >=0):
        raise ValueError("real non negative value expected for 'f'")    
    if not ((isinstance(cv, float) and cv >=0) or callable(cv)):
        raise ValueError("real non negative value or function expected for 'cv'")
    tr = 2.2 # Distribution truncating parameter, avoids large deviation effect
    n = lh.shape[0]
    c = tstep/dt
    rp = r/dt
    if isinstance(cv, float):
        cv = cv*np.ones(lh.shape)
    else:
        cv = cv(lh)
    # number of samples
    ns = int((n-c-rp-1)/c) + 1
    lhs = np.zeros(ns)
    ts = np.zeros(ns)
    for j, i in enumerate(np.arange(0, n-c-rp-1, c)):
        u = randn()
        while abs(u)>tr:
            u = randn()
        e = np.floor(f/dt*randn())
        ii = i-e+rp if i+e<0 else i+e+rp
        ii = int(min(ii,n))
        lhs[j] = lh[ii]*(1+cv[ii]*u)
        ts[j] = t[int(i+rp)]
    return ts, lhs
