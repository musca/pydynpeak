# This file is part of PyDynPeak software
# Copyright (C) 2021, Inria
# 
# PyDynpeak is a Python software derived from:
#          DynPeak (Scilab ATOMS Toolbox) version 2.1.0 (authors:
#	   	   	   	Claire Medigue, Serge Steer, Qinghua Zhang,
#              			Alexandre Vidal, Frédérique Clément
#                               cf. https://atoms.scilab.org/toolboxes/Dynpeak)
#
# PyDynPeak is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# PyDynPeak is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with PyDynPeak.  If not, see <https://www.gnu.org/licenses/>
# 
# Parts of this file are based on work covered by the following copyright
#       and permission notice:
#      """
#      // Copyright (C) 2012 - INRIA - Serge Steer
#      // This file must be used under the terms of the CeCILL.
#      // This source file is licensed as described in the file COPYING, which
#      // you should have received as part of this distribution.  The terms
#      // are also available at
#      // http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#      """
###############################################################################
#
# Authors : Frédérique Clément(+), Hande Gözükan(*), Christian Poli(*)
#
# +=scientific advisor, *=developer
#
###############################################################################

from .lhpeaks import lhpeaks
from .lhpeaks_check_options import lhpeaks_check_options
from .lhpeaks_default_params import lhpeaks_default_params

ALPHA = 0.6

def interactive_lhpeaks(lh, params):
    '''
    Parameters
    ----------
    lh: pandas.Series
        series that contains signal values and time values as index. (sig and Ts)
    params: 
        detection parameters. (peakpar)
    
    Returns
    -------
    
    
    Note
    ----
    original function signature:
    function interactive_lhpeaks(sig,t,p)
    Ts: signal sampling time (in minutes),or vector of sample dates
    sig:  signal
    peakind: vector of peaks indices.


    Note
    ----
    Parts related to user interface and interaction are removed from the original function.
    - In scilab code, after call to lhpeaks, dynpeak_init_graph is called.
    - figures dynpeak object is updated with parameters necessry for interaction
    - event handler is set
    - dynpeak_update_graph is called
    
    '''
    fname = "interactive_lhpeaks"
    alpha=ALPHA
    
    if params is None:
        params = lhpeaks_default_params()
    else:
        params = lhpeaks_check_options(params, len(lh), fname)

    # indpeaks refers to detected peaks
    indpeaks = lhpeaks(lh, lh.index, params)
       
    return indpeaks, alpha
