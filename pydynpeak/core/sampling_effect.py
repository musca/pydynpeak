# This file is part of PyDynPeak software
# Copyright (C) 2021, Inria
# 
# PyDynpeak is a Python software derived from:
#          DynPeak (Scilab ATOMS Toolbox) version 2.1.0 (authors:
#	   	   	   	Claire Medigue, Serge Steer, Qinghua Zhang,
#              			Alexandre Vidal, Frédérique Clément
#                               cf. https://atoms.scilab.org/toolboxes/Dynpeak)
#
# PyDynPeak is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# PyDynPeak is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with PyDynPeak.  If not, see <https://www.gnu.org/licenses/>
# 
# Parts of this file are based on work covered by the following copyright
#       and permission notice:
#      """
#      // Copyright (C) 2012 - INRIA - Serge Steer
#      // This file must be used under the terms of the CeCILL.
#      // This source file is licensed as described in the file COPYING, which
#      // you should have received as part of this distribution.  The terms
#      // are also available at
#      // http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#      """
###############################################################################
#
# Authors : Frédérique Clément(+), Hande Gözükan(*), Christian Poli(*)
#
# +=scientific advisor, *=developer
#
###############################################################################

from pydynpeak.core import theoretical_lh, noised_sampled_lh
from pydynpeak.core.utils import find_

import numpy as np
import pandas as pd

def show_sampling_effect(mspike, pspike, r, f, b):
    '''Original function signature:
    function ShowSamplingEffect(Mspike,Pspike,r,f,b)
    //Mspike : a character string that contains an expression which computes the
    //         spike amplitude as a function of time given in minutes
    //         (example: "15-t*8.7e-3")
    
    //Pspike : a character string that contains an expression which computes the
    //         spike periode as a function of time given in minutes
    //         (example: "100-t/30")
    //r      : initial time (in minute of the first sample)
    //f      : sampling time variability
    //b      : uncertainty on the measured sampled signa values.

    Parameters
    ----------
    mspike : 
        a character string that contains an expression which computes
        the spike amplitude as a function of time given in minutes
        (example: "15-t*8.7e-3")
  
    pspike : 
        a character string that contains an expression which computes
        the spike periode as a function of time given in minutes
        (example: "100-t/30")

    r : 
        initial time (in minute of the first sample) (first sample date)

    f : 
        sampling time variability   (uncertainty on sample data)

    b : 
        uncertainty on the measured sampled signal values. (uncertainty on LH measure)


    Note
    ----
    Parts related to user interface and interaction are removed from
    the original function.
    '''

    # synthetic ----------------------------------------
    k_lh = 0.03465735903 # stiffness coefficient 
    alpha = 6.0  # instantaneous LH clearance rate (1/min)

    # Integrate theoritical (continuous time) model
    t_max = 1500
    dt = 0.01 # observation step for theoritical model
    t = np.arange(0, t_max + dt, dt)
    lh = theoretical_lh(t, mspike, pspike, k_lh, alpha)
    lh_max = lh.max()
    lh_serie = pd.Series(data = lh.reshape(len(lh)), index = t, name = 'Synthetic signal')

    
    # sampled -------------------------------------------
    tsampling = 10.0
    ts, lhs = noised_sampled_lh(t, lh, tsampling, r, f, b)
    lhs_serie = pd.Series(data = lhs, index = ts, name = 'Sampled signal')

    len_lh = len(lh)
    peakind = find_( np.logical_and( lh[:len_lh - 2 ] < lh[ 1 : len_lh - 1 ], lh[ 2 : ] < lh[ 1 : len_lh - 1 ])) + 1
    lh_peakind = lh_serie.iloc[peakind]
    lh_peakind.name = 'Synthetic signal peaks'

    len_lhs = len(lhs)
    peakinds = find_( np.logical_and( lhs [: len_lhs - 2 ] < lhs[ 1 : len_lhs - 1 ], lhs[ 2 : ] < lhs[ 1 : len_lhs - 1 ])) + 1

    lhs_peakinds = lhs_serie.iloc[peakinds]
    lhs_peakinds.name = 'Sample signal peaks'

# IPI -------------------------------------------
    len_pk = len(peakind)
    len_pks = len(peakinds)
  
    ipi = t[peakind[1:]] - t[peakind[: len_pk - 1]]
    ipis = ts[peakinds[1:]] - ts[peakinds[: len_pks - 1]]
    
    ipi_max = np.max([np.ndarray.max(ipi), np.ndarray.max(ipis)])
    ipi_min = np.min([np.min(ipi), np.min(ipis)])
    
    ipi_serie = pd.Series(data = ipi, index = t[peakind[: len_pk - 1]], name = "Synthetic signal IPI")
    ipis_series = pd.Series(data = ipis, index = ts[peakinds[: len_pks - 1]], name = "Sample signal IPI")

    # props_int.x_range = [0, t_max]
    # props_int.y_range = [ipi_min, ipi_max]

    synthetic_frame = lh_serie.to_frame()
    sampled_frame = pd.concat([lhs_serie, lh_peakind, lhs_peakinds], axis=1)
    ipi_frame = pd.concat([ipi_serie, ipis_series], axis=1)
    
    return synthetic_frame, sampled_frame, ipi_frame
