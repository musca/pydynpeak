# This file is part of PyDynPeak software
# Copyright (C) 2021, Inria
# 
# PyDynpeak is a Python software derived from:
#          DynPeak (Scilab ATOMS Toolbox) version 2.1.0 (authors:
#	   	   	   	Claire Medigue, Serge Steer, Qinghua Zhang,
#              			Alexandre Vidal, Frédérique Clément
#                               cf. https://atoms.scilab.org/toolboxes/Dynpeak)
#
# PyDynPeak is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# PyDynPeak is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with PyDynPeak.  If not, see <https://www.gnu.org/licenses/>
# 
# Parts of this file are based on work covered by the following copyright
#       and permission notice:
#      """
#      // Copyright (C) 2012 - INRIA - Serge Steer
#      // This file must be used under the terms of the CeCILL.
#      // This source file is licensed as described in the file COPYING, which
#      // you should have received as part of this distribution.  The terms
#      // are also available at
#      // http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#      """
###############################################################################
#
# Authors : Frédérique Clément(+), Hande Gözükan(*), Christian Poli(*)
#
# +=scientific advisor, *=developer
#
###############################################################################

import numpy as np
from .dynpeak_smooth_ipi import dynpeak_smooth_ipi
#from .utils import argmin_, argmax_, find_, find_1st, min_, max_
from .utils import find_, find_1st, tee_

def lhpeaks_refine(peakind, t_, km, vars_, q_):
    """
    function [peakind,Q]=lhpeaks_refine(peakind,T,km,vars_,Q)
    //peakind     : vector of index of detected peaks in the signal
    //km          : vector of index (relative to peakind) of peaks that can be moved
    //vars_        : list. vars_(i) gives the admissible move for km(i)
    //Q           : vector. If Q(j)==0 then peakind(j) can be removed 
    //                                 else peak of index peakind(j) can only be moved
    """
    vars_ = vars_[:] #np.array(vars_)
    max_nopt = 20000 # max number of local combinaison allowed
    modifiable = np.union1d(km,find_(q_!=1)) #index of peaks that can be moved or removed
    if modifiable.size==0:
      return peakind, q_
    # find consecutive sequence of "modifiable peaks"
    dm = np.diff(modifiable)
    im = 0
    # seq_start and seq_end will store index relative to the begining and
    # end of the consecutive sequences of "modifiable peaks"
    seq_start, seq_end = [], []
    while im<modifiable.size:
        seq_start.append(im)
        # count how many consecutive modifiable peaks following this one
        k = find_1st(dm[im:]>1)
        if k is None:
            seq_end.append(modifiable.size-1)
            break
        else:
            seq_end.append(im+k)
        im += k+1
    # optimize each sequence of modifiable peaks individually to avoid
    # combinatorial explosion as well as excessive smoothing.
    seq_start = np.array(seq_start)
    seq_end = np.array(seq_end)
    for i in range(seq_start.size-1, -1, -1):
        # option applicable to the peaks in this sequence
        peakoptions = []
        nopt = 1
        for k in range(seq_start[i], seq_end[i]+1):
            po = [0]
            if np.any(modifiable[k]==km):
                po=vars_[find_1st(km==modifiable[k])] #.tolist()
            if np.all(q_[modifiable[k]]==0):
              po.append(np.nan)
            peakoptions.append(po)
            nopt=nopt*len(po)
        # determine optimal position of uncertain peaks in the ith sequence
        indi = modifiable[seq_start[i]:seq_end[i]+1] #transp?
        opt_peak_index, keep = optloc(peakind, t_, modifiable,
                                     [seq_start[i], seq_end[i]], peakoptions)
        if opt_peak_index is not None:
            peakind[indi[keep]] = opt_peak_index
        if find_(keep).size!=indi.size:
            removed=np.arange(indi.size)
            removed = np.delete(removed, np.nonzero(keep))
            q_ = np.delete(q_, indi[removed])
            peakind = np.delete(peakind, indi[removed])
            nr = removed.size
            v, kr, _ = np.intersect1d(modifiable,indi[removed], return_indices=True)
            for krir in kr:
                modifiable[krir:] -= 1
            modifiable = np.delete(modifiable, kr)
    return peakind, q_

def optloc(peakind, t_, modifiable, seq, peakoptions):
    """
    function [opt_peak_index,opt_keep]=optloc(peakind,T,modifiable,seq,peakoptions)
    //peakind     : full set of peaks index within signal
    //T           : the vector of the signal observation dates
    //modifiable  : index of uncertain peaks (peaks that can be moved or
    //              removed) relative to peakind
    //seq         : [seq_start seq_end]: index relative to modifiable of 
    //               the sequence of consecutive uncertain peaks to be optimized
    //peakoptions : peakoptions(i) if the vector of admissible modifications 
    //              that can be applied on the indi(i) peak
    //The algorithm finds the optimal peaks position in the given sequence of consecutive
    //               uncertain peaks checking all possible combinations. The
    //               criterion is the norm of the whole set of IPI's to the a
    //               least square cubic interpolation of this set. The weight of
    //               the IPI's associated to uncertain peaks that follow the
    //               current sequence is reduced.
    """
    indi = modifiable[seq[0]:seq[1]+1] # transp?
    if indi.size==0:
        return None, []
    d = gencomb(peakoptions)
    np_ = peakind.size
    # search the hypothesis that keeps less peaks
    ndel=0;

    for j in range(d.shape[0]):
        ndel= max(ndel, find_(np.isnan(d[j,:])).size)
    # Common  interpolation order for all hypothesis
    nord = min(3, max(np_-ndel -3, 1)) # max interpolation order
    # loop on hypothesis
    opt_peak_index = peakind[indi]
    opt_keep=[];
    m = np.inf
    for j in range(d.shape[0]):
        hypo = d[j, :]
        keep = np.logical_not(np.isnan(hypo))
        kept_peak_index = indi[keep]
        kept_peak_displacement = np.array(hypo[keep],dtype=np.int64)  # transp?
        new_peak_index = peakind[kept_peak_index] + kept_peak_displacement
        sel = np.concatenate([peakind[:indi[0]], new_peak_index, peakind[indi[-1]+1:]]) # col
        n1 = indi[0] + new_peak_index.size
        ns = sel.size
        loc = t_[sel]
        # compute least square weight of IPI's interpolation
        # set a weight array decreasing on the two sides of the sequence no
        # to regularize too much
        # and reduced  for uncertain IPI's on the left (not yet optimized)
        w = np.ones(loc.size)
        w[:indi[0]] = np.linspace(0.3, 1, num=indi[0]) # transp
        w[n1:] = np.linspace(1, 0.3, num=ns-n1) # transp
        # reduce weight for uncertain IPI's in the sequence and on the left (not yet optimized)
        for k in range(indi[0], n1):
            w[k] *= 0.2
        for k in range(indi[0]-1):
            if np.any(sel[k]==peakind[modifiable]):
                w[k] *= 0.2
        if loc.size < 4:
            # add fake peaks on the left and on the right
            loc = np.concatenate([[loc[0]]-(t_[peakind[1]]-t_[peakind[0]]), loc, [loc[-1]+t_[peakind[-1]]-t_[peakind[-2]]]]) # col
            w= np.concatenate([[0.2], w, [0.2]]) # col
            if loc.size < 4:
                # too few peaks with such an hypothesis the approximation curve
                # mathes exactly the IPI's. The criterion is 0. Exclude this
                # hypothesis of the optimization set.
                continue
        v = objective(loc, nord, w)
        if v<m:
            m = v
            opt_keep = keep
            opt_peak_index = new_peak_index
    if opt_keep.size==0:
        # all sequences have been excluded retain all peaks
        opt_keep = np.arange(indi.size)
        opt_peak_index=peakind[indi]
    return opt_peak_index, opt_keep

def gencomb(vars_):
    """
    function r=gencomb(vars_)
    """
    v = vars_[0]
    r = tee_(np.array(v))
    for k in range(1, len(vars_)): #_.shape[0]):
        v = tee_(np.array(vars_[k]))
        n = v.shape[0]
        r = np.concatenate([np.kron(np.ones((n,1)), r), np.kron(v, np.ones((r.shape[0],1)))], axis=1)
    return r

def objective(loc, nord, w):
    """
    function c=objective(loc,nord,w)
    """
    y = np.diff(loc)
    c = np.linalg.norm(y-dynpeak_smooth_ipi(y, nord, w[:y.size]))/y.size
    return c
