# This file is part of PyDynPeak software
# Copyright (C) 2021, Inria
# 
# PyDynpeak is a Python software derived from:
#          DynPeak (Scilab ATOMS Toolbox) version 2.1.0 (authors:
#	   	   	   	Claire Medigue, Serge Steer, Qinghua Zhang,
#              			Alexandre Vidal, Frédérique Clément
#                               cf. https://atoms.scilab.org/toolboxes/Dynpeak)
#
# PyDynPeak is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# PyDynPeak is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with PyDynPeak.  If not, see <https://www.gnu.org/licenses/>
# 
###############################################################################
#
# Authors : Frédérique Clément(+), Hande Gözükan(*), Christian Poli(*)
#
# +=scientific advisor, *=developer
#
###############################################################################

import json

class DynEnv:
    """
    env = DynEnv()
    env.set("foo.bar.xy", 3.14)
    env.set(["foo","bar","xy"], 3.14)
    x = env.get("foo.bar.xy", 3.14)
    x = env.get(["foo","bar","xy"])
    """
    def __init__(self, v=None):
        self.root_ = {} if v is None else v

    def exists(self, keys):
        if isinstance(keys, str):
            keys = keys.split(".")

        node = self.root_
        for k in keys:
            if k not in node:
                return False
        return True

    def get(self, keys, df=None):
        if isinstance(keys, str):
            keys = keys.split(".")
        node = self.root_
        for k in keys:
            if k not in node:
                if df is None:
                    raise KeyError(k+" not found")
                    
                return df
            node = node[k]
        return node

    def set(self, keys, val):
        if isinstance(keys, str):
            keys = keys.split(".")
        node = self.root_
        prev_node = node
        for k in keys:
            if k not in node:
                node[k] = {}
            prev_node = node
            node = node[k] 
        prev_node[k] = val

    def get_node(self, keys):
        if isinstance(keys, str):
            keys = keys.split(".")
        node = self.root_
        prev_node = node
        for k in keys:
            if k not in node:
                node[k] = {}
            prev_node = node
            node = node[k] 
        return node

    def load(self, fname):
        pass

    def save(self, fname):
        pass

    def to_json(self):
        return json.dumps(self.root_)
