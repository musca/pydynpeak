# This file is part of PyDynPeak software
# Copyright (C) 2021, Inria
# 
# PyDynpeak is a Python software derived from:
#          DynPeak (Scilab ATOMS Toolbox) version 2.1.0 (authors:
#	   	   	   	Claire Medigue, Serge Steer, Qinghua Zhang,
#              			Alexandre Vidal, Frédérique Clément
#                               cf. https://atoms.scilab.org/toolboxes/Dynpeak)
#
# PyDynPeak is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# PyDynPeak is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with PyDynPeak.  If not, see <https://www.gnu.org/licenses/>
# 
###############################################################################
#
# Authors : Frédérique Clément(+), Hande Gözükan(*), Christian Poli(*)
#
# +=scientific advisor, *=developer
#
###############################################################################

from pydynpeak.core import lhpeaks_default_params, lhpeaks_check_options, detect_peaks, make_struct
from pydynpeak.dataset import load_csv, load_excel
from pydynpeak.vis import view_head, view_tail, view_all, view_sampling_times, plot_lh, plot_detection_graph

from pandas.errors import EmptyDataError
from pandas import DataFrame
from xlrd.biffh import XLRDError
import logging

logger = logging.getLogger(__name__)


class DynpeakExecuter():

    '''Provides methods necessary to load hormonal signals, adjust
    detection parameters, detect pulses in signals and visualize
    graphs for data and detection.




    Note
    ----
    This class uses `FigureWidget` to visualize graphs. `FigureWidget`
    enables callback interactions (on_click event, etc.) on the
    graphs. However, renderer for `FigureWidget` is not able to export
    `FigureWidget`s to other formats like "html" or "pdf" using
    notebook's `File -> Download as` menu.

    To export the notebook to "pdf" or "html" using `File->Download
    as` menu, use `DynpeakExecuterWrapper`. This will only disable
    interaction on detection graph to view peak interval for a
    selected point.

    '''

    def __init__(self):
        '''Initializes a DynpeakExecuter instance setting all attributes to
        `None`.
        '''

        self._dataframe = None
        self._current_series = None
        self._lh_params = None
        self._detection = None

    def _set_data(self, dataframe, seriesname):
        '''Initializes the attributes of `DynpeakExecuter` instance with
        `dataframe` and `seriesname`.

        Parameters
        ----------
        dataframe : pandas.DataFrame
            dataframe that contains hormonal signals as its series.

        seriesname : str
            a string that can be used as name for each series in
            `dataframe` in combination with one-based index of
            series. Name of the series is used as title in the plots
            if no header is specified.

            Example
            -------
            Caraty ewes1, Caraty ewes2,... when seriesname is
            specified as `Caraty ewes`.

        Raises
        ------
        ValueError
            If `dataframe` is `None` or empty.
        '''

        if dataframe is not None and not dataframe.empty:
            self._dataframe = dataframe
            self._seriesname = seriesname
            self._current_series = 1

            nb_series = dataframe.shape[1]
            self._lh_params = [lhpeaks_default_params()
                               for i in range(nb_series)]
            self._detection = [None for i in range(nb_series)]
        else:
            raise ValueError('Please specify a valid and non empty dataframe!')

    def _check_dataframe(self):
        '''Checks if data is loaded.

        Returns
        -------
        bool
            `True` if data is loaded; `False` otherwise.
        '''

        if self._dataframe is None:
            logger.error("No data available! Please first load data!")
            return False
        else:
            return True

    def _is_valid_index(self, index):
        '''Checks if `index` is valid with respect to the number of series in
        data.

        An index is valid if it is between 1 and number of series in
        data inclusive, or `None`.

        Note
        ----
        `None` value is considered valid, as for any operation, if no
        index is specified, index is set to `_current_series` value.

        Parameters
        ----------
        index: int
            one-based index of series in dataframe, or `None`.

        Returns
        -------
        bool
            `True` if `index` is valid; `False` otherwise.
        '''

        if index is not None:
            if index < 1 or index > self._dataframe.shape[1]:
                logger.error(
                    f"Please specify an index value between 1 and {self._dataframe.shape[1]} inclusive.")
                return False
        return True

    def _get_valid_index(self, index):
        '''Returns 0-based series index.

        Valid index is 0-based index to be used on dataframe
        operations.

        Parameters
        ----------
        index : int
           one-based index of series in dataframe, or `None`.

        Returns
        -------
        int
            0-based index to be used on operations on dataframe
            equivalent to `index`:
            - if `index` is None, returns `_current_series` - 1
            - if `index` is valid, returns index - 1
            - if `index` is not valid, or no data is loaded, returns -1.
        '''

        if self._check_dataframe() and self._is_valid_index(index):
            return index-1 if index is not None else (self._current_series-1 if self._current_series is not None else 0)
        else:
            return -1

    @property
    def currentseries(self):
        '''Returns index of the current series.

        Current series is the selected series in the dataframe. When
        no index is specified, all operations are executed on the
        current series.

        Returns
        -------
        int
            one-based index of the series in data set as current
            series.
        '''

        return self._current_series

    @currentseries.setter
    def currentseries(self, index):
        '''Sets the current series.

        Current series is the selected series in the dataframe. When
        no index is specified, all operations are executed on the
        current series.

        Parameters
        ----------
        index : int
            one-based index of the series in data to set as current
            series.

            If `index` is not valid, the current series is not
            changed, an error log is displayed.
        '''

        if self._check_dataframe():
            if self._is_valid_index(index):
                self._current_series = index
            else:
                logger.warn(
                    f"Invalid index {index}. Current series index has not changed.")

    def load_csv(self, filepath, sep=',', header=None, names=None, seriesname='', timeinterval=10, timecolumn=None):
        '''Loads the contents of the CSV file at `filepath` as dataframe.

        Parameters
        ----------
        filepath : str
            absolute/relative path of the CSV file.

        sep : str, default ','
            separator of the CSV file.

        header : int, default 'None'
            None means no header, a positive integer value indicates
            the row number in CSV file to be used as column headers.

        names : array-like, default None
            List of column names to use. If file contains no header
            row, then you should explicitly pass header=None.

        seriesname : str, default empty str
            column name to be used in combination with one-based
            column index, in the case headers do not exist in CSV
            file.

            Example
            -------
            Caraty ewes1, Caraty ewes2,... when seriesname is
            specified as `Caraty ewes`.

        timeinterval : int, default 10
            time interval to generate the sampling time values.

        timecolumn : int, default None
            zero-based column index that specifies the index of the
            column that contains sampling time values. If `None`,
            sampling time values are generated using `timeinterval`.
        '''

        try:
            dataframe = load_csv(filepath, sep, header,
                                 names, seriesname, timeinterval, timecolumn)
            self._set_data(dataframe, seriesname)
        except EmptyDataError:
            logger.error(
                f"File '{filepath}' is empty, please specify a non-empty CSV file.")
        except FileNotFoundError:
            logger.error(
                f"File '{filepath}' does not exist. Please specify a valid CSV file.")
        except ValueError as e:
            logger.error(e)

    def load_excel(self, filepath, sheet_name=0, header=None, names=None, seriesname='', timeinterval=10, timecolumn=None):
        '''Loads the contents of the Excel file at `filepath` as dataframe.

        Parameters
        ----------
        filepath : str
            absolute/relative path of the Excel file.

        sheet_name : str or int, default 0
            Strings are used for sheet names. Integers are used in
            zero-indexed sheet positions.

        header : str, default 'None'
            None means no header, a positive integer value indicates
            the row number in Excel file to be used as column headers.

        names : array-like, default None
            List of column names to use. If file contains no header
            row, then you should explicitly pass header=None.

        seriesname : str, default empty str
            column name to be used in combination with one-based
            column index, in the case headers do not exist in CSV
            file.

            Example
            -------
            Caraty ewes1, Caraty ewes2,... when seriesname is
            specified as `Caraty ewes`.

        timeinterval : int, default 10
            time interval to generate the sampling time values.

        timecolumn : int, default None
            zero-based column index that specifies the index of the
            column that contains sampling time values. If `None`,
            sampling time values are generated using `timeinterval`.
        '''

        try:
            dataframe = load_excel(
                filepath, sheet_name, header, names, seriesname, timeinterval, timecolumn)
            self._set_data(dataframe, seriesname)
        except EmptyDataError:
            logger.error(
                f"File '{filepath}' is empty, please specify a non-empty CSV file.")
        except FileNotFoundError:
            logger.error(
                f"File '{filepath}' does not exist. Please specify a valid CSV file.")
        except IndexError:
            logger.error(
                f"File {filepath} does not contain sheet {sheet_name}. Please specify an existing sheet name.")
        except XLRDError:
            logger.error(
                f"File {filepath} does not contain sheet {sheet_name}. Please specify an existing sheet name.")
        except (AssertionError, ValueError) as e:
            logger.error(e)

    def view_head(self, n=10):
        '''Displays first `n` rows of data. 

        Parameters
        ----------
        n : int, default 10
            number of rows to be shown.

        Returns
        -------
        FigureWidget
            table that displays first `n` rows of data.
        '''

        if self._check_dataframe():
            return view_head(self._dataframe, n)

    def view_tail(self, n=10):
        '''Displays last `n` rows of data.

        Parameters
        ----------
        n : int, default 10
            number of rows to be shown.

        Returns
        -------
        FigureWidget
            table that displays last `n` rows of data.
        '''

        if self._check_dataframe():
            return view_tail(self._dataframe, n)

    def view_all(self):
        '''Displays all rows in data.

        Returns
        -------
        FigureWidget
            table that displays all rows in data.
        '''

        if self._check_dataframe():
            return view_all(self._dataframe)

    def view_sampling_times(self):
        '''Displays sampling time values.

        Returns
        -------
        FigureWidget
            table that displays sampling time values.
        '''

        if self._check_dataframe():
            return view_sampling_times(self._dataframe)

    def plot_lh_all(self, header=None):
        '''Displays all the series in data in a single graph.

        Parameters
        ----------

        header : str, default None
             text to display as header for the graph. If not set,
             defaults to `seriesname` specified while loading data. If
             no `seriesname` is specified, it is empty string.

        Returns
        -------
        FigureWidget
            graph that displays all the series in data.
        '''

        if self._check_dataframe():
            return plot_lh(self._dataframe, index=None, header=header if header is not None else self._seriesname)

    def plot_lh(self, index=None, header=None):
        '''Displays the series at `index`.

        Parameters
        ----------
        index : int, default None
            one-based index of the series in data. If index is not
            specified, displays the current series.

            If `index` is negative or greater than the number of
            series in the dataframe, prints an error message and
            returns.

        header : str, default None
             text to display as header for the graph.

        Returns
        -------
        FigureWidget
             graph that displays the series at `index`.
        '''

        valid_index = self._get_valid_index(index)

        if valid_index != -1:
            return plot_lh(self._dataframe, index=valid_index, header=header)

    def update_detection_params(self, index=None, **params):
        '''Updates LH peak parameters for the series at `index` with values
        from `**params`.

        Parameters
        ----------

        index : int, default None
            one-based index of the series in data. If index is not
            specified, updates parameters for the current series.

        **params : dict, keyword arguments, dynpeak.core.utils.make_struct.<locals>._Struct
            LH peak values to update. Possible keywords are:

            detectionthreshold : float
                a real value >= 0
            globalreltol : float
                a real value in the interval [0,1]
            localabstol : float
                a real value > 0
            period : float
                a real value > 0
            threepointtol : float
                a real value in the interval [0,1]
            uncertainty : list/array of float
                a list/array of the form [[a1,a2],[b1,b2]] where
                a1, a2 >= 0, a1 < a2, b1, b2 in interval [0, 1]
                and b1 < b2.
            ignoredpoints : list/array of int
                list/array of sampling time indexes. Values in the list
                should be in range [0, n) where "n" equals the number
                of sampling signals.
            imposedpeaks : list/array of int
                list/array of sampling time indexes. Values in the list
                should be in range [0, n) where "n" equals the number
                of sampling signals.
            interval : list/array of int
                a list/array with two elements [start, end] where "start" <
                "end" and values are in range [0, n) where "n" equals the
                number of sampling signals.

        Returns
        -------
        bool
            `True` if peak detection parameters are updated; `False` otherwise.
        '''

        if params is not None:
            valid_index = self._get_valid_index(index)

            if valid_index != -1:
                try:

                    # remove selection if it exists in params
                    if 'selection' in params:
                        del params['selection']
                        logger.warn(
                            "Selection key is not allowed to be set, ignoring.")

                    interval = params.get('interval', None)
                    len_series = self._get_interval_size(interval, valid_index)

                    # check if params are valid
                    lhpeaks_check_options(make_struct(
                        **params), len_series, "update_detection_params")

                    # if interval set, reset all previous peak detection values
                    if interval:
                        self._lh_params[valid_index] = lhpeaks_default_params()

                    self._lh_params[valid_index].update(params)
                    return True

                except ValueError as e:
                    logger.error(e)
        return False

    def _get_interval_size(self, interval, valid_index):
        len_series = len(self._dataframe[self._dataframe.columns[valid_index]])

        if interval:
            # interval does not exist in original detection params,
            # I decided to integrate it inside rather than keeping it as a separate value for each series
            # But checking upper bound conflicts with selection value checks, therefore I check it here
            if not (isinstance(interval, list) and len(interval) == 2):
                raise ValueError(
                    "(0,) or (2,) dim. vector expected for interval.")
            if interval[0] < 0 or interval[1] > len_series:
                raise ValueError(f"Interval should be in [0, {len_series}].")
            if (interval[0] >= interval[1]):
                raise ValueError(
                    "Second element must be greater than the first one for interval.")
            len_series = interval[1] - interval[0] + 1

        return len_series

    def print_detection_params(self, index=None):
        '''Prints LH peak parameters for the series at `index`.

        Parameters
        ----------

        index : int, default None 
            one-based index of the series in data. If index is not
            specified, prints parameters for the current series.
        '''

        params = self.get_detection_params(index)
        if params is not None:
            print(f"""
            Detection threshold\t = {params.detectionthreshold} ng/ml
            Coefficient of variation:
                Range\t = {params.uncertainty[0]} ng/ml
                CV\t = {params.uncertainty[1]} %
            Global relative threshold\t = {params.globalreltol} %
            Local absolute threshold\t = {params.localabstol} %
            Nominal peaks period\t = {params.period} minutes
            Three point threshold\t = {params.threepointtol} %

            Interval\t = {params.interval}
            Ignored points\t = {params.ignoredpoints}
            Imposed peaks\t = {params.imposedpeaks}
             """)

    def get_detection_params(self, index=None):
        '''Returns LH peak parameters for the series at `index`.

        Parameters
        ----------

        index : int, default None
            one-based index of the series in data.

        Returns
        -------
        dynpeak.core.utils.make_struct.<locals>._Struct
            detection parameters for the series at `index`. If index
            is not specified, returns parameters for the current
            series. The returned structure is a copy of the values in
            dynpeak executer. Changing the values outside dynpeak
            executer does not affect the values inside.

        '''

        valid_index = self._get_valid_index(index)

        if valid_index != -1:
            params = lhpeaks_default_params()
            params.update(self._lh_params[valid_index])
            return params
        return None

    def detect_peaks(self, index=None, **params):
        '''Updates LH peak parameters for the series at `index` with values
        from `**params`, detects peaks and returns detection results.

        Parameters
        ----------
        index : int, default None
            one-based index of the series in data. If no `index` is
            specified, updates parameters for the current series and
            detects peaks on the current series.

        **params : dict, keyword arguments, dynpeak.core.utils.make_struct.<locals>._Struct
            LH peak detection values to update. Possible keywords are:

            detectionthreshold : float
                a real value >= 0
            globalreltol : float
                a real value in the interval [0,1]
            localabstol : float
                a real value > 0
            period : float
                a real value > 0
            threepointtol : float
                a real value in the interval [0,1]
            uncertainty : list/array of float
                a list/array of the form [[a1,a2],[b1,b2]] where
                a1, a2 >= 0, a1 < a2, b1, b2 in interval [0, 1]
                and b1 <= b2.
            ignoredpoints : list/array of int
                list/array of sampling time indexes. Values in the list
                should be in range [0, n) where "n" equals the number
                of sampling signals in the selected `interval`.
            imposedpeaks : list/array of int
                list/array of sampling time indexes. Values in the list
                should be in range [0, n) where "n" equals the number
                of sampling signals in the selected `interval`.
            interval : list/array of int
                a list/array with two elements [start, end] where "start" <
                "end" and values are in range [0, n] where "n" equals the
                number of sampling signals.

        Returns
        -------
        dict
            a dictionary that contains detection results with the
            following keys:

            lh_frame : pandas.DataFrame
                dataframe that contains the series for which
                detection is executed.
            ipi_frame : pandas.DataFrame
                dataframe that contains ipi, median, t-high and
                t-low series after detection.
            params : dynpeak.core.utils.make_struct.<locals>._Struct
                parameters used for detection
            detected_peaks : pandas.Series
                series that contains detected peak times and values.
        '''

        valid_index = self._get_valid_index(index)

        if valid_index != -1:
            if params is not None:
                updated = self.update_detection_params(index, **params)

                if not updated:
                    logger.error(
                        "Please specify correct peak detection parameters and retry!")
                    return None

            # select series to use for detection
            series = self._dataframe[self._dataframe.columns[valid_index]]

           # select detection parameters to use for detection
            params_series = self._lh_params[valid_index]

            try:
                lh_frame, ipi_frame, params_r, detected_peaks = detect_peaks(
                    series, params=params_series)
            except ValueError as e:
                logger.error(e)
                return

            detection_dict = dict(lh_frame=lh_frame, ipi_frame=ipi_frame,
                                  params=params_r, detected_peaks=detected_peaks)

            self._detection[valid_index] = detection_dict

            return detection_dict

        return None

    def detect_peaks_and_plot(self, index=None, **params):
        '''Updates LH peak parameters for the series at `index` with values
        from `**params`, detects peaks, and plots results.

        Parameters
        ----------
        index : int, default None
            one-based index of the series in data. If no `index` is
            specified, updates parameters and detects peaks on the
            current series.

        **params : dict, keyword arguments, dynpeak.core.utils.make_struct.<locals>._Struct
            LH peak detection values to update. Possible keywords are:

            detectionthreshold : float
                a real value >= 0
            globalreltol : float
                a real value in the interval [0,1]
            localabstol : float
                a real value > 0
            period : float
                a real value > 0
            threepointtol : float
                a real value in the interval [0,1]
            uncertainty : list/array of float
                a list/array of the form [[a1,a2],[b1,b2]] where
                a1, a2 >= 0, a1 < a2, b1, b2 in interval [0, 1]
                and b1 <= b2.
            ignoredpoints : list/array of int
                list/array of sampling time indexes. Values in the list
                should be in range [0, n) where "n" equals the number
                of sampling signals.
            imposedpeaks : list/array of int
                list/array of sampling time indexes. Values in the list
                should be in range [0, n) where "n" equals the number
                of sampling signals.
            interval : list/array of int
                a list/array with two elements [start, end] where "start" <
                "end" and values are in range [0, n) where "n" equals the
                number of sampling signals.

        Returns
        -------
        DetectionGraphWidget
            a subplot of two graphs that shows LH series and
            detection series.
        '''

        try:
            detection_dict = self.detect_peaks(index, **params)
        except ValueError as e:
            logger.error(e)

        if detection_dict is not None:
            return plot_detection_graph(detection_dict['lh_frame'], detection_dict['ipi_frame'], detection_dict['detected_peaks'], **detection_dict['params'])

        return None

    def plot_detection_graph(self, index=None):
        '''Displays a subplot of two graphs that shows LH series and detection
        series.

        Parameters
        ----------
        index : int, default None
            one-based index of the series in data. If no `index` is
            specified, displays detection graph for the current series.

        Returns
        -------
        DetectionGraphWidget
            a subplot of two graphs that shows LH series and
            detection series.

            If no detection is executed for the series at `index`
            displays an error log to user and returns.
        '''

        valid_index = self._get_valid_index(index)

        if valid_index != -1:
            detection_dict = self._detection[valid_index]
            if detection_dict is None:
                logger.error(
                    f"No detection is executed for the series at {valid_index + 1}.")
            else:
                return plot_detection_graph(detection_dict['lh_frame'], detection_dict['ipi_frame'], detection_dict['detected_peaks'], **detection_dict['params'])

        return None

    def print_peaks(self, index=None):
        '''Prints peak list for the series at `index`.

        If no detection is executed for the series at `index`,
        displays an error log.

        Parameters
        ----------
        index : int, default None
            one-based index of the series in data. If no `index` is
            specified, displays peaks detected for the current series.
        '''

        valid_index = self._get_valid_index(index)

        if valid_index != -1:
            peaks = self.get_peak_list(index)
            if peaks is not None:
                df = DataFrame(data=peaks)
                return view_all(df)
        return None

    def get_peak_list(self, index=None):
        '''Returns peak list for the series at `index`.

        Parameters
        ----------
        index : int, default None
            one-based index of the series in data. If no `index` is
            specified, displays peaks detected for the current series.

        Returns
        -------
        pandas.Series
            series that contains detected peak times and values.

            If no detection is executed for the series at `index`,
            displays an error log and returns `None`.
        '''

        valid_index = self._get_valid_index(index)

        if valid_index != -1:
            detection_dict = self._detection[valid_index]
            if detection_dict is None:
                logger.error(
                    f"No detection is executed for the series at {valid_index + 1}.")
            else:
                return detection_dict['detected_peaks']
        return None


class DynpeakExecuterWrapper(DynpeakExecuter):
    '''A wrapper class for DynpeakExecuter to be able to export notebook
    to other formats like pdf or html.
    '''

    def __init__(self):
        super().__init__()

    def view_head(self, n=10):
        super().view_head(n=n).show()

    def view_tail(self, n=10):
        super().view_tail(n=n).show()

    def view_all(self):
        super().view_all().show()

    def view_sampling_times(self):
        super().view_sampling_times().show()

    def plot_lh_all(self, header=None):
        super().plot_lh_all(header=header).show()

    def plot_lh(self, index=None, header=None):
        super().plot_lh(index=index, header=header).show()

    def detect_peaks_and_plot(self, index=None, **params):
        plot = super().detect_peaks_and_plot(index=index, **params)

        if plot is not None:
            plot.show()

    def plot_detection_graph(self, index=None):
        super().plot_detection_graph(index=index).show()
