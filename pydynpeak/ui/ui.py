# This file is part of PyDynPeak software
# Copyright (C) 2021, Inria
# 
# PyDynpeak is a Python software derived from:
#          DynPeak (Scilab ATOMS Toolbox) version 2.1.0 (authors:
#	   	   	   	Claire Medigue, Serge Steer, Qinghua Zhang,
#              			Alexandre Vidal, Frédérique Clément
#                               cf. https://atoms.scilab.org/toolboxes/Dynpeak)
#
# PyDynPeak is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# PyDynPeak is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with PyDynPeak.  If not, see <https://www.gnu.org/licenses/>
# 
# Parts of this file are based on work covered by the following copyright
#       and permission notice:
#      """
#      // Copyright (C) 2012 - INRIA - Serge Steer
#      // This file must be used under the terms of the CeCILL.
#      // This source file is licensed as described in the file COPYING, which
#      // you should have received as part of this distribution.  The terms
#      // are also available at
#      // http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#      """
###############################################################################
#
# Authors : Frédérique Clément(+), Hande Gözükan(*), Christian Poli(*)
#
# +=scientific advisor, *=developer
#
###############################################################################

from ipywidgets import Label, Button, Dropdown, FloatText, IntText, HTML, Accordion, Output, HBox, VBox, Layout, Text, Tab

from pydynpeak.core import lhpeaks_default_params, detect_peaks
from pydynpeak.vis import plot_lh, plot_detection_graph
from pydynpeak.model import DynEnv

'''
This module generates the user interfaces generated in "dynpeak_batch" in scilab API.
It consists of 
- a menu to select the data series(signal) in a data file
- a menu to update Settings 
- a menu to detect peaks

It also contains functionality to add or ignore peaks.
'''

class DetectListener:
    '''
    Interface to trigger peak detection. 
    '''

    def on_detect(self, selection_index): raise NotImplementedError
    

class Gui:
    '''
    Base GUI class to initiate Dynpeak environment DynEnv.
    '''

    def __init__(self, env=None):
        self._env = DynEnv() if env is None else env # load from a file with current notebook name

        if not self._env.exists('currentselection'):
            self._env.set('currentselection', None)
            
        if not self._env.exists('detectionparams'):
            self._env.set('detectionparams', lhpeaks_default_params())
            

class DetectGui(Gui, Tab):
    '''
    '''

    def __init__(self, env=None, dataframe=None):
        Gui.__init__(self, env)
        Tab.__init__(self)

        self.layout = { 'margin': '10px' }
        self.children = self._init_widgets(dataframe)
        
        self.set_title(0, 'Data')
        self.set_title(1, 'Settings')
        self.set_title(2, 'Detection')

    def _init_widgets(self, dataframe):
        '''
        Initializes each Tab of the GUI.
        '''
        data_view = DataView(self, self._env, dataframe)
        settings_menu = SettingsMenu(self, self._env)
        detect_view = DetectView(self, self._env, dataframe)

        data_view.set_detect_listener(detect_view)

        return [data_view, settings_menu, detect_view]
        
        

class DataView(Gui, VBox, DetectListener):
    '''
    A user interface to list the Series of a pandas DataFrame and visualize the graph upon selection of Series.
    '''
    
    def __init__(self, parent= None, env=None, dataframe=None, detectlistener=None):
        Gui.__init__(self, env)
        VBox.__init__(self)

        self._parent = parent
        self._dataframe = dataframe
        self.listener_ = detectlistener
        self.children = self._init_widgets()

    def _init_widgets(self):
        '''
        Initializes the widgets of the DataView.
        It contains a label, a dropdown to view the list of series the in dataframe, and an output widget to display the graph. Output widgets helps to clean the graph, and plot new one each time selection changes.
        It also has a Detection button which functions if a detectlistener is set for DataView. Detectlistener 
        '''
        label = Label(
            layout = { 'margin' : '10px' },
            value = 'Data:'
        )

        # dropdown to select series in the dataframe, adds "All" option to be able to view all series in the same plot
        series_dropdown = Dropdown(
            layout = { 'margin' : '10px' },
            options = ['All'] + list(self._dataframe.columns),
            value = None
        )

        # button to detect peak intervals
        detect_btn = Button(
            layout = {'margin' : '10px 0px 0px 5px'},
            description = 'Detection'
        )


        # output that displays graph
        output_data = Output(
            layout = { 'border': '1px solid black', 'margin': '10px', 'padding': '5px' }
        )
                    
        def on_selection_change(change):
            if change['type'] == 'change' and change['name'] == 'value':
                output_data.clear_output( wait = True )
                if series_dropdown.index is 0:
                    detect_btn.disabled = True
                    self._env.set('currentselection', None)
                else:
                    detect_btn.disabled = False
                    self._env.set('currentselection', series_dropdown.index - 1 )

                selection = self._env.get('currentselection')
                with output_data:
                    if selection is None:
                        display(plot_lh_all(self._dataframe))
                    else:
                        display(plot_lh_byindex(self._dataframe, index=selection))

        series_dropdown.observe(on_selection_change)

        def on_detect(b):
            if self._listener is None:
                print("No detection listener is set.")
            else:
                self._listener.on_detect(self._env.get('currentselection'))

        detect_btn.on_click(on_detect)

        
        # select currentselection in env 
        series_dropdown.index = 0 if self._env.get('currentselection') is None else self._env.get('currentselection')

        return [HBox([label, series_dropdown, detect_btn]), output_data] 
        

    @property
    def selection_index(self):
        '''
        Returns the index of the selected Series in the pandas DataFrame; None if no Series is selected.
        '''
        return self._env.get('currentselection')

    def set_detect_listener(self,listener):
        self._listener = listener

    def remove_detect_listener():
        self._listener = None


class DetectView(Gui, VBox, DetectListener):
    '''
    '''

    def __init__(self, parent=None, env=None, dataframe=None):
        Gui.__init__(self, env)
        VBox.__init__(self)

        self._parent = parent
        self._dataframe = dataframe
        self._graph = None
        self.children = self._init_widgets()

    def _init_widgets(self):
        title = HTML(
            layout = { 'margin': '30px 0px 10px 0px'}, 
            value = '<center><b>Peak Detection Tool<br>'
        )

        # output to display graphs
        self._out_peak_detect = Output(
            layout = { 'border': '1px solid black', 'margin': '10px', 'padding': '5px' }
        )

        # button to detect peak intervals
        detect_btn = Button(
            layout = {'margin' : '10px 0px 10px 5px'},
            description = 'Detection'
        )

        def on_detect_btn(b):
            self.on_detect(self._env.get("currentselection"))

            
        detect_btn.on_click(on_detect_btn)
        # button to impose peaks
        peak_impose_btn = Button(
            layout = {'margin' : '10px 0px 10px 5px'},
            description = 'Impose Peak'
        )

        def on_impose_peak(b):
            imposed_peaks = self._env.get("detectionparams.imposedpeaks")
            imposed_peaks.append(1)
            self._env.set("detectionparams.imposedpeaks", imposed_peaks)
#            print(self._env.get("detectionparams"))

        peak_impose_btn.on_click(on_impose_peak)

        # button to impose peaks
        peak_ignore_btn = Button(
            layout = {'margin' : '10px 0px 10px 5px'},
            description = 'Ignore Peak'
        )

        def on_ignore_peak(b):
            ignored_peaks = self._env.get("detectionparams.imposedpeaks")
            ignored_peaks.append(2)
            self._env.set("detectionparams.ignoredpoints", ignored_points)

#            printself._env.get("detectionparams"))

        peak_ignore_btn.on_click(on_ignore_peak)

        button_row = HBox([detect_btn, peak_impose_btn, peak_ignore_btn])

        return [ button_row, title, self._out_peak_detect ]

    def on_detect(self, selection_index):
        if self._parent is not None:
            self._parent.selected_index = 2

        self._out_peak_detect.clear_output( wait = True )

        if selection_index is None:
            with self._out_peak_detect:
                print("Please select a series.")
                return
        # add params as from env
        lh_frame, ipi_frame, params, detected_peaks = detect_peaks(self._dataframe, selection_index, self._env.get('detectionparams'))

        self._env.set('detectionparams', params)

        with self._out_peak_detect:
            self._graph = plot_detection_graph(lh_frame, detected_peaks, ipi_frame)
            display(self._graph)

        
class SettingsMenu(Gui, VBox):
    '''
    '''

    def __init__(self, parent=None, env=None):
        Gui.__init__(self, env)
        VBox.__init__(self)

        self._parent = parent
        self.children = self._init_widgets()
        self.set_values(self._env.get('detectionparams'))

    def _init_widgets(self):
        layout_label = Layout(margin = '5px 10px 0px 5px', width = '200px')
        layout_text_large = Layout(margin = '5px 0px 0px px', width = '70px')
        layout_text = Layout(margin = '5px 0px 0px 0px', width = '60px') 

        self._det_thrshd = FloatText(layout = layout_text )
        det_thrshd_box = HBox([ Label( value = 'Detection threshold:', layout = layout_label ), self._det_thrshd, Label( value = "ng/ml", layout = layout_label ) ])

        # coefficient of variation
        cov_label = Label( value = 'Coefficient of variation:', layout = layout_label )

        read_btn = Button(
            layout = { 'margin': '10px 10px 10px 100px' },
            description = 'Read'
        )

        show_btn = Button(
            layout = { 'margin': '10px 10px 10px 0px' },
            description = 'Show'
        )

        btn_hbox = HBox([ read_btn, show_btn ])

        range = Text( value = '[0, 1000]', layout = layout_text_large, width = '200px' )
        range_box = HBox([ Label( value = 'Range:', layout = layout_label), range, Label( value = ' ng/ml', layout = layout_label ) ])

        cv = Text( value = '', layout = layout_text_large, width = '200px' )
        cv_box = HBox([ Label( value = 'CV:', layout = layout_label), range, Label( value = ' %', layout = layout_label ) ])

        cov_box = VBox([ cov_label, btn_hbox, range_box, cv_box])
    
        # advanced parameters
        adv_param_label = HTML( value = '<b>Advanced Parameters</b>' )

        self._global_rel_th = FloatText(layout = layout_text )
        global_rel_th_box = HBox([ Label( value = 'Global relative threshold:', layout = layout_label), self._global_rel_th, Label( value = '%', layout = layout_label ) ])

        self._local_abs_th = FloatText(layout = layout_text )
        local_abs_th_box = HBox([ Label(value = 'Local absolute threshold:', layout = layout_label ), self._local_abs_th, Label( value = 'ng/ml', layout = layout_label ) ])

                
        self._nominal_peak_per = IntText(layout = layout_text)
        nominal_peak_per_box = HBox([ Label(value = 'Nominal peaks period:', layout = layout_label), self._nominal_peak_per, Label( value = 'minutes', layout = layout_label ) ])

        self._thr_poi_th = FloatText( layout = layout_text )
        thr_poi_th_box= HBox([ Label(value = 'Three point threshold:', layout = layout_label), self._thr_poi_th, Label( value = '%', layout = layout_label ) ])


        adv_param_box =  VBox([ adv_param_label, global_rel_th_box, local_abs_th_box, nominal_peak_per_box, thr_poi_th_box ])

        # buttons

        # default button loads the default values
        def on_default_btn_click(b):
            self.set_values(lhpeaks_default_params())

        default_btn = Button(
            layout = {'margin' : '10px 10px 0px 0px'},
            description = 'Default'
        )

        default_btn.on_click(on_default_btn_click)

        
        save_btn = Button(
            layout = {'margin' : '10px 10px 0px 0px'},
            description = 'Save'
        )
        
        load_btn = Button(
            layout = {'margin' : '10px 10px 0px 0px'},
            description = 'Load'
        )

       
        # ok button saves the values to params. ? TODO should it redetect?
        def on_ok_btn_click(b):
            self.update_params()
            
        ok_btn = Button(
            layout = {'margin' : '10px 10px 0px 0px'},
            description = 'Ok'
        )

        ok_btn.on_click(on_ok_btn_click)

        # cancel button resets to initial values
        def on_cancel_btn_click(b):
            self.set_values(self._env.get('detectionparams'))
            
        cancel_btn = Button(
            layout = {'margin' : '10px 10px 0px 0px'},
            description = 'Cancel'
        )

        cancel_btn.on_click(on_cancel_btn_click)

        btn_box = HBox([default_btn, save_btn, load_btn, ok_btn, cancel_btn])
        
        return [ det_thrshd_box, cov_box, adv_param_box, btn_box ]



    def set_values(self, params):
        '''
        Sets the values in the specified 'params' parameters to the fields in the SettingMenu GUI.
        '''
        self._det_thrshd.value = params.detectionthreshold
        # TODO add values for Range and CV
        self._global_rel_th.value = params.globalreltol * 100
        self._local_abs_th.value = params.localabstol
        self._nominal_peak_per.value = params.period
        self._thr_poi_th.value = params.threepointtol * 100

    def update_params(self):
        '''
        Updates detectionparams with the values from SettingMenu GUI.
        '''
        self._env.set('detectionparams.detectionthreshold', self._det_thrshd.value)
        # TODO add values for Range and CV
        self._env.set('detectionparams.globalreltol', self._global_rel_th.value / 100)
        self._env.set('detectionparams.localabstol', self._local_abs_th.value)
        self._env.set('detectionparams.period', self._nominal_peak_per.value)
        self._env.set('detectionparams.threepointtol', self._thr_poi_th.value / 100)

