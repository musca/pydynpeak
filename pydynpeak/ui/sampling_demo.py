# This file is part of PyDynPeak software
# Copyright (C) 2021, Inria
# 
# PyDynpeak is a Python software derived from:
#          DynPeak (Scilab ATOMS Toolbox) version 2.1.0 (authors:
#	   	   	   	Claire Medigue, Serge Steer, Qinghua Zhang,
#              			Alexandre Vidal, Frédérique Clément
#                               cf. https://atoms.scilab.org/toolboxes/Dynpeak)
#
# PyDynPeak is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# PyDynPeak is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with PyDynPeak.  If not, see <https://www.gnu.org/licenses/>
# 
# Parts of this file are based on work covered by the following copyright
#       and permission notice:
#      """
#      // Copyright (C) 2012 - INRIA - Serge Steer
#      // This file must be used under the terms of the CeCILL.
#      // This source file is licensed as described in the file COPYING, which
#      // you should have received as part of this distribution.  The terms
#      // are also available at
#      // http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#      """
###############################################################################
#
# Authors : Frédérique Clément(+), Hande Gözükan(*), Christian Poli(*)
#
# +=scientific advisor, *=developer
#
###############################################################################

from ipywidgets import Label, Button, Dropdown, HTML, HTMLMath, Output, HBox, VBox, Layout

from pydynpeak.vis import plot_lh
from pydynpeak.core import show_sampling_effect

mspike1 = lambda t : 15  - 0.0087 * t

mspike2 = lambda t : 15

pspike1 = lambda t : 60

pspike2 = lambda t : 60 - t / 50 

class SamplingDemo(VBox):
    '''
    '''
    
    # ------------------------
    # synthetic LH signal parameter widgets
    # ------------------------

    def __init__(self):
        VBox.__init__(self)

        self.children = self._init_widgets()

    def _init_widgets(self):
        '''
        '''

        layout_label = Layout(margin = '10px 10px 0px 0px', width = '200px')
        layout_dropdown = Layout(margin = '', width = '120px')

        topheading = HTMLMath(
            layout = {'margin': '30px 0px 0px 0px'},
            value = '<h1>Sampling Effect Demo</h1><p>This demo illustate the effect of the initial sample date, of the uncertainty on the sample dates and on the measured LH level when the sampling period is quite high relatively to the LH signal period. The demo is based on generated synthetic LH signals which can have constant or varying amplitude and period.</p>'
        )
    
        slhheading = HTML(
            layout = {'margin': '30px 0px 40px 0px'},
            value = '<b>Synthetic LH signal</b>'
        )

        slhlabel = HTMLMath(
        layout = { 'margin': '0px 0px 20px 0px'}, 
            value = r'$$\begin{equation*}\frac{d LH}{d t} = M_{spike}(t) e^{-k_{hl} (t-floor(\frac{t}{P_{spike}(t)})P_{spike}(t))} - 6 LH(T)\end{equation*}$$'
        )

        mlabel = HTMLMath(
            layout = layout_label,
            value = r'$$M_{spike}(t)$$',
            width = 200
        )

        mdropdown = Dropdown(
            layout = layout_dropdown,
            options = {'15 - 0.0087t': mspike1, '15 ng/(ml.min)': mspike2 },
            value = mspike2
        )


        plabel = HTMLMath(
            layout = layout_label,
            value = r'$$P_{spike}(t)$$'
        )

        pdropdown = Dropdown(
            layout = layout_dropdown,
            options = { '60 min': pspike1, '60-t/50 min': pspike2 },
            value = pspike2
        )

        h1 = HBox([ mlabel, mdropdown ])
        h2 = HBox([ plabel, pdropdown ])

        slhwidgets = VBox([ topheading, slhheading, slhlabel, h1, h2])

        # ------------------------
        # sampling parameter widgets
        # ------------------------

        splabel = HTML(
            layout = { 'margin': '30px 0px 10px 0px'}, 
            value = '<b>Sampling Parameters</b>'
        )

        sdlabel = Label(
            layout = layout_label,
            value = 'First sample date'
        )

        sddropdown = Dropdown(
            layout = layout_dropdown,
            options = { '1 min': 1.0, '2 min': 2.0, '4 min': 4.0 },
            value = 4.0
        )


        usdlabel = Label(
            layout = layout_label,
            value = 'Uncertainty on sample data'
        )

        usddropdown = Dropdown(
            layout = layout_dropdown,
            options = { '0.5 min': 0.5, '1.5 min': 1.5, '2 min': 2.0 },
            value = 1.5
        )

        ulhlabel = Label(
            layout = layout_label,
            value = 'Uncertainty on LH measure'
        )

        ulhdropdown = Dropdown(
            layout = layout_dropdown,
            options = { '2%': 0.02, '5%': 0.05, '10%': 0.1 },
            value = 0.05
        )

        h3 = HBox([ splabel ])
        h4 = HBox([ sdlabel, sddropdown ])
        h5 = HBox([ usdlabel, usddropdown ])
        h6 = HBox([ ulhlabel, ulhdropdown ])

        spwidgets = VBox([ h3, h4, h5, h6])

        out_graph = Output(
            layout = { 'border': '1px solid black', 'margin': '10px', 'padding': '5px', 'height': '1700px' }
        )
    
        showbtn = Button(
            layout = { 'margin': '20px 0px 10px 10px'},
            description = 'Show'
        )

        def on_show_clicked(b):
            out_graph.clear_output( wait = True )
            with out_graph:
                title = HTML(
                    layout = { 'margin': '10px 0px 0px 0px'}, 
                    value = '<center><b>Effect of Sampling Process<br>Mspike={}, Pspike={}, r={}, ts=10+/-{}, b={}</b></center>'.format(mdropdown.label, pdropdown.label, sddropdown.label, usddropdown.label, ulhdropdown.label )
                )
                display(title)

            synthetic_frame, sampled_frame, ipi_frame = show_sampling_effect(mdropdown.value, pdropdown.value, sddropdown.value, usddropdown.value, ulhdropdown.value)
        
            with out_graph:
                display(plot_lh(synthetic_frame, index=0, header='Synthetic Signal', **dict(mode='lines')))
                display(plot_lh(sampled_frame, header='Sampled Signal', **dict(mode='lines')))
                display(plot_lh(ipi_frame, header='Inter Peak Intervals', **dict(x_title='Time (min)', y_title='IPI (min)', mode='lines')))


        showbtn.on_click(on_show_clicked)

        return [ slhwidgets, spwidgets, showbtn, out_graph ]

