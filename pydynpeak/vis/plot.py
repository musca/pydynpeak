# This file is part of PyDynPeak software
# Copyright (C) 2021, Inria
# 
# PyDynpeak is a Python software derived from:
#          DynPeak (Scilab ATOMS Toolbox) version 2.1.0 (authors:
#	   	   	   	Claire Medigue, Serge Steer, Qinghua Zhang,
#              			Alexandre Vidal, Frédérique Clément
#                               cf. https://atoms.scilab.org/toolboxes/Dynpeak)
#
# PyDynPeak is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# PyDynPeak is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with PyDynPeak.  If not, see <https://www.gnu.org/licenses/>
# 
###############################################################################
#
# Authors : Frédérique Clément(+), Hande Gözükan(*), Christian Poli(*)
#
# +=scientific advisor, *=developer
#
###############################################################################

from .plot_plotly import plot_table, plot_graph, plot_detection_subplot, get_bounds

import pandas as pd


'''
This module provides an interface to view contents of a dataframe, or series in a dataframe as a table or a graph.
'''

def view_head(dataframe, n=10):
    '''Displays first `n` rows of the `dataframe`. 

    Parameters
    ----------

    dataframe : pandas.DataFrame
        Pandas `DataFrame` that contains data to visualize.

    n : int, default 10
        number of rows to be shown.

    Returns
    -------
    FigureWidget 
        table that displays first `n` rows of `dataframe`.
    '''

    return plot_table(dataframe.head(n))


def view_tail(dataframe, n=10):
    '''Displays last `n` rows of the `dataframe`. 

    Parameters
    ----------

    dataframe : pandas.DataFrame
        Pandas `DataFrame` that contains data to visualize.

    n : int, default 10
        number of rows to be shown.

    Returns
    -------
    FigureWidget 
        table that displays last `n` rows of `dataframe`.
    '''
    
    return plot_table(dataframe.tail(n))


def view_all(dataframe):
    '''Displays all rows in `dataframe`.

    Parameters
    ----------

    dataframe : pandas.DataFrame
        Pandas `DataFrame` that contains data to visualize.

    Returns
    -------
    FigureWidget
        table that displays all rows in `dataframe`.
    '''

    return plot_table(dataframe)

    
def view_sampling_times(dataframe):
    '''Displays index values of `dataframe`.

    Parameters
    ----------

    dataframe : pandas.DataFrame
        Pandas `DataFrame` that contains data to visualize.

    Returns
    -------
    FigureWidget
        table that displays index values of `dataframe`.
    '''
    dataframe = pd.DataFrame(dataframe.index.values, columns=['Sampling times'])
    return plot_table(dataframe, withindex=False)

    
def plot_lh(dataframe, index=None, header=None, **props):
    '''Displays the series in `dataframe` at `index`.

    Parameters 
    ---------- 

    dataframe: pandas.DataFrame
        Pandas `DataFrame` that contains data to visualize.

    index : int, optional 

        None or a value between 1 and the number of the series in
        `dataframe` (inclusive). If `None` all series in `dataframe`
        are displayed; otherwise displays the series at `index`.

    header : str, optional
         text to display as header for the graph. If not set,
         defaults to name of the selected series.

    Returns
    -------
    FigureWidget
         graph that displays the series in `dataframe` at `index` or
         all series if `index` is `None`.
    '''

    props_lh = dict(x_title='Time (min)', y_title='LH (ng/ml)', mode='lines+markers', showlegend=True)

    if index is not None:
        series = dataframe[dataframe.columns[index]]

        dataframe = pd.DataFrame(series, index=dataframe.index)
        header = series.name if header is None else header
        props_lh.update(showlegend=False)
    
    # set bounds
    props_lh.update(get_bounds(dataframe))

    if props is not None:
        props_lh.update(props)

    return plot_graph(dataframe, header, **props_lh)
    

def plot_detection_graph(dataframelh, dataframeipi, detected_peaks, **params):
    '''
    '''
    lh_series = dataframelh[dataframelh.columns[0]]
    return plot_detection_subplot(lh_series, dataframeipi, detected_peaks, **params)
