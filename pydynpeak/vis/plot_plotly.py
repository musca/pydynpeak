# This file is part of PyDynPeak software
# Copyright (C) 2021, Inria
#
# PyDynpeak is a Python software derived from:
#          DynPeak (Scilab ATOMS Toolbox) version 2.1.0 (authors:
#	   	   	   	Claire Medigue, Serge Steer, Qinghua Zhang,
#              			Alexandre Vidal, Frédérique Clément
#                               cf. https://atoms.scilab.org/toolboxes/Dynpeak)
#
# PyDynPeak is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PyDynPeak is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PyDynPeak.  If not, see <https://www.gnu.org/licenses/>
#
# Parts of this file are based on work covered by the following copyright
#       and permission notice:
#      """
#      // Copyright (C) 2012 - INRIA - Serge Steer
#      // This file must be used under the terms of the CeCILL.
#      // This source file is licensed as described in the file COPYING, which
#      // you should have received as part of this distribution.  The terms
#      // are also available at
#      // http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#      """
###############################################################################
#
# Authors : Frédérique Clément(+), Hande Gözükan(*), Christian Poli(*)
#
# +=scientific advisor, *=developer
#
###############################################################################

import plotly.graph_objects as go
import plotly.io as pio
from plotly.subplots import make_subplots

import pandas as pd
import numpy as np

# default renderer is 'plotly_mimetype+notebook', pdf is
# added to be able to export notebook to pdf including output graphs
pio.renderers.default = 'plotly_mimetype+notebook+pdf'

# default table style values
T_HEADER_COLOR = '#bdccdb'
T_CELL_COLOR = '#ffffff'
T_HEIGHT = 260
T_MAX_WIDTH = 900
T_MIN_WIDTH = 250
T_COL_WIDTH = 100

# default graph style values
G_HEIGHT = 500
G_WIDTH = 800
G_XTITLE = 'x axis'
G_YTITLE = 'y axis'
G_MODE = 'lines'
G_SHOWGRID = False
G_SHOWLEGEND = False
G_LINEWIDTH = 1
G_MARKERSIZE = 4
G_DASH = "solid"
G_VISIBLE = True


def plot_table(dataframe, withindex=True, indexheader='Sampling times'):
    '''Generates and returns table that displays the contents of
    `dataframe`.

    Parameters
    ----------

    dataframe : pandas.DataFrame
        dataframe that contains data to visualize.

    withindex : bool
        a boolean value to indicate if the index of dataframe is
        viewed as first column.

    Returns
    -------
    FigureWidget
        table that displays contents of `dataframe`.

    '''

    if withindex:
        dataframe = dataframe.copy(deep=True)
        index_series = pd.Series(dataframe.index.to_numpy(
            dtype='uint32'), index=dataframe.index)
        dataframe.insert(0, column=indexheader, value=index_series)

    if isinstance(dataframe, pd.DataFrame):
        formatk = [['.3f'] * dataframe.shape[0] if i.name == 'float64' or i.name ==
                   'float32' else [''] * dataframe.shape[0] for i in dataframe.dtypes]
    else:
        formatk = ['']

    columns = dataframe.columns

    # set table column headers and cell values.
    table = go.Table(
        header=dict(
            values=columns,
            fill=dict(
                color=T_HEADER_COLOR
            ),
            line=dict(color='grey')
        ),
        cells=dict(
            values=dataframe.T,
            fill=dict(
                color=T_CELL_COLOR
            ),
            line=dict(color='grey'),
            format=formatk,
            align=['right'] * 5
        )
    )

    twidth = T_COL_WIDTH * len(columns)

    if twidth < T_MIN_WIDTH:
        twidth = T_MIN_WIDTH
    elif twidth > T_MAX_WIDTH:
        twidth = T_MAX_WIDTH

    # set table layout
    layout = go.Layout(
        height=T_HEIGHT,
        width=twidth,
        margin=go.layout.Margin(
            l=5,
            r=30,
            t=15,
            b=10,
            pad=10
        )
    )

    return go.FigureWidget([table], layout)


def plot_graph(dataframe, header=None, **props):
    '''Creates and returns a FigureWidget that displays the graph for the specified `dataframe`.

    Parameters
    ----------

    dataframe :
        pandas dataframe that contains series to plot.

    header :
        text to display as header for the graph.

    props :
        graph properties TODO detail this

    Returns
    -------
    A plotly FigureWidget that displays the graph.
    '''

    data = []
    for column in dataframe.columns:
        data.append(_get_scatter(dataframe[column], **props))

    layout = _create_layout(dataframe, header, **props)

    return go.FigureWidget(data, layout)


def plot_detection_subplot(lh_series, dataframeipi, detected_peaks, **params):
    '''

    '''

    return DetectionGraphWidget(lh_series, dataframeipi, detected_peaks, **params)


def _create_layout(data, header='', **props):
    '''
    '''
    x = data.index
    x_min = props.get('x_min', x[0])
    x_max = props.get('x_max', x[len(x) - 1])

    s_min = props.get('y_min', data.min().min())
    s_max = props.get('y_max', data.max().max())

    layout = go.Layout(
        title=header,
        height=props.get('height', G_HEIGHT),
        width=props.get('width', G_WIDTH),
        xaxis=go.layout.XAxis(
            title=props.get('x_title', G_XTITLE),
            range=[x_min, x_max],
            showgrid=G_SHOWGRID,
            ticks="outside",
            type="linear",
            showline=True,
            ticklen=4,
            nticks=10,
            linewidth=0.5,
            linecolor='black',
            mirror=True
        ),
        yaxis=go.layout.YAxis(
            title=props.get('y_title', G_YTITLE),
            anchor="x",
            range=[s_min, s_max],
            showgrid=G_SHOWGRID,
            ticks="outside",
            type="linear",
            showline=True,
            ticklen=4,
            nticks=10,
            linewidth=0.5,
            linecolor='black',
            mirror=True,
            zeroline=False
        ),
        dragmode="zoom"
    )
    return layout


def _get_scatter(series, **props):
    '''Creates a trace for the specified `series` with the specified `props` values.

    Parameters
    ----------

    series :
        pandas series that has a name to be used as trace name,
        index to be used as x values and the series itself as y values
        of the trace.

    props :
        a dictionary with plotly trace API keys:
        name
        mode
        line_dash
        marker_size
        marker_color
        marker_symbol
        visible
        showlegend
        These are the only implemented keys, update implementation to use other keys.


    Returns
    -------
    plotly.graph_objects.Scatter trace.

    '''
    series = series.dropna()
    return go.Scatter(
        x=series.index,
        y=series,
        customdata=props.get('customdata', None),
        hovertemplate=props.get('hovertemplate', None),
        name=props.get('name', series.name),
        mode=props.get('mode', G_MODE),
        line_width=G_LINEWIDTH,
        line_dash=props.get('line_dash', G_DASH),
        marker_size=props.get('marker_size', G_MARKERSIZE),
        marker_color=props.get('marker_color', None),
        marker_symbol=props.get('marker_symbol', None),
        visible=props.get('visible', G_VISIBLE),
        showlegend=props.get('showlegend', G_SHOWLEGEND),
        xaxis=props.get('xaxis', 'x'),
        yaxis=props.get('yaxis', 'y')
    )


def get_bounds(data):
    '''Finds the min and max values for `data` index and the min and
    max values of all series it contains.

    Parameters
    ----------

    data :

    Returns
    -------

    Returns a dict that contains x_min, x_max as min-max index values
    and y_min, y_max as min-max series values.

    '''

    # find min-max values for index
    x = data.index
    x_min = x[0]
    x_max = x[len(x) - 1]

    # find min-max values for (all) series
    if isinstance(data, pd.DataFrame):
        # dataframe.min() returns a series of min values of all series in the dataframe.
        d_max = data.max().max()
    else:
        d_max = data.max()

    y_max = 1.5 * d_max
    y_min = y_max - 2 * d_max

    return dict(x_min=x_min, x_max=x_max, y_min=y_min, y_max=y_max)


# ------------------------------------------------

class DetectionGraphWidget(go.FigureWidget):

    title = 'Peak Detection'
    title_x = 'Time (min)'

    name_lh = 'LH'
    title_lh_y = 'LH (ng/ml)'
    name_peak = 'detected peaks'
    name_selection = 'selection'

    title_ipi_y = 'Inter peak interval'

    def __init__(self, lh_series, dataframeipi, detected_peaks, **params):
        super().__init__(make_subplots(rows=2, cols=1, vertical_spacing=0.2))

        bounds_lh = get_bounds(lh_series)

        self.update_layout(title=self.title,
                           width=G_WIDTH,
                           height=1.5*G_HEIGHT)

        self._plot_detection_lh(lh_series, detected_peaks, params, bounds_lh)
        self._plot_detection_ipi(dataframeipi, bounds_lh)

    def _plot_detection_lh(self, lh_series, detected_peaks, params, bounds):
        '''
        '''
        self._add_lh_series(lh_series, detected_peaks, params)
        self._update_lh_axes(bounds)
        self._lh_peaklines = self._set_peak_lines(
            detected_peaks, params, bounds)

    def _plot_detection_ipi(self, dataframeipi, bounds):
        '''
        '''
        self._add_ipi_series(dataframeipi, bounds)
        self._update_ipi_axes(bounds)

    def _add_lh_series(self, lh_series, detected_peaks, params):
        '''
        '''
        # add LH trace
        props = dict(mode='lines+markers',
                     name=self.name_lh,
                     customdata=np.arange(0, len(lh_series)),
                     hovertemplate='LH:  %{y} <br>Time:  %{x} <br>Time index:  %{customdata}',
                     showlegend=True)
        self.add_trace(_get_scatter(lh_series, **props))
        trace_lh = self.data[0]

        # series to show selected point(s)
        selection = pd.Series(name=self.name_selection, dtype="float64")
        props.update(dict(visible=True,
                          name='',
                          mode='markers',
                          hovertemplate='LH:  %{y}',
                          marker_size=10,
                          marker_color='red',
                          marker_line_width=3,
                          showlegend=False))
        self.add_trace(_get_scatter(selection, **props))
        trace_selection = self.data[1]

        # add peak trace
        props.update(dict(visible='legendonly',
                          mode='markers',
                          marker_size=10,
                          marker_color='red',
                          marker_symbol='cross',
                          name=self.name_peak))
        self.add_trace(_get_scatter(detected_peaks, **props))
        trace_peaks = self.data[2]

        ign_pts = params.get('ignoredpoints', [])
        props.update(dict(visible=True,
                          name='ignored points',
                          mode='markers',
                          marker_size=10,
                          marker_color='Gold',
                          marker_symbol='x',
                          marker_line_width=0,
                          showlegend=True))
        self.add_trace(_get_scatter(lh_series.iloc[ign_pts], **props))

        imp_peaks = params.get('imposedpeaks', [])
        props.update(dict(visible=True,
                          name='imposedpeaks',
                          mode='markers',
                          marker_size=10,
                          marker_color='Magenta',
                          marker_symbol='triangle-up',
                          marker_line_width=0,
                          showlegend=True))
        self.add_trace(_get_scatter(lh_series.iloc[imp_peaks], **props))

        # set event handlers

        def peaks_on_click(trace, points, selector):
            if len(points.xs) > 0:
                selection_x = points.xs[0]
                selection_y = points.ys[0]

                if np.isin(selection_x, trace_selection.x) and np.isin(selection_y, trace_selection.y):
                    self._remove_selection(
                        trace_selection, selection_x, selection_y)
                else:
                    self._add_selection(
                        trace_selection, selection_x, selection_y)

        trace_lh.on_click(peaks_on_click)
        trace_peaks.on_click(peaks_on_click)

    def _update_lh_axes(self, bounds):
        '''
        '''
        self.update_xaxes(dict(title_text=self.title_x,
                               ticks='outside',
                               ticklen=4,
                               nticks=10,
                               showline=True,
                               linewidth=0.5,
                               linecolor='black',
                               mirror=True,
                               showgrid=G_SHOWGRID,
                               range=[bounds['x_min'], bounds['x_max']]),
                          row=1, col=1)

        self.update_yaxes(dict(title_text=self.title_lh_y,
                               ticks='outside',
                               ticklen=4,
                               nticks=10,
                               showline=True,
                               linewidth=0.5,
                               linecolor='black',
                               showgrid=G_SHOWGRID,
                               mirror=True,
                               range=[bounds['y_min'], bounds['y_max']],
                               zeroline=False),
                          row=1, col=1)

    def _set_peak_lines(self, detected_peaks, params, bounds):
        '''
        '''
        lines = []
        for i in detected_peaks.index:
            lines.append(go.layout.Shape(
                type="line",
                opacity=0.5,
                x0=i,
                y0=bounds['y_min'],
                x1=i,
                y1=bounds['y_max'],
                xref='x1',
                yref='y1',
                line=go.layout.shape.Line(color="grey", width=0.5)
            ))

        lines.append(go.layout.Shape(
            type="line",
            opacity=0.5,
            x0=0,
            y0=params.get('detectionthreshold', 0),
            x1=bounds['x_max'],
            y1=params.get('detectionthreshold', 0),
            xref='x1',
            yref='y1',
            line=go.layout.shape.Line(color="red", width=0.5)
        ))

        self.update_layout(shapes=lines)
        return lines

    def _add_ipi_series(self, dataframeipi, bounds):
        '''
        '''
        props = dict(mode='lines+markers', showlegend=True, hovermode='x',
                     hovertemplate='ipi:  %{y}',
                     xaxis='x2', yaxis='y2')
        self.add_trace(_get_scatter(dataframeipi['ipi'], **props))
        trace_ipi = self.data[5]

        # add median
        props.update(dict(mode='lines',
                          line_dash='dashdot',
                          hovertemplate='median:  %{y}'
                          ))
        self.add_trace(_get_scatter(dataframeipi["median"], **props))

        # add t-high
        props.update(dict(mode='lines',
                          line_dash='dot',
                          hovertemplate='t-high:  %{y}'
                          ))
        self.add_trace(_get_scatter(dataframeipi["t-high"], **props))

        # add t-low
        props.update(dict(mode='lines',
                          line_dash='dash',
                          hovertemplate='t-low:  %{y}'
                          ))
        self.add_trace(_get_scatter(dataframeipi["t-low"], **props))

        # series to show selected point
        selection = pd.Series(name=self.name_selection, dtype="float64")
        props.update(dict(mode='markers', marker_size=10,
                          marker_color='red', showlegend=False, hoverinfo='none'))
        self.add_trace(_get_scatter(selection, **props))
        trace_selection = self.data[9]

        def ipi_on_click(trace, points, selector):
            if len(points.xs) > 0 and points.trace_name == trace_ipi.name:
                selection_exists = False if len(
                    trace_selection.x) == 0 else True
                selection_x = points.xs[0]
                selection_y = points.ys[0]
                add_point = True

                if selection_exists:
                    if np.isin(selection_x, trace_selection.x) and np.isin(selection_y, trace_selection.y):
                        add_point = False
                    self._remove_selection(
                        trace_selection, trace_selection.x[0], trace_selection.y[0])
                    self._hide_ipi()

                if add_point:
                    self._add_selection(
                        trace_selection, selection_x, selection_y)
                    self._show_ipi(selection_x, selection_y, bounds)

        trace_ipi.on_click(ipi_on_click)
        self.update_layout(hovermode='x')

    def _update_ipi_axes(self, bounds):
        '''
        '''
        self.update_xaxes(dict(title_text=self.title_x,
                               ticks='outside',
                               ticklen=4,
                               nticks=10,
                               showline=True,
                               linewidth=0.5,
                               linecolor='black',
                               mirror=True,
                               showgrid=G_SHOWGRID,
                               range=[bounds['x_min'], bounds['x_max']]),
                          row=2, col=1)

        self.update_yaxes(dict(title_text=self.title_ipi_y,
                               ticks='outside',
                               ticklen=4,
                               nticks=10,
                               showline=True,
                               linewidth=0.5,
                               linecolor='black',
                               showgrid=G_SHOWGRID,
                               mirror=True,
                               zeroline=False),
                          row=2, col=1)

    def _add_selection(self, trace_selection, xs, ys):
        trace_selection.x = [xs]
        trace_selection.y = [ys]

    def _remove_selection(self, trace_selection, xs, ys):
        trace_selection.x = []
        trace_selection.y = []

    def _show_ipi(self, x_end, length, bounds):
        '''
        '''
        lines_update = self._lh_peaklines + [go.layout.Shape(
            type="rect",
            fillcolor="lightgrey",
            opacity=0.5,
            x0=x_end - length,
            y0=bounds['y_min'],
            x1=x_end,
            y1=bounds['y_max'],
            line=go.layout.shape.Line(color="grey", width=0.5)
        )]

        self.update_layout(
            shapes=lines_update
        )

    def _hide_ipi(self):
        '''
        '''
        self.update_layout(shapes=self._lh_peaklines)
