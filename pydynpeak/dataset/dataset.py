# This file is part of PyDynPeak software
# Copyright (C) 2021, Inria
# 
# PyDynpeak is a Python software derived from:
#          DynPeak (Scilab ATOMS Toolbox) version 2.1.0 (authors:
#	   	   	   	Claire Medigue, Serge Steer, Qinghua Zhang,
#              			Alexandre Vidal, Frédérique Clément
#                               cf. https://atoms.scilab.org/toolboxes/Dynpeak)
#
# PyDynPeak is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# PyDynPeak is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with PyDynPeak.  If not, see <https://www.gnu.org/licenses/>
# 
###############################################################################
#
# Authors : Frédérique Clément(+), Hande Gözükan(*), Christian Poli(*)
#
# +=scientific advisor, *=developer
#
###############################################################################

import pandas as pd
import os
import glob
from pathlib import Path


def load_csv(filepath, sep=',', header=None, names= None, seriesname='', timeinterval=10, timecolumn=None):
    '''Loads the contents of the CSV file specified by `filepath` to a
    dataframe with index from 0 to (numberoflines - 1) * 10, with an
    increment of timeinterval.

    Parameters
    ----------

    filepath : str
        absolute/relative path of the CSV file.

    sep : str, default ','
        separator of the CSV file.

    header : int, default 'None'
        None means no header, a positive integer value indicates
        the row number in CSV file to be used as column headers.

    names : array-like, default None
        List of column names to use. If file contains no header
        row, then you should explicitly pass header=None.

    seriesname : str, default empty str
        column name to be used in combination with one-based
        column index, in the case headers do not exist in CSV
        file.

        Example 
        ------- 
        Caraty ewes1, Caraty ewes2,... when seriesname is
        specified as `Caraty ewes`.

    timeinterval : int, default 10
        time interval to generate the sampling time values.

    timecolumn : int, default None
        zero-based column index that specifies the index of the
        column that contains sampling time values. If `None`,
        sampling time values are generated using `timeinterval`.

    Returns
    -------
    pandas.DataFrame 
        Dataframe that contains the data in CSV file with an index
        from 0 to (numberoflines - 1) * 10, with an increment of 10.
        If the specified `filepath` does not exist or invalid, prints
        a message to the user and returns.
    '''
        
    df = pd.read_csv(filepath, sep=sep, header=header, index_col=timecolumn)

    return _process_dataframe(df, header, seriesname, timeinterval, timecolumn)


def load_excel(filepath, sheet_name=0, header=None, names= None, seriesname='', timeinterval=10, timecolumn=None):
    '''Loads the contents of the excel file specified by `filepath` to a
    dataframe with index from 0 to (numberoflines - 1) * 10, with an
    increment of timeinterval.

    Parameters
    ----------

    filepath : str
        absolute/relative path of the Excel file.

    sheet_name : str or int, default 0
        Strings are used for sheet names. Integers are used in
        zero-indexed sheet positions.

    header : str, default 'None'
        None means no header, a positive integer value indicates
        the row number in Excel file to be used as column headers.

    names : array-like, default None
        List of column names to use. If file contains no header
        row, then you should explicitly pass header=None.

    seriesname : str, default empty str
        column name to be used in combination with one-based
        column index, in the case headers do not exist in CSV
        file.

        Example 
        ------- 
        Caraty ewes1, Caraty ewes2,... when seriesname is
        specified as `Caraty ewes`.

    timeinterval : int, default 10
        time interval to generate the sampling time values.

    timecolumn : int, default None
        zero-based column index that specifies the index of the
        column that contains sampling time values. If `None`,
        sampling time values are generated using `timeinterval`.

    Returns
    -------
    pandas.DataFrame 
        Dataframe that contains the data in Excel file with an index
        from 0 to (numberoflines - 1) * 10, with an increment of 10.
        If the specified `filepath` does not exist or invalid, prints
        a message to the user and returns.
    '''

    df = pd.read_excel(filepath, sheet_name=sheet_name, header=header, index_col=timecolumn)
    
    return _process_dataframe(df, header, seriesname, timeinterval, timecolumn)


def _process_dataframe(df, header, seriesname, timeinterval, timecolumn):
    '''
    '''

    # rename columns, if no header is row is specified
    if header == None or header < 0:
        df.columns = [f'{seriesname}{i}' for i in range(1, len(df.columns) + 1)]

    # set time index if no column is specified as time column
    if timecolumn is None:
        df.index = pd.RangeIndex(0, df.shape[0] * timeinterval, timeinterval)

    return df


