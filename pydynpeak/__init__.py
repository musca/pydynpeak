# This file is part of PyDynPeak software
# Copyright (C) 2021, Inria
# 
# PyDynpeak is a Python software derived from:
#          DynPeak (Scilab ATOMS Toolbox) version 2.1.0 (authors:
#	   	   	   	Claire Medigue, Serge Steer, Qinghua Zhang,
#              			Alexandre Vidal, Frédérique Clément
#                               cf. https://atoms.scilab.org/toolboxes/Dynpeak)
#
# PyDynPeak is a free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# PyDynPeak is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with PyDynPeak.  If not, see <https://www.gnu.org/licenses/>
# 
# Parts of this file are based on work covered by the following copyright
#       and permission notice:
#      """
#      // Copyright (C) 2012 - INRIA - Serge Steer
#      // This file must be used under the terms of the CeCILL.
#      // This source file is licensed as described in the file COPYING, which
#      // you should have received as part of this distribution.  The terms
#      // are also available at
#      // http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#      """
###############################################################################
#
# Authors : Frédérique Clément(+), Hande Gözükan(*), Christian Poli(*)
#
# +=scientific advisor, *=developer
#
###############################################################################

import sys, csv, yaml, io


DYNE = None
def get_dyne():
    from .model import DynpeakExecuterWrapper
    global DYNE
    if DYNE is None:
        print("Init Dyne")
        DYNE = DynpeakExecuterWrapper()
    return DYNE

def sub_load_csv(arg, kw={}):
    dyne = get_dyne()
    if arg is None and not kw:
        raise ValueError('filepath arg is mandatory')
    if arg is not None:
        filepath=arg
        with open(filepath, newline='') as csvfile:
            dialect = csv.Sniffer().sniff(csvfile.read(1024))
        return dyne.load_csv(filepath=arg, sep=dialect.delimiter, **kw)
    return dyne.load_csv(**kw)

def sub_paste_csv(arg, content):
    dyne = get_dyne()
    sio = io.StringIO(content)
    dialect = csv.Sniffer().sniff(sio.read(1024))
    sio2 = io.StringIO(content)
    return dyne.load_csv(filepath=sio2, sep=dialect.delimiter)

    
def sub_info(arg):
    assert not arg
    dyne = get_dyne()
    return dyne._dataframe.info()

def sub_describe(arg):
    assert not arg
    dyne = get_dyne()
    return dyne._dataframe.describe()

def _sub_view_ht(mode, arg):
    n_rows = 10
    if arg:
        n_rows = int(arg)
    dyne = get_dyne() 
    return dyne.view_head(n_rows) if mode=='h' else dyne.view_tail(n_rows)

def sub_view_head(arg):
    return _sub_view_ht('h', arg)

def sub_view_tail(arg):
    return _sub_view_ht('t', arg)
    
def sub_view_all(arg):
    assert not arg
    dyne = get_dyne()
    return dyne.view_all()

def sub_plot_lh_all(arg):
    header = None
    if arg:
        header = arg
    dyne = get_dyne()
    return dyne.plot_lh_all(header=header)


def sub_plot_lh(arg, kw=None): # plot_lh(self, index=None, header=None)
    dyne = get_dyne()
    index = None
    if arg:
        assert not kw
        index = int(arg)
    elif kw:
        return dyne.plot_lh(**kw)
    return dyne.plot_lh(index)

def sub_print_peaks(arg, kw=None): # print_peaks(self, index=None)
    assert kw is None
    dyne = get_dyne()    
    index = None
    if arg:
        index = int(arg)
    return dyne.print_peaks(index)

def sub_currentseries(arg, kw=None): # dyne.currentseries = 3
    assert kw is None
    dyne = get_dyne()
    if not arg:
        return dyne.currentseries
    index = int(arg)
    dyne.currentseries = index

def sub_detect_peaks(arg, kw): # detect_peaks(self, index=None, **params)
    dyne = get_dyne()
    index = None
    if arg:
        index = int(arg)
        return dyne.detect_peaks(index, **kw)
    return dyne.detect_peaks(**kw)

def sub_detect_peaks_and_plot(arg, kw={}): # detect_peaks_and_plot(self, index=None, **params)
    dyne = get_dyne()
    index = None
    if arg:
        index = int(arg)
        return dyne.detect_peaks_and_plot(index, **kw)
    return dyne.detect_peaks_and_plot(**kw)

def sub_print_detection_params(arg, kw=None): # print_detection_params(self, index=None)
    assert kw is None
    dyne = get_dyne()
    index = None
    if arg:
        index = int(arg)
    return dyne.print_detection_params(index)

def sub_update_detection_params(arg, kw): # update_detection_params(self, index=None, **params)
    assert kw
    dyne = get_dyne()
    return dyne.update_detection_params(**kw)


class DisplayCommands:
    def __init__(self, lst):
        self._lst = lst
    def _repr_html_(self):
        base_url = "https://musca.gitlabpages.inria.fr/pydynpeak/api/model.html#pydynpeak.model.DynpeakExecuter."
        title = "Use the magic command %dyne with one of the following subcommands:"
        res = [f"<h3>{title}</h3><ul>"]
        for elt in self._lst:
            if elt in ['info', 'describe', 'paste_csv']:
                res.append(f"<li>{elt}</li>")
            else:
                res.append(f"<li>{elt} <a href={base_url}{elt} target='_blank'>(API doc, params ...)</a></li>")
        res.append("<ul>")
        return ''.join(res)

IS_NOTEBOOK = False
try:
    get_ipython()
    IS_NOTEBOOK = True
except NameError:
    pass

if IS_NOTEBOOK:
    lst_func = ['load_csv', 'paste_csv', 'info', 'describe', 'view_head', 'view_tail', 'view_all',
                'plot_lh_all', 'plot_lh', 'print_peaks', 'currentseries', 'detect_peaks',
                'detect_peaks_and_plot',  'update_detection_params', 'print_detection_params']

    from IPython.core.display import display, HTML
    from IPython.core.magic import (Magics, magics_class, line_magic,
                                    cell_magic, line_cell_magic, needs_local_scope,
                                    register_line_magic, register_cell_magic)
    from IPython.display import clear_output

    @register_line_magic
    def dyne(line):
        if not line:
            clear_output()            
            return DisplayCommands(sorted(lst_func))
        subcmd, *args = line.strip().split(' ', 1)
        assert subcmd in lst_func
        arg = None
        if args:
            arg = args[0]
        return globals()[f"sub_{subcmd}"](arg)

    @register_cell_magic
    def dyne(line, cell):
        subcmd, *args = line.strip().split(' ', 1)
        assert subcmd in lst_func
        arg = None
        if args:
            arg = args[0]
        if subcmd == 'paste_csv':
            return sub_paste_csv(arg, cell)
        kw = yaml.safe_load(cell)
        if subcmd == 'load_csv':
            if 'sep' in kw and kw['sep']=='\\t':
                kw['sep'] = '\t' # avoids a warning
            return sub_load_csv(arg, kw)
        return globals()[f"sub_{subcmd}"](arg, kw)        

