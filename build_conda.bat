setlocal
REM In order to always run tests in a blank environment, every job removes the corrent Miniconda3 dir and re-creates it
REM by clonning a blank Miniconda3
rmdir /q /s c:\Users\ci\Miniconda3
robocopy c:\Users\ci\Blank_Miniconda3 c:\Users\ci\Miniconda3\ /MIR
REM if errorlevel 1 exit 1
REM set CONDA=c:\Users\ci\Miniconda3\condabin\conda.bat
REM In order to always run tests in a blank environment, every job removes the current Miniconda3 dir and re-creates it
REM by clonning a blank Miniconda3
rmdir /q /s c:\Users\ci\Miniconda3
robocopy c:\Users\ci\Blank_Miniconda3 c:\Users\ci\Miniconda3\ /MIR
set CONDA_BUILD_CMD=c:\Users\ci\Miniconda3\Scripts\conda-build
set CONDA_CMD=c:\Users\ci\Miniconda3\condabin\conda.bat
REM conda-build and  conda-verify pre-installed on Blank_Miniconda3
call %CONDA_CMD% config --set always_yes yes --set changeps1 no
if errorlevel 1 exit 1
call %CONDA_CMD% remove --name pydynpeak --all --yes
if errorlevel 1 exit 1
call %CONDA_CMD% update -q conda
if errorlevel 1 exit 1
call %CONDA_CMD% info -a
if errorlevel 1 exit 1
REM %CONDA_BUILD% purge
REM %CONDA% remove dynpeak
call %CONDA_CMD% env create -q -f binder\environment.yml
if errorlevel 1 exit 1
endlocal
